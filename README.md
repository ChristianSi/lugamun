# Lugamun, an easy and fair language for global communication

**Lugamun**, short for **luga komun** (common language), is an
easy-to-learn, logical and well-balanced auxiliary language for global
usage. Its goal is to allow people who otherwise don't share a common
language to communicate effectively and on an equal footing.

For more on Lugamun, see the website: https://www.lugamun.org/.

[TOC]


## License

The software in this repository is licensed under the [ISC
license](https://en.wikipedia.org/wiki/ISC_license), a permissive free
software license. See the file [LICENSE.txt](LICENSE.txt) for the full
license text.

The language Lugamun – including the dictionary contained in this
repository and all related data files – is placed in the public domain as
per the [CC0 1.0 Universal (CC0 1.0) Public Domain
Dedication](https://creativecommons.org/publicdomain/zero/1.0/deed.en).


## Files in This Repository

The rest of file document explains the other files that can be found in
this repository, including their purposes and their formats.


### The data directory

This directory contains files used to generate the Lugamun dictionary as
well as automatically generated files on the website. Most files in it can
be read as plain text, but they are written in specific formats that make
them easily parsable by computers.


#### dict.csv

A [CSV file](https://en.wikipedia.org/wiki/Comma-separated_values) that
contains the Lugamun/English directory in compact form, listing only the
English translations of each entry (first field) followed by the
corresponding word or expression in Lugamun (second field).

It can be imported into the
[Anki](https://en.wikipedia.org/wiki/Anki_(software)) learning software,
thus offering an easy way of learning and repeating the Lugamun vocabulary.

This file is automatically updated by `buildvoc.py` every time a new entry
is added or an existing entry is modified.


#### dict.txt

The multilingual Lugamun dictionary itself, in computer-readable form: A
structured text file that contains a series of entries, each describing a
word. Entries are separated by empty lines. Note that these lines must be
completely empty – if they contain whitespace (spaces or tabs), the parser
won't recognized them as empty, which might cause parsing errors. There
must be at least one empty line before each new entry – additional empty
lines are allowed, but ignored.

Each line in an entry is a key/value pair. Keys and values are separated by
a colon (':'). Whitespace around the separator or around the whole line is
ignored. Duplicate keys are not allowed. Lines starting with '#' are
considered comments and ignored.

The following field names are used as keys. Unless stated otherwise, all
fields are optional.

* **word:** The word itself. This field is always present. In a few cases
  it lists two or more comma-separated alternatives.

* **class:** The class of the word, describing the role it plays in a
  sentence – also know as POS tag. If it belongs to several classes, they
  are comma-separated. For a list of classes and their full English names,
  see the file `transvalues.txt` (all "values" that start with a lower-case
  letter are class names). This field is always present.

* **gloss:** If the word is derived from another Lugamun word by adding an
  affix or prefix, or by combining two words into a single joint form, this
  field specifies how it is formed. Usually the parts are separated using a
  '+' sign, e.g. "mabet" is glossed as "ma+bet", "poliski" is glossed as
  "Poliska+i". It's possible to enclose some letters in parentheses to
  indicate that they are ignored when the parts are joined. For example,
  "Merkur(i)+den" indicates that the parts "Merkuri" and "den" are joined
  to form "merkurden". This should be used very sparingly (so far it's only
  used for some of the days of the week). In a few other cases, a word is
  derived from another one by changing only its case; in such cases, the
  gloss starts with an '=' sign, e.g. "esti" is glossed as "=Esti". A few
  affixes are derived in an informal way from Lugamun words. If this is the
  case, an informal explanation (that will always contain multiple words
  and hence spaces) is given as gloss, e.g. "ma-" is glossed as "Common
  prefix of 'man' and 'mal'".

* **infl:** A comma-separated list of source languages (language tags) that
  influenced the form of the selected word, if it was directly derived from
  words in other languages. In addition to the 2 or 3-letter ISO language
  codes of the languages from which the word derived, the code "taxo" may
  be used to indicate that it was derived from the name used in the
  biological taxonomy for a species – see the "taxo" field documented
  below. Most entries have either an "infl" field (if they are derived from
  the source languages) or a "gloss" (if they are derived from other
  Lugamun words). However, multi-word expressions such as "den tali" have
  neither. Only if a word derived from the source languages has a synonym
  derived from other Lugamun words, both "infl" and "gloss" will be present
  in the same entry. For example, for "falsa, nesahi", the influences are
  "es, en, id" (the languages from which "falsa" is derived) and the gloss
  (referring to "nesahi") is "ne+sahi".

* **sample:** This field may give one or more usage examples for the word.

* **sense:** The sense or senses under which translations for a word are
  listed in Wiktionary. Multiple senses are separated by " | ", e.g. "weka"
  (put, place) is listed as "to place something somewhere | to put in a
  specific location", with "to place something somewhere" being the sense
  specified for [put](https://en.wiktionary.org/wiki/put#Translations) and
  "to put in a specific location" the sense for the verb
  [place](https://en.wiktionary.org/wiki/place#Translations_2). If the
  English words cannot be trivially mapped to the senses, the English word
  corresponding to each sense is added in parentheses at its end, e.g. the
  senses for "manan" include "to convey, indicate (mean) | symbolic value
  of something (meaning) | to signify (mean)". A hyphen ('-') is used as
  sense if an English word is added as translation without having a
  corresponding Wiktionary entry.

* **tags:** An optional comma-separated list of tags associated with an
  entry. For a list of tags, see below.

* **taxo:** in case of animal or plant specifies, this field may give the
  term used in biological taxonomy for this species. For example, for
  "troglodit" (wren), the biological name "Troglodytidae" is listed here.

* **value:** In the case of numbers, this field specifies their numeric
  value, e.g. "16" for "des sis".

* Various field names with 2 or 3 letters such as "cmn", "en", "fr", "it",
  "sw" specify one or more translations into the specified language;
  multiple translations are separated by commas; an explanation of meaning
  or usage may be enclosed in parentheses. The 2-letter ISO language code
  is used for all languages that have one; otherwise the 3-letter ISO
  language code is used.

* Various field names with 2 or 3 letters followed by "-latn" such as
  "ar-latn", "bn-latn", "ru-latn": the romanized contents of the
  corresponding simple field ("ar", "bn", "ru" etc.). The used
  transcription method is the one used by Wiktionary (e.g. pinyin for
  Chinese).

The order of fields is as listed above, with the language codes following
in alphabetic order after all other fields and each each "xx-latn" field
following immediately after the corresponding "xx" field. Entries are
ordered alphabetically as per the "word" field.

Other fields may be added in the future. Note that, to avoid potential
conflicts with language codes, all other field names must have 4 letters or
more.

Currently, the following values can be used in the **tags** field:

* **core**: signals that a word can be considered part of Lugamun's most
  fundamental, core vocabulary
* **erg**: marks a verb as ergative
* **ntr**: marks a verb as intransitive
* **tr**: marks a verb as typically transitive

Note that at most one of the three verb-related tags (erg/ntr/tr) may be
used in each entry and that they may be used only with verbs (one of the
word classes must be "verb").


#### extradict.txt

This manually updated file complements and corrects the entries listed in
`termdict.txt`. The format is essentially the same as of `dict.txt` and
`termdict.txt`. In contrast to `dict.txt`, this file never has the
Lugamun-specific fields "word", "gloss", "infl", and "sample", since those
are only added when a word is actually added in Lugamun, and this file
merely helps to prepare this. But additionally it may have `xx-ipa` fields
(where xx is a 2 or 3-letter language code) that give the pronunciation of
a word in the IPA (International Phonetic Alphabet). These fields are used
when generating candidates from English (en-ipa) as well as from other
languages that are not among our common source languages, such as German or
Persian. For the other regular source languages, IPA information is not
needed nor used.

Entries are identified by the combination of the "en" and "sense" fields –
if an `extradict.txt` entry has the same combination as a `termdict.txt`
entry, they are merged. This can be used to supply additional translations
and other information (such as IPA and romanization) missing in the
original entry. If both entries contain the same keys, the values in
`extradict.txt` are used instead of those in `termdict.txt`. This can be
used to correct translations and other field values.

It is also possible to add entries to `extradict.txt` that don't have a
corresponding `termdict.txt` entry, in order to define concepts missing in
the latter. Such entries **must** include the line `tags: add` to mark them
as additional, otherwise a warning will be printed and the entry will be
ignored. (That line won't be copied to `dict.txt` when an entry is added;
nor are the `-ipa` fields.)


#### kaikki.org-dictionary-English.json.gz and kaikki.org-dictionary-English.json.gz.date

The `json.gz` file is a machine-readable copy of all the information in the
English Wiktionary, downloaded from https://kaikki.org/ by the
`updatekaikki.py` script. It is used by `parsewikt.py` to generate
`termdict.txt`, which contains those parts of that information that are
relevant for us in a more accessible way.

The accompanying `date` file contains a timestamp specifying when the
`json.gz` was last changed. It is used and updated by `updatekaikki.py` to
avoid unnecessary re-downloads of this (pretty big) file when there were no
changes since the last download.

**Note:** Like `termdict.txt` (see below), these files (the first of which
is pretty big) are ignored and not stored in the repo. To download them,
type `make updatekaikki` in the `data` directory .


#### langcodes.csv

A file in [CSV
format](https://en.wikipedia.org/wiki/Comma-separated_values) that contains
a short mapping from names used for languages in Wiktionary translations
(sometimes accidentally in malformatted entries) to the corresponding ISO
language code. If a name doesn't represent a real language, the code should
be set to "mis" for "missing". This file is used by `parsewikt.py` – only
if that script complaints about a language that lack a language code, a new
entry should be added to it.

Note: Except for `dict.csv`, all CSV files in this directory contain a list
of field headers as first line. This list only serves for documentation
purposes and is ignored when reading files.


#### luglosser.cfg

This configures the `luglosser.py` script, specifying any custom glosses to
be used for entries. The basic format are "key: value" pairs just as in
`dict.txt` and the other dict files, but there is only one large entry
(empty lines are ignored). Lines indented with two or more spaces are
considered continuation lines that continue the preceding field.

Keys can have the form "langcode.number", e.g. "en.2" and "en.3" for
English. In this case, the value is a comma-separated list of words for
which the specified number of translations should be used when glossing the
word. For example `en.3: xi` means that the first three of its translations
(yes, indeed, be) should all be printed when glossing the word *xi*. If a
word isn't mentioned in `luglosser.cfg`, only the first translation is used
by default.

Keys can also have the form "langcode.word", where "word" is the Lugamun
word to be glossed. In this case, the gloss to use follows as value. For
example `en.sem: -self` means that *sem* should be glossed as "-self". Some
words are glossed by a short description of their grammatical function
rather than by a translation – such grammatical descriptions are
conventionally written in all-caps, e.g. `en.bi: PASSIVE`.


#### Makefile

This file controls the Unix `make` utility; it can be used to conveniently
execute various important operations related to the Lugamun dictionary. It
does this by invoking some of the scripts defined in the `scripts`
directory. To run one of the defined targets, type `make TARGETNAME` in the
`data` directory.

Specifically, the following targets are defined:

* **check:** invokes `checkdict.py` in order to check that the Lugamun
  dictionary and the `extradict.txt` file are free of errors and formatting
  problems. Otherwise prints a list of the errors and problems encountered.

* **formattop30:** invokes `formattop30.py` to print the most widely spoken
  languages (listed in `top-30-langs.csv`) as a nicely formatted list.

* **sourcelangs.csv:** invokes `selectsourcelangs.py` to generate the set
  of source languages (defined in `sourcelangs.csv`), on the basis of the
  most widely spoken languages (defined in `top-30-langs.csv`). Normally
  there shouldn't be any reason to invoke this target, since Lugamun's set
  of source languages can be considered fairly stable.

* **stats:** invokes `showstats.py` to generate the [dictionary
  statistics](https://www.lugamun.org/en/statistics) as shown on the
  website. This script is automatically called once per night to update the
  statistics shown on the website (if anything has changed), so there
  should rarely be much need to invoke this target manually.

* **termdict.txt:** invokes `parsewikt.py` to update the `termdict.txt`
  term dictionary if the raw JSON dictionary downloaded from kaikki.org has
  changed. If not, this target does nothing.

* **updatekaikki:** Invokes `updatekaikki.py` to download the latest
  version of `kaikki.org-dictionary-English.json.gz` from kaikki.org. If
  that file hasn't changed since the last invocation, this target does
  nothing.

* **word:** invokes `buildvoc.py` to select a word to be added to the
  dictionary and to generate the list of candidates for it. This is the
  most commonly used and most useful target. But note that by itself it
  will never modify the dictionary, since to do so, `buildvoc.py` must be
  invoked with one or more command-line arguments in order so select a
  candidate for inclusion – the simplest case is to use `-s1` to select the
  first candidate. Note that `buildvoc.py` must be invoked directly to pass
  it any arguments. Typing `make word` will not work for this, since it
  doesn't allow any further arguments (they would instead by interpreted as
  arguments for the `make` utility, likely resulting in errors).

If `make` is invoked without any explicit target, the default targets are
executed. These are `updatekaikki` and `termdict.txt` (download the latest
JSON dump and regenerate the term dictionary, if necessary), followed by
`word` (propose a word to be added).


#### phon-??.csv

These files provide the basis of converting words from our various source
languages to the spelling used in Lugamun. In most cases, ?? represents an
ISO language code – e.g. `phon-hi.csv` provides the mapping for Hindi
(Hindustani) – but there are also a few other cases:

* `phon-ipa.csv`: A mapping from IPA to Lugamun. This file is used for
  converting `-ipa` entries – such entries are used for English as well as
  optionally for languages that aren't among our usual source languages.
  The latter are typically only used when adding words for languages and
  countries, which are typically based on the endonym (the locally used
  name).

* `phon-pinyin.csv`: A mapping from Pinyin to Lugamun – used for converting
  Mandarin words.

* `phon-taxo.csv`: A mapping from the spellings used in biological taxonomy
  to Lugamun – used when converting the `taxo` field.

Each line in each mapping contains three values: the original spelling; the
spelling to be used in Lugamun; and whether or not a penalty should be
applied when converting that letter (0 = no penalty, 1 = penalty). For
example, in `phon-ipa.csv`, 'z' becomes 's' with a penalty (because a
voiced consonant is changed into a voiceless one), while "ɒ" becomes "o"
without a penalty.


#### selectionlog.txt

Documents the word selection process, including the reasons for the
selection of each word. Each choice is described in an entry block, and
separate entry blocks are separated by two empty lines (while the blocks
themselves don't contain any empty lines). The contents of the blocks
should be fairly self-documenting.


#### sourcelangs.csv and top-30-langs.csv

Two CSV files that include some details on Lugamun's source languages as
well as on the most widely spoken languages. The first line is a header
line which documents the fields used in each entry.

`sourcelangs.csv` was originally generated from `top-30-langs.csv` by the
`selectsourcelangs.py` script, though the list of source languages is now
stable and further changes in the near future are unlikely.


#### termdict.txt

A parsed version of all the information from Wiktionary that's relevant and
used for our dictionary generation process. The format is essentially the
same as of `extradict.txt`. This file is generated by `parsewikt.py`, based
on the machine-readable version of Wiktionary stored as
`kaikki.org-dictionary-English.json.gz`.

Note that this file cannot be edited manually, since any changes will be
overwritten once it is regenerated the next time. In order to make changes
to existing fields or to add additional fields (such as missing
translations), copy the "sense" and "en" fields of an entry to
`extradict.txt` and add any further fields that you want to modify or add
there.

Note also this this file doesn't contain any "taxo" fields, since taxonomic
information isn't available in Wiktionary in a machine-readable way. To add
such as field, copy the entry to `extradict.txt` as explained above, and
add it there.

**Note:** To save space, this file is not actually stored in the git
repository, but rather ignored (it's listed in the `.gitignore` file).
Instead, it can be locally generated from the `kaikki.org...` files (which
are likewise ignored), once the latter have been downloaded. To do so and
(re-)generate the `termdict.txt` file, type `make termdict.txt` in the
`data` directory. It's also possible to type just `make`, though that will
additionally yield a proposal for the next word to be added to Lugamun's
dictionary. Even if the files already exist, you should repeat this command
every few days (at least once a week or so), since kaikki.org frequently
publishes new versions based on the latest Wiktionary data.



#### transvalues.txt

This file is used by several scripts to localize values into English and –
hopefully in the future – into other languages. It contains multiple groups
of "key: value" pairs, using the same basic format as the various
`...dict.txt` files. It is used to translate the word class abbreviations
used in `dict.txt` into the section names used for the [online
wordlist](https://www.lugamun.org/en/wordlist), e.g. the entry

```
value: adj
en: Adjectives
```

specifies how to call the section listing "adj" entries in English.
Translations into other languages can be added in the same way, by using
the ISO language code of that language as key, followed by a (pluralized)
translation.

While these abbreviations all start with a lower-case letter, there is also
a set of values that by convention start with an upper-case letter, such as
"Intro". These contain translated text blocks used in automatically
generated files on Lugamun's website, such as the wordlist and the
[dictionary statistics](https://www.lugamun.org/en/statistics). For
example, "Intro" contains the introductory note that the file is
auto-generated and cannot be edited manually.

Currently, this file contains only translations into English (en), but once
the website becomes multilingual, it will hopefully contain translations
into all the other supported languages (identified by their ISO language
codes).


### The scripts directory

This directory contains the Python scripts used to add new words to the
Lugamun dictionary, to print statistics on it, and convert it in a more
human-accessible form. For more details, see the intro comments within the
various `py` (Python) files as well as the description of the multiple
goals defined by the [Makefile](#makefile) in the `data` directory.

Additionally, this directory also contains the following supporting files:

* `comment-dict.txt`: a list of words used in comments within the Python
  files; consulted by the `pylint` spell checker. Any word neither
  recognized as English word nor listed here will trigger a warning.

* `Makefile`: defines several `make` targets that check the code quality of the
  Python files, checking that there are no type errors or other detectable
  problems, that there are no spelling errors in the comments, and that all
  defined tests pass (sadly, there aren't many). Just invoke `make` in this
  directory to run all these targets together; if there are any errors or
  warnings, they should be fixed.

* `.pylintrc`: used to configure the `pylint` code checker.
