# pylint: disable=invalid-name, missing-docstring

import util

# count_vowels


def test_count_vowels_zero_expected():
    for word in ('', 'blb'):
        assert util.count_vowels(word) == 0


def test_count_vowels_one_expected():
    for word in ('tai', 'tan', 'tru'):
        assert util.count_vowels(word) == 1


def test_count_vowels_two_expected():
    for word in ('dauda', 'fala', 'inte', 'kaishen'):
        assert util.count_vowels(word) == 2


def test_count_vowels_four_expected():
    for word in ('kontinyada', 'rezultiza', 'trides tri mil'):
        assert util.count_vowels(word) == 4


# final_vowel

def test_final_vowel_yes():
    assert util.final_vowel('pli') == 'i'


def test_final_vowel_capitalized():
    assert util.final_vowel('A') == ''


def test_final_vowel_external_diphthong():
    assert util.final_vowel('enjoi') == 'oi'


def test_final_vowel_internal_diphthong():
    assert util.final_vowel('enjoI') == 'oI'


def test_final_vowel_consonant():
    assert util.final_vowel('posib') == ''


def test_final_vowel_empty():
    assert util.final_vowel('') == ''


def test_final_vowel_no_just_a_single_vowel():
    assert util.final_vowel('o') == 'o'
