#!/usr/bin/env python3
"""Print the top-30 languages as nicely formatted list."""

import csv

INFILE = 'top-30-langs.csv'


def selectlangs() -> None:
    """Print the top-30 languages as nicely formatted list."""
    with open(INFILE, newline='', encoding='utf8') as infile:
        csvreader = csv.reader(infile)
        next(csvreader)  # skip header
        for row in csvreader:
            rank, name, code, branch, region, writing, speakers, ethnologue = row
            print(f'{rank}. {name} ({code}): {speakers} speakers  ')
            suf = '  ' if ethnologue else ''
            print(f'Branch: {branch}, region: {region}, writing system: {writing}{suf}')
            if ethnologue:
                print(f'In Ethnologue: {ethnologue}')
            print()


if __name__ == '__main__':
    selectlangs()
