"""Utility functions and useful types for the Lugamun scripts."""

from abc import ABC, abstractmethod
from contextlib import contextmanager
import csv
from datetime import datetime
import os
from os import path
import re
import readline  # pylint: disable=unused-import
from shutil import copyfile
import sys
from typing import (Any, Callable, Dict, Iterator, List, Mapping, MutableMapping, Optional,
                    Sequence, Tuple, TypeVar)
from warnings import warn
import warnings


##### Global config (adjust how warnings are shown) #####

def compact_warning(message, category, filename, lineno, line=None):
    """Print warnings without showing the warning message line itself."""
    # pylint: disable=unused-argument
    return f'{filename}:{lineno}: {category.__name__}: {message}\n'


warnings.formatwarning = compact_warning


##### Constants and precompiled regexes #####

# Generic type variable
T = TypeVar('T')  # pylint: disable=invalid-name

# Directory and file parts of the preparsed kaikki.org dump (created by wiktextract)
KAIKKI_EN_DIR = 'https://kaikki.org/dictionary/English'
KAIKKI_EN_FILE = 'kaikki.org-dictionary-English.jsonl'

# Names of locally used files
EXTRA_DICT = 'extradict.txt'
DICT_FILE = 'dict.txt'
TERM_DICT = 'termdict.txt'

# Vowel and consonant listings
SIMPLE_VOWELS = 'aeiou'
FINAL_CONSONANTS = 'lmnrst'

# This uses the internal spellings for the diphthongs (second letter is capitalized)
INTERNAL_DIPHTHONGS_STR = 'aI aU oI'
DIPHTHONGS_SET = frozenset(INTERNAL_DIPHTHONGS_STR.lower().split())
DIPHTHONGS_RE = r'(?:a[iu]|oi)'
VOWEL_RE = re.compile(f'(?:{DIPHTHONGS_RE}|[{SIMPLE_VOWELS}])')

# A comma/semicolon, with optional whitespace around it
COMMA_RE = re.compile(r'\s*,\s*')
SEMICOLON_RE = re.compile(r'\s*;\s*')

# A comma surrounded by parentheses
COMMA_IN_PARENS_RE = re.compile(r'\([^)]*,[^)]*\)')

# The 3 auxlangs frequently represented in Wiktionary (ignoring Volapük which is only of
# historical interest)
COMMON_AUXLANGS = frozenset('eo ia io'.split())


##### LineDict and related types and functions #####

class ToStringDict(ABC):
    """Interface for objects that can be converted into a dictionary of string/string pairs."""
    # pylint: disable=too-few-public-methods

    @abstractmethod
    def to_dict(self) -> Mapping[str, str]:
        """Convert this object into a dictionary of string/string pairs."""


class LineDict(MutableMapping[str, str], ToStringDict):
    """
    A dictionary that contains line number information to generate meaningful messages.

    Since the dictionary is read from a text file, all keys and values must be strings.

    To add key/value, call add(key, value, lineno).

    Accessing keys and values works in the usual way.

    To get the line number of a key, call lineno(key).

    The get the line number of the first key/value pair, call first_lineno().
    """

    def __init__(self, filename: Optional[str] = None) -> None:
        """Create a new instance.

        The optional 'filename' argument is stored as a field (self.filename) and used in
        error messages.
        """
        self.filename = filename
        self._store: Dict[str, str] = {}
        self._first_lineno: Optional[int] = None
        self._lines: Dict[str, int] = {}

    def errprefix(self, lineno: int = -1) -> str:
        """Return the prefix to use for error messages (warnings).

        If self.filename is known, it is included in the prefix.
        If lineno is set to a non-negative value, it is likewise included.
        """
        if self.filename:
            lineinfo = f', line {lineno}' if lineno >= 0 else ''
            return f'Error parsing {self.filename}{lineinfo}: '
        lineinfo = f' on line {lineno}' if lineno >= 0 else ''
        return f'Error{lineinfo}: '

    def __getitem__(self, key: str) -> str:
        """Return the value for a key."""
        return self._store[key]

    def __iter__(self) -> Iterator[str]:
        """Return an iterator over the keys in this dictionary."""
        return iter(self._store)

    def __len__(self) -> int:
        """Return the number of key/value pairs in this dictionary."""
        return len(self._store)

    def add(self, key: str, value: str, lineno: int = -1, allow_replace: bool = False) -> None:
        """Add a key/value pair to the dictionary.

        If the key already exists in a dictionary, it will be replaced. Unless 'allow_replace'
        is True, a warning will be printed.

        If this method is called for the first time, 'lineno' will also be stored as firstline.
        Note that subsequent calls will not override this value, even if they come with a
        smaller 'lineno' value.
        """
        if key in self._store and not allow_replace:
            warn(f'{self.errprefix(lineno)}Duplicate key "{key}"')
        self._store[key] = value
        self._lines[key] = lineno
        if self._first_lineno is None:
            self._first_lineno = lineno

    def append_to_val(self, key: str, append_text: str) -> None:
        """Appends 'append_text' at the end of the value stored for 'key'.

        Throws a KeyError if that key doesn't yet exist in the dictionary.
        """
        self._store[key] += append_text

    def __setitem__(self, key, item):
        """Add a key/value pair to the dictionary using the standard dict protocol.

        Note that it's necessary to call 'add' instead to properly set a line number.
        If called like this, the 'first_lineno' will be re-used for the new entry.
        """
        self.add(key, item, coalesce(self._first_lineno, -1))

    def __delitem__(self, key):
        """Delete the specified key. Raises a KeyError if *key* is not in the map."""
        del self._store[key]

    def first_lineno(self) -> int:
        """Return the number of the first key added to this dictionary.

        Returns -1 if the dictionary is empty.
        """
        return coalesce(self._first_lineno, -1)  # type: ignore

    def lineno(self, key: str) -> int:
        """Return the line number associated with the key.

        Returns -1 if the key is unknown.
        """
        return coalesce(self._lines.get(key), -1)  # type: ignore

    def to_dict(self) -> MutableMapping[str, str]:
        """Return this dictionary itself."""
        return self


def dict_from_str(entry: str, firstline: int = 1, filename: Optional[str] = None) -> LineDict:
    """Parse a multi-line string into a dictionary structure.

    Each line must contain a key/value pair, separated by ':'. Whitespace around the
    separator or at the end of the line is ignored.

    There shouldn't be any empty or whitespace-only lines in the string. If such a line is
    encountered, a warning is printed and it is otherwise ignored.

    If a line does not contain a colon, it is ignored and a is printed.
    In case of duplicate keys, a warning is printed and the last value is used.
    If a key is preceded by a single space or by tabs, a warning is printed, but the key is
    accepted (without the initial whitespace).

    Empty keys are not allowed – in such cases, a warning is printed and the line is discarded.

    Lines starting with '#' (after possible whitespace) are considered comments and ignored.

    Continuation lines start with at least two spaces, followed by at least one printable
    character. They continue the value started or continued on the preceding line.
    The first two spaces on the line are discarded, while the rest of the line is added to
    the value, separated by a line break (which is therefore preserved).

    If a dictionary starts with a continuation line or if a such line follows a comment, a
    warning is printed and the line is ignored.

    It is an error to start a line with a single space or a tab followed by printable
    characters, unless the rest of the line is a comment. In such cases, a warning is printed
    and the line is ignored.

    'firstline' is the line number of the first line, used to add line number information
    to the dictionary and to generate meaningful warning messages.
    """
    lines = entry.splitlines()
    lineno = firstline - 1
    result = LineDict(filename)
    last_key_added = None  # None: initial value, '': last line was comment or empty

    for line in lines:
        lineno += 1
        line = line.rstrip()

        if line.startswith('  '):
            # Continuation line
            if last_key_added:
                value = line[2:]
                result.append_to_val(last_key_added, '\n' + value)
            else:
                if last_key_added == '':
                    error_cond = 'after a comment or empty line'
                else:
                    error_cond = 'at the start of a dictionary'
                warn(f'{result.errprefix(lineno)}Unexpected continuation line {error_cond}: '
                     f'"{line.strip()}"')
            continue

        if not line or line.lstrip().startswith('#'):
            # Skip empty and comment lines – the former trigger a warning
            if not line:
                warn(f'{result.errprefix(lineno)}Line is empty')
            last_key_added = ''
            continue
        if line[0] in (' ', '\t'):
            warn(f'{result.errprefix(lineno)}Invalid leading whitespace: "{line}"')
            line = line.lstrip()

        parts = line.split(':', 1)

        if len(parts) < 2:
            warn(f'{result.errprefix(lineno)}No valid key/value pair found: "{line}"')
            continue

        key = parts[0].strip()
        value = parts[1].strip()
        last_key_added = key

        if not key:
            warn(f'{result.errprefix(lineno)}Ignoring invalid empty key: "{line}"')
            continue

        result.add(key, value, lineno)

    return result


def read_dicts_from_file(filename: str) -> Sequence[LineDict]:
    """Read multiple dicts from a text file and return them in order.

    Dicts are separated by one or more empty lines and must comply with the format described in
    'dict_from_str'.
    """
    lineno = 1
    result = []

    with open(filename, newline='', encoding='utf8') as infile:
        # Split into entries (separated by empty lines)
        entries = infile.read().split('\n\n')

        for entrystr in entries:
            entry = dict_from_str(entrystr, lineno, filename)
            if entry:
                result.append(entry)
            lineno += entrystr.count('\n') + 2

    # If the last element is empty, we remove it altogether (this will happen esp. in case of
    # empty files)
    if result and not result[-1]:
        result.pop()

    return result


def read_single_dict_from_file(filename: str) -> LineDict:
    """Read a single dicts from a text file and return it.

    The dict must comply with the format described in 'dict_from_str'.
    """
    with open(filename, newline='', encoding='utf8') as infile:
        return dict_from_str(infile.read(), 1, filename)


def dump_dicts(dict_objects: Sequence[ToStringDict], filename: str) -> None:
    """Dump a sequence of dictionary-serializable objects into a file.

    Dictionaries are serialized using the format described in 'dict_from_str' and
    separated by an empty line. Keys are serialized in the order they have in the
    dict.  All whitespace in keys and values is normalized.
    """
    first = True
    with open(filename, 'w', encoding='utf8') as outfile:
        for obj in dict_objects:
            if first:
                first = False
            else:
                outfile.write('\n')
            outfile.write(stringify_dict(obj))


def stringify_dict(dict_object: ToStringDict) -> str:
    """Convert a dictionary-serializable object into a string.

    The object is serialized using the format described in 'dict_from_str'. Keys are
    serialized in the order they have in the dict.  All whitespace in keys and values is
    normalized.
    """
    dct = dict_object.to_dict()
    result = ''
    for key, value in dct.items():
        norm_key = normalize(key)
        norm_value = normalize(value)
        result += f'{norm_key}: {norm_value}\n'
    return result


##### Other Types #####

class RecordingLogger():
    """A very simple logger that keeps a record of everything logged.

    All messages are immediately print to stdout.

    The 'append_all_messages' method allows appending all messages recorded to far to a file.
    """

    def __init__(self) -> None:
        """Create a new instance."""
        self._messages: List[str] = []

    def info(self, msg: str) -> None:
        """Print an info message to stdout and record it for later."""
        print(msg)
        self._messages.append(msg)

    def warn(self, msg: str) -> None:
        """Print an warn message to stdout and record it for later.

        All warn messages will be prefixed by 'WARNING: '.
        """
        self.info('WARNING: ' + msg)

    def append_all_messages(self, filename: str) -> None:
        """Append all logged messages to 'filename'.

        If the file exists already, two empty lines will be inserted between the existing
        content and the new messages.

        If it doesn't exist, it will be created.

        If there are no message to append, this method does nothing. If there are any,
        the list of messages is cleared after printing it.
        """
        if not self._messages:
            return
        file_exists = path.exists(filename)

        # Create a backup copy if the file exists
        copy_to_backup(filename)

        with open(filename, 'a', encoding='utf8') as outfile:
            if file_exists:
                outfile.write('\n\n')
            for msg in self._messages:
                outfile.write(msg + '\n')

        self._messages = []


class ValueProvider():
    """Provide translations of metadata into the configured language.
    """
    # pylint: disable=too-few-public-methods

    def __init__(self, lang: str, values_file: str = 'transvalues.txt') -> None:
        """Create a new instance, using 'lang' as language.

        'values_file' is the file from which value strings and their translations will be read.
        """
        self.lang = lang
        self.values_file = values_file
        dct: Dict[str, str] = {}
        line_dicts = read_dicts_from_file(values_file)

        for line_dict in line_dicts:
            # store mapping from "value" to 'lang' code
            value = line_dict.get('value')
            trans = line_dict.get(lang)
            if value is None:
                warn(f'{values_file} entry starting on line {line_dict.first_lineno()} lacks a '
                     '"value" field')
                continue
            if trans is None:
                warn(f'{values_file} entry starting on line {line_dict.first_lineno()} lacks a '
                     f'translation into "{lang}"')
                continue
            dct[value] = trans
        self.dict = dct

    def lookup(self, value: str) -> str:
        """Lookup the translation into the configured language of the specified 'value'.

        If no translation is listed in the 'values_file', the value itself is returned, with
        ' (?)' appended.
        """
        result = self.dict.get(value)
        if result is None:
            result = value + ' (?)'
        return result


##### CSV-related utility functions #####

@contextmanager
def open_csv_reader(filename: str, skip_header: bool = True) -> Iterator[Any]:
    """Open a CSV file for reading.

    If `skip_header` is true (default), the first row is skipped as header row.
    """
    with open(filename, mode='r', newline='', encoding='utf-8') as csvfile:
        reader = csv.reader(csvfile)
        if skip_header:
            next(reader)
        yield reader


@contextmanager
def open_csv_writer(filename: str, create_backup: bool = True) -> Iterator[Any]:
    """Open a CSV file for writing.

    If `create_backup` is true (default) and there is already a file with the specified file, that
    file will be preserved by adding '.bak' to its name.
    """
    rename_to_backup(filename)
    with open(filename, mode='w', newline='', encoding='utf-8') as csvfile:
        writer = csv.writer(csvfile)
        yield writer


##### Text-related utility functions #####

def capitalize(text: str) -> str:
    """Return a copy of 'text' with its first character capitalized.

    In contrast to the build-in '.capitalize()' method, the rest of the string is left unchanged.

    If 'text' is empty, it is returned as-is.
    """
    if text:
        return text[0].upper() + text[1:]
    return text


def count_vowels(text: str) -> int:
    """Count the vowels in a Lugamun word. A diphthong is counted as a single vowel."""
    return len(VOWEL_RE.findall(text))


def eliminate_parens(text: str) -> str:
    """Eliminate those parts of a text written in parentheses."""
    return re.sub(r'\s*\([^)]*\)', '', text).strip()


def discard_text_in_brackets(text: str) -> str:
    """
    Remove all text contained within square brackets and any whitespace preceding them.

    Returns the input string with all bracketed text and preceding whitespace removed.
    If if those contain any square brackets, the original string is returned unchanged.
    """
    return re.sub(r'\s*\[.*?\]', '', text)


def extract_text_in_brackets(text: str, otherwise_return_fully: bool = True) -> Optional[str]:
    """Extract text between square brackets if present.

    If there is no such text and 'otherwise_return_fully' is true, 'text' is returned completely.
    Otherwise, None is return.
    """
    start = text.find('[')
    end = text.find(']')
    if start != -1 and end > start:
        return text[start + 1:end]
    return text if otherwise_return_fully else None


def final_vowel(word: str) -> str:
    """This returns the final vowel of a word, provided that it ends with one.

    The returned vowel may be a simple vowel or a diphthongs.
    If the word doesn't end with a vowel, the empty string is returned.

    The word may use the internal or external spelling, using 'aI' or 'ai' etc. for diphthongs.
    """
    if not word:
        return ''
    substr = word[-2:]
    if substr.lower() in DIPHTHONGS_SET:
        return substr
    substr = word[-1]
    if substr in SIMPLE_VOWELS:
        return substr
    return ''


def initial_vowel(word: str) -> str:
    """This returns the initial vowel of a word, provided that it starts with one.

    The returned vowel may be a simple vowel or a diphthongs.
    If the word doesn't start with a vowel, the empty string is returned.

    The word may use the internal or external spelling, using 'aI' or 'ai' etc. for diphthongs.
    """
    if not word:
        return ''
    substr = word[:2]
    if substr.lower() in DIPHTHONGS_SET:
        return substr
    substr = word[0]
    if substr in SIMPLE_VOWELS:
        return substr
    return ''


def format_compact_string_list(strlist: List[str], prefix: str = '  ', sep: str = ',',
                               max_line_length: int = 78) -> str:
    """Format a string list into a compact way.

    Returns a multi-line string that puts each many items of the list into each line as fit
    without surpassing the 'max_line_length'. Items are separated by 'sep' followed by either
    a space or a newline. Each line is prefixed by 'prefix'.

    Note that the list items themselves are never broken, therefore a very long item might
    cause 'max_line_length' to be surpassed.
    """
    if not strlist:
        return ''

    result_arr = []
    # First item in the first line (appended regardless of length)
    line = prefix + strlist.pop(0) + sep

    for item in strlist:
        if len(line) + len(item) + len(sep) < max_line_length:
            line += ' ' + item + sep
        else:
            # Append full line and start new one
            result_arr.append(line)
            line = prefix + item + sep

    # Discard trailing separator
    result_arr.append(line[:-len(sep)])
    return '\n'.join(result_arr)


def gloss_is_informal(gloss: str) -> bool:
    """Check whether a gloss is informal (just an explanation of a word's origins).

    An informal gloss contains spaces; it doesn't contain a plus sign (+) and it doesn't
    start with an equals sign (=).
    """
    return ' ' in gloss and not ('+' in gloss or gloss.startswith('='))


def normalize(text: str) -> str:
    """Normalize whitespace in a string.

    Any leading or trailing whitespace is discarded; each internal whitespace sequence
    is replaced by a single space.
    """
    return ' '.join(text.strip().split())


def process_tokens(text: str, word_converter: Callable[[str], str],
                   nonword_converter: Callable[[str], str] = lambda token: token) -> str:
    """Tokenize and process a text using one or two callback functions.

    The text is tokenized into words (letter sequences) and non-word tokens between them
    (sequences of whitespace, punctuation, symbols etc.)

    The callback function 'word_converter' is invoked on each word and the return value is
    added to the result.

    The callback function 'nonword_converter' is invoked on each non-word token in the same
    manner. If it's omitted, the token is simply returned unchanged.

    The processed text is joined back into a single string and returned.
    """
    conv_tokens = []
    # Split text in words and non-words between them
    for token in re.split(r'(\W+)', text):
        if token and token[0].isalpha():
            # It's a word
            conv_tokens.append(word_converter(token))
        else:
            conv_tokens.append(nonword_converter(token))
    return ''.join(conv_tokens)


def split_on_commas(text: Optional[str]) -> List[str]:
    """Split a string into its comma-separated parts.

    Whitespace at the beginning and end of all parts is discarded.

    Commas within regular parentheses (such as these) are ignored when splitting.

    If 'test' is None or empty, an empty list will be returned.

    Otherwise, if there are no commas, the returned list will have just one element.
    """
    if not text:
        return []
    text = text.strip()
    need_to_handle_parens = bool(COMMA_IN_PARENS_RE.search(text))
    result = COMMA_RE.split(text)

    if need_to_handle_parens:
        # Remerge elements if the comma occurs within parentheses
        wait_for_end = False
        merged_result: List[str] = []
        for elem in result:
            if wait_for_end:
                # Merge with preceding element
                merged_result[-1] += ', ' + elem
                if ')' in elem and '(' not in elem:
                    wait_for_end = False
            else:
                if '(' in elem and ')' not in elem:
                    wait_for_end = True
                merged_result.append(elem)
        return merged_result

    return result


def split_on_semicolons(text: Optional[str]) -> List[str]:
    """Split a string into its semicolon-separated parts.

    Whitespace at the beginning and end of all parts is discarded.

    If 'text' is None or empty, an empty list will be returned.

    Otherwise, if there are no semicolon, the returned list will have just one element.

    Note that this function doesn't give parentheses any special treatment.
    """
    if not text:
        return []
    text = text.strip()
    return SEMICOLON_RE.split(text)


def split_text_and_explanation(text: str) -> Tuple[str, Optional[str]]:
    """Split a text that ends in explanation enclosed in parentheses into its parts.

    If the text has the form 'main text (explanation'), the parts 'main text' and 'explanation'
    will be returned as a tuple.

    If the text doesn't end in a parentheses, a tuple of the original 'text' followed by None will
    be returned.
    """
    text = text.strip()
    if text.endswith(')') and '(' in text:
        opening_paren = text.rfind('(')
        main_text = text[:opening_paren].strip()
        explanation = text[opening_paren + 1:-1].strip()
        return main_text, explanation
    return text, None


##### Data-related utility functions #####

def coalesce(*args: Optional[T]) -> Optional[T]:
    """Return the first of its arguments that is not None.

    If all arguments are None, then None is returned.
    """
    for arg in args:
        if arg is not None:
            return arg
    return None


def current_datetime() -> str:
    """Return the current date and time as a formatted string, following ISO conventions."""
    return str(datetime.now())[:19]


def extract_key_val(row: Sequence[str], filename: str,
                    ignore_extra_fields: bool = False) -> Tuple[str, str]:
    """
    Extracts a key/value pair from a sequence of strings.

    The first element is treated as key and the second as value.

    Prints a warning if 'row' contains less than two elements.

    Unless 'ignore_extra_fields' is True, this also prints a warning if 'row' contains more
    than two elements.
    """
    fieldcount = len(row)
    if fieldcount != 2:
        if fieldcount < 2 or not ignore_extra_fields:
            warn(f'Error parsing {filename}: Row "{",".join(row)}" has {fieldcount} '
                 'fields instead of 2')
    key = row[0] if fieldcount else ''
    val = row[1] if fieldcount >= 2 else ''
    return (key, val)


def extract_key_val_ignoring_extras(row: Sequence[str], filename: str) -> Tuple[str, str]:
    """Like 'extract_key_val', but with 'ignore_extra_fields' set to True."""
    return extract_key_val(row, filename, True)


def get_elem(seq: Sequence[str], idx: int, default: str = '') -> str:
    """Safely retried a string from a sequence

    'default' is returned if 'seq' ends before the requested 'idx' position.
    """
    if len(seq) > idx:
        return seq[idx]
    return default


##### IO-related utility functions #####

def dump_file(text: str, filename: str) -> None:
    """Store 'text' in a file called 'filename'."""
    with open(filename, 'w', encoding='utf8') as outfile:
        outfile.write(text)


def read_file(filename: str) -> str:
    """Read and return the contents of 'filename'."""
    with open(filename, 'r', encoding='utf8') as file:
        return file.read()


def data_file(local_filename: str) -> str:
    """Return the absolute filename of a file in the data directory.

    The data directory has the name "data" and is a child of the parent directory containing
    this Python module.
    """
    return path.abspath(path.join(path.dirname(path.abspath(__file__)),
                                  '../data/' + local_filename))


def rename_file_if_exists(currentname: str, newname: str) -> None:
    """Rename a file, if it exists. If it doesn't exist, this function does nothing.

    If 'newname' exists already, it will be deleted and overwritten.
    """
    if path.exists(currentname):
        os.replace(currentname, newname)


def rename_to_backup(currentname: str) -> None:
    """Rename a file by adding '.bak' to its name.

    If the file doesn't exist, this function does nothing. If the backup file exists already,
    it will be deleted and overwritten.
    """
    rename_file_if_exists(currentname, currentname + '.bak')


def copy_to_backup(currentname: str) -> None:
    """Create a backup copy of a file that has '.bak' appended to its name.

    If the file doesn't exist, this function does nothing. If the backup file exists already,
    it will be deleted and overwritten.
    """
    if path.exists(currentname):
        copyfile(currentname, currentname + '.bak')


def read_dict_from_csv_file(filename: str, delimiter: str = ',', skip_header_line: bool = True,
                            converter: Callable[[Sequence[str], str], Tuple[str, T]] =
                            extract_key_val) -> Dict[str, T]:  # type: ignore
    """Read a dictionary from a two-column CSV file.

    If 'skip_header_line' is True, the first line is considered a header and skipped.

    The 'converter' function is invoked to convert each row into a key/value pair; it receives
    the 'filename' as second argument, for logging purposes. Defaults to the 'extract_key_val'
    function, which will treat the first field as key and the second as value.

    Prints a warning if any key more than once.
    """
    result: Dict[str, T] = {}
    with open(filename, newline='', encoding='utf8') as csvfile:
        dictreader = csv.reader(csvfile, delimiter=delimiter)

        if skip_header_line:
            next(dictreader)

        for row in dictreader:
            key, val = converter(row, filename)
            if key in result:
                warn(f'Error parsing {filename}: Key {key} occurs more than once')
            else:
                result[key] = val
    return result


##### CLI-related utility functions #####

def process_files(filelist: Sequence[str], text_processor: Callable[[str], str]) -> None:
    """Read each file in 'filelist' and calls 'text_processor' on the contents of each.

    The results are printed to stdout.
    """
    for filename in filelist:
        with open(filename, encoding='utf8') as infile:
            print(text_processor(infile.read()), end='')


def process_stdin(text_processor: Callable[[str], str]) -> None:
    """Call 'text_processor' on input read from stdin and print the result to stdout.

    If stdin is read from a terminal (tty), users can edit and repeat their lines and each line
    will be immediately processed after it has been read.
    """
    if sys.stdin.isatty():
        try:
            while True:
                # We use the input function here, since it allows pressing Arrow-Up to repeat
                # earlier inputs (thanks to the readline module)
                line = input()
                print(text_processor(line))
        except EOFError:
            return
    else:
        print(text_processor(sys.stdin.read()), end='')


def process_text_from_files_or_stdin(text_processor: Callable[[str], str]):
    """Process a series of files or else stdin.

    Calls 'text_processor' on each file or each line read from stdin to do the actual
    processing. Delegates to 'process_files' if one or more file names have been given as
    command-line arguments, to 'process_stdin' otherwise.
    """
    if len(sys.argv) > 1:
        # Read and process each file listed as argument
        process_files(sys.argv[1:], text_processor)
    else:
        # Read and process stdin, possibly interactively
        process_stdin(text_processor)


def retrieve_single_arg(default: Optional[str] = None):
    """Returns a single command-line argument.

    If two or more arguments were specified, exits with an error message.

    If no argument was specified and an explicit 'default' is set, it is returned.
    Otherwise, exits with an error message.
    """
    if len(sys.argv) > 1:
        if len(sys.argv) > 2:
            sys.exit('error: Too many arguments')
        return sys.argv[1]
    if default is not None:
        return default
    sys.exit('error: One argument required, but none given')
