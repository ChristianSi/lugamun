"""Web frontend for Lugamun's dictionary and for the addition of new words."""

import os
from os import path
import logging
import time
from typing import Dict, List, Optional, Sequence, Union

from flask import Flask, render_template, request, redirect
from flask.logging import create_logger
from werkzeug.wrappers import Response

from showstats import create_voc_builder
import util


##### Constants and globals #####

SERVER_SOFTWARE = os.environ.get('SERVER_SOFTWARE', 'flask')


##### App config #####

app = Flask(__name__)  # pylint: disable=invalid-name
LOG = create_logger(app)

# Configure logging
if 'gunicorn' in SERVER_SOFTWARE:
    gunicorn_logger = logging.getLogger('gunicorn.error')  # pylint: disable=invalid-name
    LOG.handlers = gunicorn_logger.handlers
    LOG.setLevel(gunicorn_logger.level)

LOG.info('App ready to serve under %s', SERVER_SOFTWARE)


##### Helper classes #####

class _Concepts:
    """A singleton class that wraps the concepts listed in termdict.txt.

    The concepts are automatically reloaded whenever the underlying files have changed.

    For efficiency, checks whether this is the case are performed at most once every five
    minutes.
    """
    #pylint: disable=too-few-public-methods

    def __init__(self) -> None:
        """Create a new instance."""
        self._last_check_time = time.time()
        self._load_concepts()

    def _load_concepts(self) -> None:
        """Load the list of concepts.

        Also updates the last-modification times of the underlying files.
        """
        self._term_dict_mtime = path.getmtime(util.TERM_DICT)
        self._extra_dict_mtime = path.getmtime(util.EXTRA_DICT)
        sorted_entry_dict = create_voc_builder().sort_entries_by_transcount()
        result_list: List[Dict[str, Union[str | int]]] = []

        for entries in sorted_entry_dict.values():
            for entry in entries:
                word = entry.get('en', '')
                sense = entry.get('sense', '')
                cls = entry.get('class', '')
                transcount = entry.get('transcount', 0)
                if transcount:
                    result_list.append({'word': word,
                                        'sense': sense,
                                        'class': cls,
                                        'transcount': int(transcount)})

        result_list.sort(key=lambda entry: (
            entry['word'].lower(), entry['sense'].lower(), entry['class'].lower()))  # type: ignore
        self._concepts = result_list
        LOG.debug('Concepts (re)loaded')

    def retrieve(self) -> Sequence[Dict[str, Union[str | int]]]:
        """Return the list of concepts.

        When the underlying files have changed, these changes will be picked up after at most
        five minutes and the latest version will be returned. Otherwise, the result is cached,
        so that repeated calls to this method will return very quick.y
        """
        current_time = time.time()
        if current_time - self._last_check_time > 300.0:
            self._last_check_time = current_time
            # Time to check if the underlying files have changed
            if (path.getmtime(util.TERM_DICT) > self._term_dict_mtime
                    or path.getmtime(util.EXTRA_DICT) > self._extra_dict_mtime):
                self._load_concepts()
            else:
                LOG.debug("%s and %s haven't changed", util.TERM_DICT, util.EXTRA_DICT)
        return self._concepts


CONCEPTS = _Concepts()


##### Endpoints for the REST API #####

@app.route('/api/concepts')
def api_concepts() -> Dict:
    """Returns a list of concepts wrapped a JSON object.

    The main object contain a single entry named "concepts" that contains the actual result
    array.

    Each array element is an object with four name/value pairs: "word", "sense", "class"
    (the word class), and "transcount" (an integer giving the number of translations in
    Wiktionary).

    The array is sorted alphabetically, first by word, then by sense and if necessary by class.
    Case is ignored when sorting.
    """
    log_web_event()
    return {'concepts': CONCEPTS.retrieve()}


@app.route('/api/candidates', methods=['POST'])
def api_candidates() -> Dict:
    """Generate and return the candidates for the specified concept.

    Expects a JSON request specifying "word" and "sense".

    Return TODO Document.
    """
    json = request.json
    if not json:
        return {'error': 'No or empty JSON data provided'}

    word = json['word']
    sense = json['sense']
    log_web_event(f'Generating candidates for "{word}" ({sense})')
    # TODO
    return {}


##### Endpoints for the web app #####

# TODO Define a suitable error handler
##@app.errorhandler(404)
##def page_not_found(err):
##    """Return requests for non-existing pages to the start page."""
##    #pylint: disable=unused-argument
##    log_web_event('%s: Redirecting to start page', request.path)
##    return redirect('/')


@app.route('/')
def startpage() -> Response:
    """Main entry point."""
    log_web_event('%s: Redirecting to www.lugamun.org', request.path)
    # Redirecting to naya page for now
    return redirect('/naya')


@app.route('/naya')
def naya() -> str:
    """Render page for the selection of new words to add."""
    log_web_event(request.path)
    return render_template('naya.html')


@app.route("/favicon.ico", methods=['GET'])
def favicon() -> Response:
    """Redirect old browsers which may expect the favicon in the root."""
    # TODO Add favicon in the right place
    log_web_event('/favicon.ico: Redirecting to /static/favicon.ico')
    return redirect('/static/favicon.ico')


##### Helper functions #####

def log_web_event(msg: Optional[str] = None, *args):
    """Log an info message in the context of a web request.

    Any 'args' are merged into 'msg' using the string formatting operator. Additionally, the
    user agent making the request will be added to the logged message.

    If 'msg' is omitted, a default message noticing that the current request path was fetched
    will be logged.
    """
    # pylint: disable=consider-using-f-string, keyword-arg-before-vararg
    if not msg:
        msg = '%s fetched'
        args = (request.path,)

    msg += ' (user agent: %s)'
    agent = request.user_agent
    agent_string = '{}/{} {}'.format(
        agent.platform or '-', agent.browser or '-', agent.version or '-')  # type: ignore

    if agent_string == '-/- -':
        # Log the raw User-Agent header instead (if sent)
        agent_string = '"{}"'.format(agent.string or '')

    args = (*args, agent_string)
    LOG.info(msg, *args)
