#!/usr/bin/env python3
"""Prints a word-for-word gloss of a Lugamun text.

To invoke, either specify one or more files whose contents will be converted.
Or if no filenames are specified, input will be read from stdin.

In either case, the output will be printed to stdout.
"""

import re
from typing import Dict, Optional, Sequence
from warnings import warn

from util import LineDict
import util


##### Main class and entry point #####

class Glosser():
    """Build and return word-for-word glosses for Lugamun."""

    def __init__(self, lang: str = 'en') -> None:
        """Create a new instance."""
        self.prefix = f'{lang}.'
        entries = util.read_dicts_from_file(util.data_file(util.DICT_FILE))
        config = util.read_single_dict_from_file(util.data_file('luglosser.cfg'))
        multigloss_dict = self._build_multigloss_dict(config)
        # Mapping from words to their glosses
        self.wordmap: Dict[str, str] = {}
        # The two previous words
        self.prev = ''
        self.prev_2 = ''

        for entry in entries:
            word = util.normalize(entry.get('word', ''))

            # If a config has a custom gloss entry ("LANGCODE.WORD: ..."), we use it
            custom_gloss = config.get(f'{self.prefix}{word}')

            if not custom_gloss:
                trans = util.split_on_commas(util.normalize(entry.get(lang, '')))
                if not trans:
                    warn(f'No translations found for "{word}"')
                    continue

                # Use as many glosses as configured, defaulting to two
                gloss_count = multigloss_dict.get(word, 2)
                glosses = trans[:gloss_count]
            else:
                glosses = [custom_gloss]

            # A word entry may actually contain several comma-separated synonyms
            synonyms = util.split_on_commas(word)
            is_first = True

            for synonym in synonyms:
                self.add_word(synonym, glosses, is_first)
                is_first = False

        self._warn_unused_entries(multigloss_dict, config)

    def add_word(self, word, glosses, is_first):
        """Add a word to the wordmap.

        'is_first' should be True if this is the only or the first word defined by an entry,
        False if it's an additional synonym. For example, in the entry, "de mi, mi", "de mi"
        is first, while "mi" isn't.
        """
        if word in self.wordmap and not is_first:
            # First entries overwrite existing entries, while others are silently discarded
            # in such cases
            return
        parts = word.split()
        if len(parts) > 4 or '...' in parts or word.startswith('-') or word.endswith('-'):
            # We skip long phrases with 4 or more words, as they are unlikely to need separate
            # glossing, as well as those that include '...' and affixes that start or end with
            # a hyphen (as those won't be found in actual text anyway)
            return
        self.wordmap[word] = self.format_glosses(glosses, word)
        ##print(f 'Glossing "{word}" as "{self.wordmap[word]}"')

    @staticmethod
    def format_glosses(glosses: Sequence[str], word: str) -> str:
        """Format the glosses to use for a word.

        Multiple glosses are separated by slashes, e.g. ["wonder", "marvel"] becomes
        "wonder/marvel".

        If a gloss contains spaces and the word itself doesn't, each space in the gloss is
        converted into a dot, as recommended by the Leipzig Glossing Rules. But if the word
        itself contains spaces, spaces in the gloss are left as is. Hence "give birth" glossing
        "can" becomes "give.birth", but "be born" glossing "bi can" remains unchanged.

        Explanations in parentheses are stripped from the gloss. If nothing remains (the whole
        gloss was set in parentheses, as is the case with some entries such as the object
        marker 'o'), the gloss is discarded. If there are no other non-empty glosses, a warning
        is printed. In such cases, a custom gloss should be defined in the config file.
        By convention, such custom glosses are written in ALL.CAPS.
        """
        # Eliminate parentheses and discard any glosses that are afterwards empty
        glosses = [util.eliminate_parens(gloss) for gloss in glosses]
        glosses = list(filter(None, glosses))
        if not glosses:
            warn(f'No non-empty gloss remains for "{word}"')
        if ' ' not in word:
            # Replace spaces with dots
            glosses = [gloss.replace(' ', '.') for gloss in glosses]
        return '/'.join(glosses)

    def _build_multigloss_dict(self, config: LineDict) -> Dict[str, int]:
        """Determine for which words more or less than two translations should be used as glosses.

        For example, the config may have entries such as "en.1: adil, ajiba, ajibin" specifying
        that only the first translations should be used when glossing each of listed words in
        English (language code "en"). Likewise, "en.3: xi" specifies that the first three
        translations should be used when glossing "xi".

        This method looks first for "{lang}.1" and then for increasingly higher numbers. It
        stops when two sequential numbers are not found. Therefore, if you want to use five
        translations for certain words but never four, you nevertheless need to create an empty
        "{lang}.3" or "{lang}.4" entry (doesn't matter which one), otherwise the "{lang}.5"
        entry won't be found.

        As a special case, "{lang}.3" is also found even if neither a "{lang}.1" nor a
        (redundant) "{lang}.2" entry exists.
        """
        result: Dict[str, int] = {}
        num = 1
        gap_found = False

        while True:
            if num == 2:
                # This is the default, so we can skip it
                num += 1
                gap_found = False

            # Start with the key ending in ".1" (e.g. "en.1") and increment number, stopping
            # when there is no such key and the last one didn't exist either.
            value = config.get(f'{self.prefix}{num}')
            if value is None:
                if gap_found:
                    break
                gap_found = True
                num += 1
                continue

            gap_found = False
            words = util.split_on_commas(util.normalize(value))
            for word in words:
                if word in result:
                    warn(f'Lemma "{word}" listed more than once')
                result[word] = num
            num += 1
        return result

    def _warn_unused_entries(self, multigloss_dict: Dict[str, int], config: LineDict):
        """Print a warning if any of the listed entries doesn't actually exist in the dictionary.
        """
        for word in sorted(multigloss_dict.keys()):
            if word not in self.wordmap:
                warn(f'"{word}" is listed in the config, but doesn\'t exist in the dictionary')
        for entry in sorted(config.keys()):
            if entry.startswith(self.prefix):
                # We only check the entries that start with the language-tag prefix
                word = entry[len(self.prefix):]
                if not (word in self.wordmap or word.isdigit()):
                    warn(f'"{word}" is listed in the config, but doesn\'t exist in the dictionary')

    def gloss_word(self, word: str) -> str:
        """Return the gloss for a single word.

        If the word is not known, it is returned with a question mark in front of it.
        """
        # TODO Also add multi-word glosses (of 2 or 3 words, such as "kosta rika" or
        # "jen korei sude") – probably add as " (= ...)" to the gloss
        # Consult and update self.prev, self.prev_2
        gloss = self.find_in_wordmap(word)
        if gloss is None:
            gloss = '?' + word
        return gloss

    def find_in_wordmap(self, word: str) -> Optional[str]:
        """Find the gloss for a word in the wordmap.

        If the word contains capitalized letters, a lower-cased glossed version will also be
        found, provided that no entry that matches the cased form exists.
        """
        result = self.wordmap.get(word)
        if result is None:
            result = self.wordmap.get(word.lower())
        return result

    def run(self, text: str) -> str:
        """Return a word-for-word gloss of a Lugamun text."""
        collected_tokens = []

        # Add a final newline, if necessary
        if not text.endswith('\n'):
            text += '\n'

        # Split text in words and non-words between them
        for token in re.split(r'(\W+)', text):
            if token and token[0].isalpha():
                # It's a word
                gloss = self.gloss_word(token)
                collected_tokens.append(gloss)
            else:
                collected_tokens.append(token)

        return ''.join(collected_tokens)


if __name__ == '__main__':
    glosser = Glosser()  # pylint: disable=invalid-name
    util.process_text_from_files_or_stdin(glosser.run)
