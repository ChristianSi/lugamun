#!/usr/bin/env python3
"""Checks that the Lugamun dictionary looks OK.

Must be called from the directory containing the dictionary.
"""

from collections import Counter, defaultdict
import csv
import re
import sys
from typing import Dict, FrozenSet, List, Sequence, Set

from buildvoc import DICT_CSV
from util import LineDict
import util


##### Constants and precompiled regexes #####

CLASS_FIELD = 'class'
GLOSS_FIELD = 'gloss'
SAMPLE_FIELD = 'sample'
TAGS_FIELD = 'tags'
VALUE_FIELD = 'value'
WORD_FIELD = 'word'

ALLOWED_CLASSES = frozenset('adj adv aux conj intj name noun num particle phrase prefix prep '
                            'prep_phrase pron quant sel suffix verb'.split())

VERB_TAGS = frozenset('erg ntr tr'.split())
ALLOWED_TAGS = frozenset('core'.split()).union(VERB_TAGS)

# Words ignored by the spellchecker, even though they aren't listed in the dictionary.
IGNORED_WORDS = frozenset('Tina Ben'.split())

# Allowed combinations of word classes. The order of classes does not matter and can vary.
ALLOWED_COMBINATIONS = [
    frozenset('adj intj'.split()),
    frozenset('adv intj'.split()),
    frozenset('aux noun'.split()),
    frozenset('conj prep'.split()),
    frozenset('noun adj'.split()),
    frozenset('noun intj'.split()),
    frozenset('prep prefix'.split()),
    frozenset('quant verb'.split()),
    frozenset('verb noun intj'.split()),
    frozenset('verb noun'.split()),
    frozenset('verb noun suffix'.split()),
    frozenset('verb suffix'.split()),
]

# Some affixes result in words of a specific word class, as per the following listings.
# Note that if the class includes "verb" or "adj", "noun" is always allowed as well, since
# verbs and adjectives can generally be re-used as nouns.
# If a affix can produce words of multiple classes, it may be listed several times (e.g.
# some suffixes can produce either a "noun" or a "name").
PREFIXES_CLASSES: Dict[FrozenSet[str], Sequence[str]] = {
    frozenset('ma we yu'.split()): ['noun']
}

SUFFIXES_CLASSES: Dict[FrozenSet[str], Sequence[str]] = {
    frozenset('bal i nari ori'.split()): ['adj'],
    frozenset('u t'.split()): ['aux'],
    frozenset('astan eria istan lan'.split()): ['name'],
    frozenset('aje eria in isme ja lan nes'.split()): ['noun'],
    frozenset(['i']): 'num pron'.split(),
    frozenset('da ha usa'.split()): ['verb'],
}

ALLOWED_KEYS = frozenset('class gloss infl sample sense tags taxo value word'.split())

# Words of these classes should always be tagged 'core' (though large numerals are exempted)
CORE_CLASSES = frozenset('conj num particle quant sel'.split())

# Words of these classes should always be tagged 'core' unless they contain spaces
SEMI_CORE_CLASSES = frozenset('adv prep pron'.split())

# Key parts that may follow a language code in keys such as 'cmn-latn' and 'en-ipa'
ALLOWED_LANG_CODE_COMPLEMENTS = frozenset('latn ipa'.split())

# An ISO language code: 2 or 3 lowercase ASCII letters
LANG_CODE_RE = re.compile(r'^[a-z]{2,3}$')

# Matches sequences of the characters allowed in numbers (including fractions such as "1/3"
# and examples of power notation such as "10^6")
NUMBER_CHARS_RE = re.compile(r'^[-./^0-9]*$')

# A comma between digits
COMMA_BETWEEN_DIGITS_RE = re.compile(r'\d,\d')

# A comma + space (, ) between digits (both digits are captured)
COMMA_AND_SPACE_BETWEEN_DIGITS_RE = re.compile(r'(\d), (\d)')


##### Types #####

class SpellCheck:
    """A simple spellchecker for texts written in Lugamun."""
    # pylint: disable=too-few-public-methods

    def __init__(self):
        """Create a new instance, population a set of all of Lugamun's known words.

        Limitation: currently multi-word expressions such as "Korea Norte" and "ol ples" are
        skipped, based on the assumption that all their parts are also listed separately. So
        far this is indeed the case, but in the future names may be added whose parts aren't
        usable standalone and hence have no separate entries.
        """
        # Set of Lugamun's words, plus the ignored words
        self.wordset: Set[str] = set(IGNORED_WORDS)
        entries = util.read_dicts_from_file(util.DICT_FILE)
        for entry in entries:
            # An entry may have multiple-comma separates terms such as "wanita, wejen"
            terms = util.split_on_commas(entry.get(WORD_FIELD))
            for term in terms:
                if ' ' not in term:
                    # Multi-word expressions such as "ol ples" are skipped, based on the
                    # assumption that their parts are also listed separately
                    self.wordset.add(term)

    def check(self, text: str) -> Sequence[str]:
        """Spellcheck a text.

        Returns a list of any unknown words – if the list is empty, there were none.

        Unknown words are listed in the order encountered, but each of them is only listed
        once, even if encountered several times. If a word that's capitalized in the
        dictionary (e.g. "Korea") is encountered in lowercase form ("korea"), this form is
        likewise listed as unknown.
        """
        seen_words: Set[str] = set()
        unknown_word_list: List[str] = []

        # Split text in words and non-words between them
        for token in re.split(r'(\W+)', text):
            if token and token[0].isalpha():
                # It's a word
                if token in seen_words:
                    continue  # We inspect each word only once
                if not (token in self.wordset or token.lower() in self.wordset):
                    unknown_word_list.append(token)
                seen_words.add(token)
        return unknown_word_list


##### Main class #####

class DictChecker:
    """"Checks that the Lugamun dictionary and related files don't have formatting issues."""

    def __init__(self):
        """Create a new instance."""
        # Incremented for each error (but not for warnings)
        self._error_count = 0
        # Count related vocabulary of each source language (the entry 'total' counts the
        # number of words derived from source languages)
        self._infl_counter = defaultdict(int)
        # Word class counter
        self._class_counter = Counter()
        self._words_with_multiple_classes = 0

    def print_error(self, msg) -> None:
        """Print an error message to stdout and increment the error count."""
        print('error ' + msg)
        self._error_count += 1

    def check_field_order(self, entry: LineDict, filename: str) -> None:
        """Check that the fields in a dict entry have the expected order.

        Field names with 4 or more letters should come first (e.g. class, sense), followed by
        those with up to three letters (language codes such as en, sw). The latter may be
        followed by hyphenated variants such as "cmn-latn". Within each group, fields should
        be ordered alphabetically.
        """
        prev_key = None
        in_short_group = False

        for key in entry.keys():
            keylen = len(key)
            if not in_short_group and (keylen < 4 or '-' in key):
                in_short_group = True
                prev_key = None
            elif in_short_group and keylen >= 4 and '-' not in key:
                self.print_error(f'in {filename}, line {entry.lineno(key)}: long key "{key}" '
                                 'should be placed before short keys')
            if prev_key and key < prev_key:
                self.print_error(f'in {filename}, line {entry.lineno(key)}: key "{key}" should be '
                                 f'placed before "{prev_key}"')
            prev_key = key

    def compare_entry_order(self, entry1: LineDict, entry2: LineDict, order_fields: Sequence[str],
                            filename: str) -> None:
        """
        Check that two LineDicts occur in the correct alphabetic order.

        Prints an error message if 'entry1' is alphabetically larger than 'entry2', checking
        the contents of the fields given as 'order_fields' for comparison.

        If one of the entries lacks one of these fields and the entry ordering could not
        already be determined, an error message is printed as well (except in cases where
        this would lead to the same error message being printed twice).

        'filename' is used in the error message.
        """
        final_field = order_fields[-1]
        for field in order_fields:
            val1 = entry1.get(field, '').lower()
            val2 = entry2.get(field, '').lower()

            if not val1:
                # Return without error message to avoid printing the same message twice
                return
            if not val2:
                self.print_error(f'in {filename}: entry starting on line {entry2.first_lineno()} '
                                 f'lacks a "{field}" field')
                return
            if val1 > val2:
                self.print_error(f'in {filename}: entries containing the fields "{field}: {val1}" '
                                 f'(line {entry1.first_lineno()}) and "{field}: {val2}" (line '
                                 f"{entry2.first_lineno()}) out of alphabetic order")
                return
            if val1 < val2:
                break  # Everything's fine
            if field == final_field:
                self.print_error(f'in {filename}: entries starting on lines '
                                 f'{entry1.first_lineno()} and {entry2.first_lineno()} contain '
                                 f'the same significant fields ({", ".join(order_fields)})')

    def check_dict_order(self, entries: Sequence[LineDict], filename: str) -> None:
        """Check that entries in a dictionary are ordered alphabetically."""
        prev_entry = None
        for entry in entries:
            self.check_field_order(entry, filename)
            if prev_entry:
                # Check that are sorted correctly, first by 'en' and then by 'sense'
                self.compare_entry_order(prev_entry, entry, ('en', 'sense'), filename)
            prev_entry = entry

    def check_field_names(self, entries: Sequence[LineDict], filename: str) -> None:
        """Check that the specified file doesn't contain any unexpected keys (field names)."""
        for entry in entries:
            for key in entry.keys():
                if key in ALLOWED_KEYS or LANG_CODE_RE.match(key):
                    continue
                if '-' in key:
                    first, second = key.split('-', maxsplit=1)

                    # This matches keys such as 'cmn-latn' and 'en-ipa'
                    if LANG_CODE_RE.match(first) and second in ALLOWED_LANG_CODE_COMPLEMENTS:
                        continue
                self.print_error(f'in {filename}, line {entry.lineno(key)}: unexpected field '
                                 f'"{key}"')

    def check_field_contents(self, entries: Sequence[LineDict], filename: str) -> None:
        """Check that the fields in the specified file don't contain spurious whitespace or commas.
        """
        for entry in entries:
            for key, value in entry.items():
                parts = util.split_on_commas(value)
                if '' in parts:
                    self.print_error(f'in {filename}, line {entry.lineno(key)}: spurious commas in '
                                     f'"{key}: {value}"')
                jointed_parts = ', '.join(parts)
                # Commas within large numbers such 1,000,000 are fine
                if COMMA_BETWEEN_DIGITS_RE.search(value):
                    jointed_parts = COMMA_AND_SPACE_BETWEEN_DIGITS_RE.sub(r'\1,\2', jointed_parts)
                if jointed_parts != value:
                    self.print_error(f'in {filename}, line {entry.lineno(key)}: spurious '
                                     f'whitespace around commas in "{key}: {value}"')
                norm_value = re.sub(r'\s+', ' ', value)
                if norm_value != value:
                    self.print_error(f'in {filename}, line {entry.lineno(key)}: spurious '
                                     f'whitespace in "{key}: {value}"')

    def check_value_field(self, entries: Sequence[LineDict], filename: str) -> None:
        """Check the 'value' fields in a file.

        The rules for 'value' fields are as follows:

        1. Only num's (numbers) must have such a field.
        2. All num's must have such a field.
        3. Its value must be parsable as a number.
        4. Fractions may be noted as "x/y", e.g. "1/3", and powers of ten may be noted as
           "10^x", e.g. "10^6".
        """
        for entry in entries:
            class_list = util.split_on_commas(entry.get(CLASS_FIELD, ''))
            raw_value = entry.get(VALUE_FIELD)

            if 'num' in class_list:
                if raw_value:
                    if not NUMBER_CHARS_RE.match(raw_value):
                        self.print_error(f'in {filename}, line {entry.lineno(VALUE_FIELD)}: '
                                         f'"{raw_value}" is not a number')
                else:
                    self.print_error(f'in {filename}: entry starting on line {entry.first_lineno()}'
                                     f' describes a number, but lacks a "{VALUE_FIELD}" field')
            else:
                if raw_value is not None:
                    self.print_error(f'in {filename}, line {entry.lineno(VALUE_FIELD)}: '
                                     f'unexpected field "{VALUE_FIELD}" (entry doesn\'t describe '
                                     'a number)')

    def spellcheck_sample_field(self, entries: Sequence[LineDict], filename: str) -> None:
        """Check that all words used in 'sample' fields are known."""
        spellchecker = SpellCheck()

        for entry in entries:
            sample_text = entry.get(SAMPLE_FIELD)
            if not sample_text:
                continue
            unknown_words = spellchecker.check(sample_text)
            if unknown_words:
                self.print_error(f'in {filename}, line {entry.lineno(SAMPLE_FIELD)}: unknown '
                                 f'word(s) used in "{SAMPLE_FIELD}" field: '
                                 + ', '.join(unknown_words))

    def check_word_classes(self, entries: Sequence[LineDict], filename: str) -> None:
        """Check that all entries in the specified file belong to the expected word classes.

        Entries may belong to multiple word classes. In this case we also check that there are
        no duplicates and that the combination is among the expected combinations.
        """
        for entry in entries:
            class_val = entry.get(CLASS_FIELD, '')
            if not class_val:
                self.print_error(f'in {filename}: entry starting on line {entry.first_lineno()} '
                                 f'lacks a "{CLASS_FIELD}" field')
                continue

            self._check_commasep_values(class_val, ALLOWED_CLASSES, False, CLASS_FIELD, entry,
                                        filename)
            class_set = frozenset(util.split_on_commas(class_val))

            # Check combinations
            if len(class_set) > 1 and class_set not in ALLOWED_COMBINATIONS:
                self.print_error(f'in {filename}, line {entry.lineno(CLASS_FIELD)}: unexpected '
                                 f'combination of classes: {class_val}')

    def _check_commasep_values(self, fieldval: str, expected_values: FrozenSet[str],
                               check_order: bool, fieldname: str, entry: LineDict,
                               filename: str) -> None:
        """Run checks on a field containing comma-separated values.

        Specifically, this checks:

        * That there are no duplicates.
        * That all values are from 'expected_values'.

        If 'check_order' is true, we also check that all values are given in alphabetic order.
        """
        # pylint: disable=too-many-arguments
        value_list = util.split_on_commas(fieldval)
        value_set = frozenset(value_list)

        # The set shouldn't be shorter than the list, otherwise there are duplicates
        if len(value_set) < len(value_list):
            dup_count = len(value_list) - len(value_set)
            plur = 's' if dup_count > 1 else ''
            self.print_error(f'in {filename}, line {entry.lineno(fieldname)}: {fieldname} '
                             f'field "{fieldval}" contains {dup_count} duplicate{plur}')
        # If we subtract all the allowed values, none should remain
        unexpected_values = value_set - expected_values
        if unexpected_values:
            plur = 's' if len(unexpected_values) > 1 else ''
            self.print_error(f'in {filename}, line {entry.lineno(fieldname)}: unexpected '
                             f'values{plur} in {fieldname} field: {", ".join(unexpected_values)}')

        if check_order and len(value_set) > 1:
            # Check alphabetic order
            liststr = ', '.join(value_list)
            sorted_liststr = ', '.join(sorted(value_list))
            if liststr != sorted_liststr:
                self.print_error(f'in {filename}, line {entry.lineno(fieldname)}: values in '
                                 f"{fieldname} field aren't sorted alphabetically: {fieldval}")

    def _do_check_affixed_word_classes(self,
                                       class_val: str,
                                       affix: str,
                                       is_prefix: bool,
                                       lineno: int,
                                       filename: str) -> None:
        """Check that the word class of a prefixed or suffixes word matches the affix."""
        # pylint: disable=too-many-arguments
        class_set = set(util.split_on_commas(class_val))
        affix_dict = PREFIXES_CLASSES if is_prefix else SUFFIXES_CLASSES
        error_desc = None

        for affixes, expected_classes in affix_dict.items():
            if affix in affixes:
                if class_val in expected_classes:
                    return  # God a match

                alternative_class_set = None
                if 'verb' in expected_classes or 'adj' in expected_classes:
                    # If the class is "verb" or "adj", "noun" is always allowed as well,
                    # since verbs and adjectives can generally be re-used as nouns.
                    alternative_class_set = set(expected_classes)
                    alternative_class_set.add('noun')
                    if class_set == alternative_class_set:
                        return  # God a match

                word_desc = f'starting with {affix}-' if is_prefix else f'ending with -{affix}'
                error_desc = (f'in {filename}, line {lineno}: unexpected class "{class_val}" '
                              f'for compound {word_desc}')

        if error_desc:
            # We didn't get a good match, only an error, so we have to print it
            self.print_error(error_desc)

    def check_affixed_word_classes(self, entries: Sequence[LineDict], filename: str) -> None:
        """Check that an affixed word has the expected word class.

        Some of the affixes always result in a word belonging to a specific class.
        This check ensures that all entries actually comply with these expectations.
        """
        for entry in entries:
            gloss = entry.get(GLOSS_FIELD, '')
            if not gloss or '+' not in gloss:
                continue

            parts = gloss.split('+')
            class_val = entry.get(CLASS_FIELD, '')
            lineno = entry.lineno(CLASS_FIELD)
            self._do_check_affixed_word_classes(class_val, parts[0], True, lineno, filename)
            self._do_check_affixed_word_classes(class_val, parts[-1], False, lineno, filename)

    def _find_parts(self, entry: LineDict, known_words: Set[str]) -> Set[str]:
        """Find and return the parts of a compound.

        If this is a root, an empty set will be returned. Otherwise a set of all its parts
        will be returned. This set may have just one member if the compound is formed through
        reduplication (e.g. the sole part of "gengen" is "gen"), otherwise it will have two
        or more member.

        Parts include both any parts listed as gloss (e.g. the parts of "sugo" are "su, "go")
        as well as any words making up a multi-word entry (e.g. the parts of "fa hau" are
        "fa", "hau"). If an entry consists of comma-separated alternatives, both word parts
        and gloss parts will be returned, e.g. the parts of "bina prehukum, neprehukumi" are
        "bina", "prehukum", "ne", "prehukumi").

        'known_words' is the set of all known words. It may include prefixes (such as "yu-")
        and suffixes (such as "-in"). If the first part of a gloss is only listed as affix
        starting or ending with a hyphen, that form will be returned (e.g. "yu-" instead
        of "yu").
        """
        result = set()
        lm_word = entry.get(WORD_FIELD, '')
        gloss = entry.get(GLOSS_FIELD, '')

        if ' ' in lm_word:
            # There may be comma-separated alternatives, which we check separately
            alternatives = util.split_on_commas(lm_word)
            for alternative in alternatives:
                if ' ' in alternative:
                    # Sometimes a capitalized part may be listed in the dictionary in lower-case
                    # form, which we return instead (e.g. the second part of "Korea Norte" is
                    # "norte", not "Norte")
                    parts = alternative.split()
                    for part in parts:
                        if part not in known_words and part.lower() in known_words:
                            part = part.lower()
                        result.add(part)

        if gloss and not util.gloss_is_informal(gloss):
            if '(' in gloss:
                # Remove parentheses, but not their content, changing e.g the gloss
                # "Jupi(ter)+den" to "Jupiter+den"
                gloss = gloss.replace('(', '').replace(')', '')
            if gloss.startswith('='):
                result.add(gloss[1:])
            elif '+' in gloss:
                gloss_parts = gloss.split('+')
                # First or last part may be an affix
                first_part = gloss_parts[0]
                if first_part not in known_words and f'{first_part}-' in known_words:
                    gloss_parts[0] = f'{first_part}-'
                last_part = gloss_parts[-1]
                if last_part not in known_words and f'-{last_part}' in known_words:
                    gloss_parts[-1] = f'-{last_part}'
                result.update(gloss_parts)
        return result

    def check_core_tagging(self, entries: Sequence[LineDict], filename: str) -> None:
        """Check that the "core" tag is used consistently."""
        # pylint: disable=too-many-branches
        core_words: Set[str] = set(['...'])  # '...' is used in some words and considered as core
        non_core_words: Set[str] = set()

        # Group in core vs. non-core words
        for entry in entries:
            word = entry.get(WORD_FIELD, '')
            taglist = util.split_on_commas(entry.get(TAGS_FIELD, ''))
            alternatives = []

            if ',' in word:
                # Also add all comma-separated alternatives (e.g. "prefer, mas suki") to the
                # correct set
                alternatives = util.split_on_commas(word)

            if 'core' in taglist:
                core_words.add(word)
                core_words.update(alternatives)
            else:
                non_core_words.add(word)
                non_core_words.update(alternatives)

        known_words = core_words.union(non_core_words)
        # Iterate entries again to check if core tagging is consistent
        for entry in entries:
            word = entry.get(WORD_FIELD, '')
            parts = self._find_parts(entry, known_words)
            if not parts:
                continue
            first_lineno = entry.first_lineno()

            # Warn if a part is totally unknown
            unknown_parts = parts.difference(known_words)
            if unknown_parts:
                self.print_error(f'in {filename}, line {first_lineno}: Some parts of "{word}" are '
                                 f'unknown: {", ".join(unknown_parts)}')

            if word in core_words:
                # If a core word has a gloss, every part must be tagged "core" too
                for part in parts:
                    if part in non_core_words:
                        self.print_error(f'in {filename}, line {first_lineno}: entry "{word}" '
                                         f'is tagged "core", but its part "{part}" isn\'t')
            elif not unknown_parts:
                # If all the parts of a word are tagged "core", it should be too
                if not parts.intersection(non_core_words):
                    self.print_error(f'in {filename}, line {first_lineno}: "{word}"  isn\'t tagged '
                                     f'"core", though all its parts are')

    def check_tags(self, entries: Sequence[LineDict], filename: str) -> None:
        """Check the contents of the "tags" field."""
        for entry in entries:
            tags_val = entry.get(TAGS_FIELD, '')
            if tags_val:
                self._check_commasep_values(tags_val, ALLOWED_TAGS, True, TAGS_FIELD, entry,
                                            filename)

                # Only "verb" entries may have verb tags and no entry may have several of them
                taglist = util.split_on_commas(tags_val)
                used_verb_tags = VERB_TAGS.intersection(taglist)

                if len(used_verb_tags) > 1:
                    self.print_error(f'in {filename}, line {entry.lineno(TAGS_FIELD)}: '
                                     f'verb tags {", ".join(sorted(used_verb_tags))} cannot be '
                                     'combined')
                elif used_verb_tags:
                    classlist = util.split_on_commas(entry.get(CLASS_FIELD, ''))
                    if 'verb' not in classlist:
                        tag, = used_verb_tags
                        self.print_error(f'in {filename}, line {entry.lineno(TAGS_FIELD)}: '
                                         f'non-verb shouldn\'t be tagged "{tag}"')
            else:
                # Words of certain classes should always be tagged 'core'
                class_val = entry.get(CLASS_FIELD, '')
                word = entry.get(WORD_FIELD, '')
                if class_val in CORE_CLASSES or (class_val in SEMI_CORE_CLASSES
                                                 and ' ' not in word):
                    if class_val == 'num' and word.endswith('alyon'):
                        # Large numerals using the -alyon suffix are exempted
                        continue
                    self.print_error(f'in {filename}, line {entry.first_lineno()}: entry should be '
                                     'tagged "core", but isn\'t')

    def check_csv_duplicates(self) -> None:
        """Check that there are no duplicated field values in the CSV file."""
        with open(DICT_CSV, newline='', encoding='utf8') as csvfile:
            reader = csv.reader(csvfile)
            en_values: Set[str] = set()
            lugamun_values: Set[str] = set()

            for lineno, fields in enumerate(reader, start=1):
                en_value = fields[0]
                lugamun_value = fields[1]
                if en_value in en_values:
                    self.print_error(f'in {DICT_CSV}, line {lineno}: repeated English entry '
                                     f'"{en_value}"')
                en_values.add(en_value)
                if lugamun_value in lugamun_values:
                    self.print_error(f'in {DICT_CSV}, line {lineno}: repeated Lugamun entry '
                                     f'"{lugamun_value}"')
                lugamun_values.add(lugamun_value)

    def run(self) -> int:
        """Main function: perform a serious of useful checks.

        Returns the number of errors found -- if 0, everything is fine.
        """
        # Each file is read only once for efficiency
        dict_entries = util.read_dicts_from_file(util.DICT_FILE)
        extradict_entries = util.read_dicts_from_file(util.EXTRA_DICT)

        # This test is only run on the extradict since the other one is automatically printed
        # in the expected order
        self.check_dict_order(extradict_entries, util.EXTRA_DICT)

        # These tests are run on both dict files
        self.check_field_names(dict_entries, util.DICT_FILE)
        self.check_field_names(extradict_entries, util.EXTRA_DICT)
        self.check_field_contents(dict_entries, util.DICT_FILE)
        self.check_field_contents(extradict_entries, util.EXTRA_DICT)

        # These tests are only run on the output dict
        self.check_value_field(dict_entries, util.DICT_FILE)
        self.spellcheck_sample_field(dict_entries, util.DICT_FILE)
        self.check_word_classes(dict_entries, util.DICT_FILE)
        self.check_affixed_word_classes(dict_entries, util.DICT_FILE)
        self.check_tags(dict_entries, util.DICT_FILE)
        self.check_core_tagging(dict_entries, util.DICT_FILE)

        # Test on dict.csv
        self.check_csv_duplicates()
        return self._error_count


##### Main entry point #####

if __name__ == '__main__':
    # pylint: disable=invalid-name
    checker = DictChecker()
    errorcount = checker.run()

    if errorcount:
        # Signal failure by returning the error count as exit code (but capped at 127
        # since many systems expect errors codes to be within this range)
        exitcode = min(127, errorcount)
        sys.exit(exitcode)
