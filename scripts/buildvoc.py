#!/usr/bin/env python3
"""Build our vocabulary, generating and selecting suitable candidate words.

This must be invoked repeatedly in the data/ directory, to add one word after the other.

Words in Wiktionary are placed in three groups: (1) nouns, (2) adjectives and adverbs, (3)
verbs and other words. By default, the word with the highest number of translations in
Wiktionary not yet in the dictionary from the currently least represented group will be added
next. (Nouns are particularly less representing in Wiktionary and many of them have a high
number of translations, so we use this grouping to avoid creating a dictionary that's made up
of nouns.) The --add command-line (CLI) option can be used to select another word for addition
instead.

Each run proposes a candidate for selection, but a suitable CLI option must be specified to
confirm or revise this choice.

The algorithm is as follows:

* Read termdict.txt.
* Read extradict.txt and merge the entries into the termdict entries that have the same
  English (en) word and sense. This can be used to supply missing translations and to
  correct translations. If entries in this file have field "tags" with the value "add",
  the entry will be added even if there is no corresponding entry in the main termdict.txt
  file – this allows supplying entries missing altogether.
* If no other word was specified, the not-yet-handled word with the highest number of
  translations becomes to next word to be added (according to the grouping explained above).
  In case of a tie, candidate proposals for generated for each word and the one with the
  alphabetically first candidate is selected.
* Candidate proposals are generated for the selected word and the combined penalty for each
  candidate is calculated, following the algorithm described in
  https://www.reddit.com/r/auxlangs/comments/mlf8h8/vocabulary_selection_for_a_worldlang/.
* Candidates are sorted by their penalty (the lower, the better). By default, the first one
  will be added, but the user has to confirm this and they also also select another one to
  be added instead -- in that case, a reason for the choice must be given.

Requires Python 3.7+ and the editdistance library.
"""
# pylint: disable=too-many-lines

from __future__ import annotations
import argparse
from collections import defaultdict
import csv
from enum import Enum
from dataclasses import dataclass, field
from datetime import datetime
from functools import lru_cache
from operator import itemgetter
from os import path
import re
import sys
import textwrap
from typing import Counter, Dict, FrozenSet, List, Match, Optional, Sequence, Set, Tuple

import editdistance

from util import LineDict
import util


##### Constants and precompiled regexes #####

# Used for logging
LOG = util.RecordingLogger()

# Names of input and output files
DICT_FILE = util.DICT_FILE
DICT_CSV = 'dict.csv'
SELECTION_LOG = 'selectionlog.txt'

# Dummy word used for the --polycheck argument
POLY_DUMMY = 'dummy'
# Tag used for languages that aren't default source languages
OTHER = 'others'

# The number of entries to keep. The actual number may be somewhat higher, as we keep
# the entries that have more than a certain number of translations
TARGET_ENTRY_COUNT = 1000

# Conversion dictionaries used to generate candidates
CONV_DICTS = sorted('ar es hi fr id ipa ja pinyin ru sw taxo'.split())

# Codes of the five most widely spoken languages (sorted by speaker count)
TOP_5_CODES = 'en cmn hi ar es '.split()

# Codes of our additional source languages (sorted by speaker count)
ADDITIONAL_SOURCE_LANG_CODES = 'fr ru id ja sw'.split()

# Codes of the source languages for which we generate candidates (sorted by speaker count)
SOURCE_LANG_CODES = TOP_5_CODES + ADDITIONAL_SOURCE_LANG_CODES

# Word classes of content words (adverbs are deliberately no longer included here, since
# pure adverbs are rare and may well be short)
CONTENT_CLASSES = frozenset('adj name noun verb'.split())

# Regex patterns describing how a syllable in a candidate word should look like
INITIAL_CONSONANTS = 'bcdfghjklmnprstvxy'
FINAL_CONSONANTS = util.FINAL_CONSONANTS
# Consonant pairs allowed at the start of syllables
INITIAL_PAIRS = frozenset(
    'bl br by cv cy dr dv fl fr fy gl gr gv hv kl kr kv ky my ny pl pr py sl sv tr tv xv xy'
    .split())
SECOND_CONSONANTS = 'lrvy'
ALL_CONSONANTS = INITIAL_CONSONANTS
ALL_CONSONANTS_RE = re.compile(rf'[{ALL_CONSONANTS}]')
NON_FINAL_CONSONANTS = 'bcdfghjkpvxy'      # Consonants not allowed at the end of a syllable
NOT_SECOND_CONSONANTS = 'bcdfghjkptmnsx'   # Consonants not allowed after another one
SIMPLE_VOWELS = util.SIMPLE_VOWELS
SIMPLE_VOWEL_RE = re.compile(rf'[{SIMPLE_VOWELS}]')
CAND_VOWEL_FRAG = f'(?:a[IU]|oI)|[{SIMPLE_VOWELS}ə]'
# Capturing and non-capturing regexes matching a candidate vowel
CAND_VOWEL_RE_CAPT = re.compile('(' + CAND_VOWEL_FRAG + ')')
CAND_VOWEL_RE_NONCAPT = re.compile('(?:' + CAND_VOWEL_FRAG + ')')
# The two consonants are captured in two subgroups
ILLEGAL_CONS_PAIR_RE = re.compile(f'([{NON_FINAL_CONSONANTS}])([{NOT_SECOND_CONSONANTS}])')
# Matches 'ks' at the start of words and after vowels.
KS_COMBI_RE = re.compile(r'(?:\b|(?<=[aeiouəIU]))ks')
# Matches 'ts' at the start of words and after final consonants.
TS_COMBI_RE = re.compile(rf'(?:\b|(?<=[{FINAL_CONSONANTS}]))ts')
# R or S followed by a nasal and or T and another consonant that's not allowed here -- the first
# two are captured in one subgroup, the final consonant in another
ILLEGAL_RN_TRIPLE_RE = re.compile(f'([rs][mnt])([{NOT_SECOND_CONSONANTS}])')
# Matches sequences of a syllable-final consonants followed by one of 'sk/sp/st'
# -- the last two are captured (and hence kept) together; in the case of 'st', another letter
# must follow (since word-final 'set' is fine, but 'est' isn't)
ILLEGAL_SKPT_TRIPLE_RE = re.compile(f'([{FINAL_CONSONANTS}])(s[kp]|st.)')
# Sequences of three consonants at the start of a word, the middle of which is a final
# consonant – the first one and the last two are captured in separate subgroups, since we
# can insert a filler vowel between them
INITIAL_CONS_TRIPLE_RE = re.compile(
    rf'\b([{INITIAL_CONSONANTS}])([{FINAL_CONSONANTS}][{INITIAL_CONSONANTS}])')
# Sequences of two syllable-final consonants followed by another one are also illegal
# -- the first two and the last one are captured in separate subgroups
ILLEGAL_CONS_TRIPLE_RE = re.compile(
    f'([{FINAL_CONSONANTS}][{FINAL_CONSONANTS}])([{NOT_SECOND_CONSONANTS}])')
# If two consonants that might start a syllable are followed by another one, we keep the first
# two together (first subgroup)
ILLEGAL_CONS_TRIPLE2_RE = re.compile(
    f'([{INITIAL_CONSONANTS}][{SECOND_CONSONANTS}])([{ALL_CONSONANTS}])')
# 'iy' or 'ny' before another consonant -- the first and last letters are captured in subgroups
INY_BEFORE_CONSONANT = re.compile(f'([in])y([{INITIAL_CONSONANTS}])')
# Non-final consonant (1st group) followed by 'x' followed by another consonant (2nd group) --
# this requires TWO filler vowels
X_BETWEEN_CONSONANT = re.compile(rf'([{NON_FINAL_CONSONANTS}])x([{INITIAL_CONSONANTS}])')
# A double consonant such as 'nn' or 'rr' -- the consonant itself is captured in a subgroup
DOUBLE_CONS_RE = re.compile(rf'([{ALL_CONSONANTS}])\1')
# Matches 'n' and 'r' between Pinyin vowels (lowercase only) – the first vowel as captured in
# a group, the rest in another. Also captured the sequence 'nr' in this position, which will be
# divided between the two groups.
NR_BETWEEN_VOWELS = re.compile(r'([aeiouü]n?)([nr][aeiouü])')
# Matches 'i' and 'u' (first group) before another Spanish vowel (second group) -- but not in
# cases where another semivowel is allowed after two consonants
IU_BEFORE_VOWEL = re.compile(
    rf'(?<![{INITIAL_CONSONANTS}][{SECOND_CONSONANTS}])([iu])([aeiouáéíóú])')
# 'h' before another consonant, excluding semivowels (the consonant is captured)
H_BEFORE_CONSONANT = re.compile('h([bcdfghjklmnprstx])')
# Pairs ending a "second consonant" that are no longer allowed by our tightened phonology
ILLEGAL_SECOND_CONS_PAIRS = re.compile(
    rf'(?:bv|[cx][lr]|d[ly]|fv|gy|h[lry]|pv|[jvy][{SECOND_CONSONANTS}])')
# Ditto, but matching pairs that start with a "final consonant" and are hence only illegal
# after other consonants or at the start of words
ILLEGAL_SECOND_CONS_PAIRS2 = re.compile(
    rf'(?:^|(?<=[{INITIAL_CONSONANTS}]))'
    rf'(?:lr|lv|ly|ml|mr|mv|nl|nr|nv|sr|sy|t[ly]|[r][{SECOND_CONSONANTS}])')
# An R-colored schwa at the end of English words (in IPA)
FINAL_R_COLORED_SCHWA = re.compile(r'(ɚ|əɹ|\(ə\)ɹ)$')
# Words ending in 'er' after a single final consonant
E_AFTER_FINAL_CONSONANT = re.compile(rf'[^{ALL_CONSONANTS}][{FINAL_CONSONANTS}]e$')
# Matches 'c' before any character except 'e' and 'i'
C_BEFORE_NON_EI = re.compile(r'c(?![ei])')
# Matches 'ng' not followed by a vowel of one of the consonants allowed after 'g'
ILLEGAL_NG = re.compile(rf'ng(?![lrv{SIMPLE_VOWELS}])')
# Matches the I/U of a diphthong before a vowel
DIPHTHONG_I_BEFORE_VOWEL = re.compile(rf'I(?=[{SIMPLE_VOWELS}])')
DIPHTHONG_U_BEFORE_VOWEL = re.compile(rf'U(?=[{SIMPLE_VOWELS}])')

# Clusters listed here are avoided and instead replaced with the listed value
AVOIDED_CLUSTERS = {
    'ry': 'ri',
    'sy': 'si',
    'ty': 'ti',
}

### A semivowel is inserted between vowel pairs that would otherwise look like diphthongs
## VOWEL_PAIR_DISAMBIGUATION = {
##    'ai': 'ayi',
##    'au': 'avu',
##    'oi': 'oyi',
## }

# Vowels with acute accents (incl. y), used for some languages
ACUTE_ACCENTS = 'áéíóúý'
ACUTE_ACCENT_RE = re.compile(rf'[{ACUTE_ACCENTS}]')
# Equivalent of the above, but without accents
VOWELS_WO_ACCENT = 'aeiouy'
VOWEL_WO_ACCENT_RE = re.compile(rf'[{VOWELS_WO_ACCENT}]')

# A normal vowel followed by one marked with an acute accent -- both captured as groups
ACUTE_ACCENT_AFTER_VOWEL = re.compile(rf'([{VOWELS_WO_ACCENT}])([{ACUTE_ACCENTS}])')

# Sequences of common punctuation
PUNCTUATION_RE = re.compile(r'[-,.;:!?"/()]+')

# Replacements needed to convert the internal form used for candidate word to the external form
# used in the dictionary.
EXPORT_REPL = {
    'ai': "a'i",
    'aI': 'ai',
    'au': "a'u",
    'aU': 'au',
    'oi': "o'i",
    'oI': 'oi',
    'ə': 'e',
}

# Replacements needed for normalization (to avoid minimal pairs).
NORM_REPL = {
    'j': 'c',
    'v': 'u',
    'y': 'i',
    "'": '',
}

# Enum for grouping all words into three kinds
Kind = Enum('Kind', 'NOUN ADJ VERB')  # pylint: disable=invalid-name

KIND_DESC = {
    Kind.NOUN: 'a noun',
    Kind.ADJ: 'an adjective or adverb',
    Kind.VERB: 'a verb or other word',
}


##### Types #####

@dataclass
class Conversion():
    """The result of a phonetic conversion."""
    output: str
    penalty: bool  # Whether a penalty should be applied


@dataclass
class Candidate():
    """A word in (nearly) our phonology considered a candidate for selection.

    The phonology used here has a few differences to the generally used one:

    * The diphthongs are written aI, aU, oI to distinguish them from sequences of two vowels
    * "ng" is written N to follow the "one sound, one letter" principle
    * Vowels inserted to fit our phonology are written "ə" instead of "e" – such vowels
      are ignored when comparing similarities between candidate pairs
    * No apostrophes are used (may otherwise be necessary for disambiguation in a few cases)
    """
    # pylint: disable=too-many-instance-attributes
    word: str
    penalty: int  # The penalty incurred when converting the source word into our phonology
    lang: str               # The code of the language from which the candidate was adapted
    original: str = ''      # The original word or romanization (empty if not set)
    raw_psim: float = -1.0  # The raw similarity penalty (distance to other candidates)
    psim: float = -1.0      # The similarity penalty, normalized to [0..1]
    pinf: float = -1.0      # The influence penalty (relative influence of the source language)
    related_cands: Dict[str, List[Candidate]] = field(
        default_factory=lambda: defaultdict(list), compare=False)
    filled = False      # set to True by insert_filler_vowels

    @property
    def pcon(self) -> float:
        """The conversion penalty PC, calculated by diving the raw (conversion) penalty by 5."""
        return self.penalty / 5.0

    @property
    def total_penalty(self) -> float:
        """The total penalty: sum of pcon, psim, and pinf."""
        return self.pcon + self.psim + self.pinf

    def export_word(self) -> str:
        """Return the external form of the word that will be used in the dictionary."""
        return export_word(self.word)

    def find_langs_with_identical_candidate(self) -> Set[str]:
        """Find languages whose candidate are identical to this one.

        This checks the 'related_cands' field and returns a set of zero or more language codes.
        """
        result = set()
        word = self.export_word()

        for lang, cand_list in self.related_cands.items():
            for cand in cand_list:
                if cand.export_word() == word:
                    result.add(lang)
                    break

        return result

    @staticmethod
    def resolve_illegal_pairs(match: Match) -> str:
        """Helper function replacing an illegal consonant pair ending in a second consonant.

        The patched string must have exactly two consonants, the second of which must be one
        of the SECOND_CONSONANTS.

        If the second consonant is 'v' or 'y', it will be replaced by the corresponding vowel
        ('u' or 'i').

        Otherwise 'ə' will be inserted in front of both vowels if the first if them is allowed
        to end a syllable; otherwise it will be inserted between them.
        """
        match_group = match.group()
        if len(match_group) != 2 or match_group[-1] not in SECOND_CONSONANTS:
            raise ValueError(f'Invalid argument for resolve_illegal_pairs: "{match_group}"')
        first, second = match_group
        if second == 'v':
            return first + 'u'
        if second == 'y':
            return first + 'i'
        if first in FINAL_CONSONANTS:
            return 'ə' + match_group
        return first + 'ə' + second

    def insert_filler_vowels(self) -> None:
        """Insert filler vowels where necessary to make the phonology of this candidate valid.

        The penalty is increased accordingly.

        Also cleans up the phonology in some other ways, e.g. by simplifying 'tx' to 'c' and
        by eliminating double consonants.

        Note that this method will run only ONCE per candidate – subsequent invocations will
        simply do nothing. The reason for this is that otherwise the distortion penalties
        would be overcounted.
        """
        # pylint: disable=too-many-branches, too-many-statements
        if self.filled:
            return  # Avoid having this method run twice
        # Replace punctuation by spaces
        text = PUNCTUATION_RE.sub(' ', self.word)
        words = text.split()
        new_words = []

        for word in words:
            # Replace avoided clusters with their replacement version
            for cluster, repl in AVOIDED_CLUSTERS.items():
                word = word.replace(cluster, repl)

            # Simplify tx/tc to c
            word = word.replace('tx', 'c').replace('tc', 'c')
            # Likewise 'iy' to 'i' (including in diphthongs); 'uv' becomes 'u' only in
            # diphthongs, since 'v' often comes from /v/ which shouldn't be dropped completely
            # in other cases (e.g. English 'move' should become 'muvə' rather than just 'mu')
            word = word.replace('iy', 'i').replace('Iy', 'I').replace('Uv', 'U')

            # Eliminate double consonants
            word = DOUBLE_CONS_RE.sub(r'\1', word)

            # Before vowels, the second part of the diphthong is replaced with the corresponding
            # semivowel, for a more consistent treatment of such sequences
            word = DIPHTHONG_I_BEFORE_VOWEL.sub('y', word)
            word = DIPHTHONG_U_BEFORE_VOWEL.sub('v', word)

            # 'iy' and 'ny' before a consonant are simplified by dropping the 'y'
            # (without a penalty, since the pronunciation difference is pretty small)
            word = INY_BEFORE_CONSONANT.sub(r'\1\2', word)

            # 'ks' at the start of words and after vowels is simplified to 's' (incurs a
            # penalty since one sound is lost)
            word, count = KS_COMBI_RE.subn('s', word)
            self.penalty += count

            # 'ts' at the start of words and after final consonants is simplified to 's'
            # (incurs a penalty since one sound is lost)
            word, count = TS_COMBI_RE.subn('s', word)
            self.penalty += count

            # Initial 'ts' is simplified to 's' (also with penalty)
            if word.startswith('ts'):
                word = word[1:]
                self.penalty += 1

            # Final 'ng' is simplified to 'n' (without penalty, since the 'g' is hardly
            # audible in this position and in some cases it's just a mistranscription)
            # – except in French words such as 'longue', where the 'g', if not removed in
            # preprocessing, is indeed clearly audible
            if word.endswith('ng'):
                if self.lang == 'fr':
                    word = word + 'ə'
                else:
                    word = word[:-1]

            # Final 'iy' is simplified to 'i' (without penalty)
            if word.endswith('iy'):
                word = word[:-1]

            # Final 'ay/oy' are changed to 'aI/oI' (without penalty, since the sound is very
            # similar)
            if word.endswith('ay') or word.endswith('oy'):
                word = word[:-1] + 'I'

            # Final 'h' is dropped (with penalty)
            if word.endswith('h'):
                word = word[:-1]
                self.penalty += 1

            # As is 'h' before another consonant (excluding semivowels)
            word, count = H_BEFORE_CONSONANT.subn(r'\1', word)
            self.penalty += count

            # Add a final vowel if the word contains consonants, but no vowels (penalties for
            # added vowels are calculated below)
            if ALL_CONSONANTS_RE.search(word) and not SIMPLE_VOWEL_RE.search(word):
                word = word + 'ə'

            # Insert filler vowels between illegal pairs and triples of consonants
            word = X_BETWEEN_CONSONANT.sub(r'\1əxə\2', word)
            word = word.replace('rld', 'rəld')
            word = ILLEGAL_SKPT_TRIPLE_RE.sub(r'\1ə\2', word)
            word = ILLEGAL_CONS_PAIR_RE.sub(r'\1ə\2', word)
            word = ILLEGAL_RN_TRIPLE_RE.sub(r'\1ə\2', word)
            word = word.replace('stl', 'stəl')
            word = word.replace('stv', 'stəv')
            word = word.replace('smr', 'səmr')
            word = INITIAL_CONS_TRIPLE_RE.sub(r'\1ə\2', word)
            word = ILLEGAL_CONS_TRIPLE_RE.sub(r'\1ə\2', word)
            word = ILLEGAL_CONS_TRIPLE2_RE.sub(r'\1ə\2', word)

            # Most consonants are not allowed at the end of words, nor are pairs of consonants
            if (word and word[-1] in NON_FINAL_CONSONANTS) or (
                    len(word) > 2 and word[-1] in ALL_CONSONANTS and word[-2] in ALL_CONSONANTS):
                word += 'ə'

            # Prepend a vowel if that leads to a valid sequence in a word starting with two
            # consonants
            if len(word) > 2 and word[0] in FINAL_CONSONANTS and word[1] in NOT_SECOND_CONSONANTS:
                word = 'ə' + word

            # The semivowel 'y'  followed or preceded by a filler vowel is simplified to the vowel
            # equivalent instead -- in some cases it may become part of a diphthong
            word = word.replace('ayə', 'aI')
            word = word.replace('oyə', 'oI')
            # DON'T do this in case of the sequence 'ny' which is quite characteristic
            # (and may be pronounced as a single sound)
            word = word.replace('ny', 'NY')
            word = word.replace('yə', 'i')
            word = word.replace('NY', 'ny')
            word = word.replace('əy', 'i')
            # Note: We don't to this for /w/, since it often comes from /v/, which would be
            # too distorted by changing it a vowel

            # 'h' before a filler vowel is dropped (with penalty)
            self.penalty += word.count('hə')
            word = word.replace('hə', '')

            # This may have resulted in spurious filler vowels (before other vowels),
            # which we delete
            word = word.replace('əu', 'u')
            word = word.replace('əi', 'i')

            # Eliminate double consonants again (since new ones might have been added by the
            # previous rules)
            word = DOUBLE_CONS_RE.sub(r'\1', word)

            # Resolve issues with our tightened rules for syllable-initial consonant pairs
            word = ILLEGAL_SECOND_CONS_PAIRS.sub(self.resolve_illegal_pairs, word)
            word = ILLEGAL_SECOND_CONS_PAIRS2.sub(self.resolve_illegal_pairs, word)

            # Eliminate the double 'ii' that occurs in some Russian words
            if self.lang == 'ru':
                word = word.replace('ii', 'i')

            ### Vowel combinations that look like diphthongs are resolved by inserting a semivowel
            ### between them ('y' before 'i' and 'v' before 'u) – this incurs a penalty
            ##for pair, repl in VOWEL_PAIR_DISAMBIGUATION.items():
            ##    self.penalty += word.count(pair)
            ##    word = word.replace(pair, repl)

            self.penalty += word.count('ə')
            new_words.append(word)
        self.word = ' '.join(new_words)
        self.filled = True

    def validate(self) -> Optional[str]:
        """Check that this candidate is valid.

        Returns an error message if this is not the case.
        """
        words = self.word.split()

        for word in words:
            # Split at vowels and examine the non-vowel parts (which may be empty)
            parts = CAND_VOWEL_RE_NONCAPT.split(word)
            last_idx = len(parts) - 1
            for idx, part in enumerate(parts):
                if not part:
                    continue
                if idx > 0 and part[0] in FINAL_CONSONANTS:
                    part = part[1:]  # discard consonant ending the preceding syllable
                if not part:
                    continue
                if idx == last_idx:
                    return f'"{part}" is not allowed at the end of words ({word})'
                if not ((len(part) == 1 and part in INITIAL_CONSONANTS) or part in INITIAL_PAIRS):
                    return f'"{part}" is not allowed at the start of syllables ({word})'

            # Check for clusters that should be avoided
            for cluster, repl in AVOIDED_CLUSTERS.items():
                if cluster in word:
                    return f'"{cluster}" should be "{repl}" instead ({word})'
        return None

    def __str__(self) -> str:
        """Return a compact string representation."""
        return f'{self.lang}:{self.export_word()}'

    def show_info(self) -> str:
        """Return a detailed string representation."""
        result = (f'{self.lang}:{self.export_word()} (P:{self.total_penalty:.3f} – '
                  f'C:{self.pcon:.3f}+S:{self.psim:.3f}+I:{self.pinf:.3f}, ')

        identical_cand_count = len(self.find_langs_with_identical_candidate())
        if identical_cand_count:
            if identical_cand_count == 1:
                result += '1 identical candidate, '
            else:
                result += f'{identical_cand_count} identical candidates, '

        if self.related_cands:
            language_str = 'language' if len(self.related_cands) == 1 else 'languages'
            result += f'related candidates in {len(self.related_cands)} {language_str}: '
            rel_cands: List[Candidate] = []
            for rel_lang in sorted(self.related_cands):
                rel_cands += self.related_cands[rel_lang]
            result += ', '.join(str(cand) for cand in rel_cands) + ')'
        else:
            result += 'no related candidates)'

        return result


##### Helper functions #####

@lru_cache(maxsize=None)
def export_word(word: str) -> str:
    """Convert a word from the internal form to the external form used in the dictionary."""
    for key, value in EXPORT_REPL.items():
        word = word.replace(key, value)
    return word


@lru_cache(maxsize=None)
def normalize_word(word: str) -> str:
    """Normalize a word to avoid adding minimal pairs in the dictionary.

    * Case is converted to lower-case.
    * 'j' is converted to 'c'
    * 'v' and 'y' are converted to their vowel equivalents.
    * Apostrophes are deleted.
    * Leading or trailing hyphens (marking affixes) are deleted
    """
    word = word.lower().strip('-')
    for key, value in NORM_REPL.items():
        word = word.replace(key, value)
    return word


def extract_phonetic_conversion_rule(row: Sequence[str], filename: str) -> Tuple[str, Conversion]:
    """Extract a phonetic conversion rule from a line in a CSV file.

    The line should have 3 fields:

    1. The input characters (IPA)
    2. The output characters (our phonology)
    3. Whether the conversion should incur a penalty ('1') or not ('0')
    """
    if len(row) != 3:
        LOG.warn(f'Error parsing {filename}: Row "{",".join(row)}" has {len(row)} fields '
                 'instead of 3.')
    inchar = util.get_elem(row, 0)
    outchar = util.get_elem(row, 1)
    rawpenalty = util.get_elem(row, 2, '1')
    if rawpenalty == '0':
        penalty = False
    else:
        penalty = True
        if rawpenalty != '1':
            LOG.warn(f'Unexpected penalty in {filename}: "{rawpenalty}" instead of "0" or "1".')
    return inchar, Conversion(outchar, penalty)


##### Main class #####

class VocBuilder:
    """Build our vocabulary, generating and selecting suitable candidate words."""
    # pylint: disable=too-many-instance-attributes, too-many-public-methods

    def __init__(self, args: argparse.Namespace) -> None:
        """Create a new instance."""
        self.args = args

        # Read existing entries, if any
        self.existing_entries: Sequence[LineDict] = []
        if path.exists(DICT_FILE):
            self.existing_entries = util.read_dicts_from_file(DICT_FILE)

        if not (self.args.add
                or self.args.addenglish
                or self.args.addlugamun
                or self.args.delete
                or self.args.delete_only
                or self.args.polycheck):
            # Which kind(s) of word should be added next
            self.kinds_to_add: FrozenSet[Kind] = self.determine_kinds_to_add(self.existing_entries)

        # Sets used for the polysemy check
        self.polyseme_dict: Dict[str, Set[str]] = self.fill_polyseme_dict(self.existing_entries)
        self.langcode_sets: Dict[str, Set[str]] = self.fill_langcode_sets(self.existing_entries)

        # Mapping from existing words (converted to lower-case) to their classes
        existing_words_dict: Dict[str, Set[str]] = defaultdict(set)
        # Just the words, normalized to avoid minimal pairs and with affix hyphens removed
        existing_norm_words: Set[str] = set()

        for entry in self.existing_entries:
            wordlist = entry.get('word', '')
            classes = set(util.split_on_commas(entry.get('class', '')))
            for word in util.split_on_commas(wordlist):
                existing_words_dict[word.lower()] |= classes
                existing_norm_words.add(normalize_word(word))

        self.existing_words_dict = existing_words_dict
        self.existing_norm_words = existing_norm_words

        # A cache of candidates to avoid having to build them twice. Keys have the form
        # "langcode:word". Note that the class is NOT part of the key. Usually this works best
        # when merging words of different classes, where the class might change from e.g.
        # "verb" to "verb, noun".
        self.candi_cache: Dict[str, Optional[Candidate]] = {}

        # Input/output mappings to convert IPA, Pinyin etc. into our phonology
        convdicts = {}
        for conv_dict in CONV_DICTS:
            convdicts[conv_dict] = self.read_conversion_dict(f'phon-{conv_dict}.csv')

        # What's the length of the longest each in each of these dictionaries?
        maxkeylengths = {}
        for name, dct in convdicts.items():
            maxkeylengths[name] = len(max(dct.keys(), key=len))

        self.convdicts = convdicts
        self.maxkeylengths = maxkeylengths

        # Used by the --addlugamun option
        self.old_word = ''

        # For Pinyin, also used for French, Japanese, Portuguese and Thai
        self.tone_trans_table = str.maketrans('āēīōūǖáéíóúǘǎěǐǒǔǚàèìòùǜăĕĭŏŭâêîôû',
                                              'aeiouüaeiouüaeiouüaeiouüaeiouaeiou')
        # For Spanish
        self.acute_accents_trans_table = str.maketrans(ACUTE_ACCENTS, VOWELS_WO_ACCENT)

    @staticmethod
    def get_kind(entry: LineDict) -> Kind:
        """Read the class of an entry and convert it into one of three kinds.

        * Nouns and names (proper nouns) are treated as Kind.NOUN
        * Adjectives and adverbs (quantifiers and selectors are also included here) are
          treated as Kind.ADJ
        * Verbs and words of all other classes are treated as Kind.VERB

        If a word belongs to several classes, only its first kind will be returned. So,
        "verb, noun" will be treated as Kind.VERB.
        """
        classes = util.split_on_commas(entry.get('class', ''))
        if not classes:
            word = entry.get('word', '')
            LOG.warn(f'Entry {word} lacks a class (treating as noun)')
            return Kind.NOUN

        first_class = classes[0]
        if first_class in ('noun', 'name'):
            return Kind.NOUN
        if first_class in ('adj', 'adv', 'quant', 'sel'):
            return Kind.ADJ
        return Kind.VERB

    def determine_kinds_to_add(self, entries: Sequence[LineDict]) -> FrozenSet[Kind]:
        """Determine the kinds of word to be added next.

        Words are grouped into three kinds as per the 'get_kind' method (nouns, adjectives,
        verbs). We try to ensure that all kinds are equally common in the dictionary, so this
        will return the rarest kind. If two kinds are equally rare, both will be returned.
        If all kinds are equally common, all three will be returned.
        """
        # We cannot use Python's counter class here, since 0 counts matter as well
        counter: Dict[Kind, float] = {}
        for kind in KIND_DESC:
            counter[kind] = 0
        for entry in entries:
            counter[self.get_kind(entry)] += 1

        # Nouns only count 57% and verbs 85% to give them a somewhat higher priority
        counter[Kind.NOUN] *= 0.57
        counter[Kind.VERB] *= 0.85

        result = set()
        rarest_count = -1.0

        for kind, count in sorted(counter.items(), key=lambda pair: pair[1]):
            if rarest_count < 0:
                result.add(kind)
                rarest_count = count
            else:
                if count == rarest_count:
                    result.add(kind)
                else:
                    break

        # Log choice, unless an option is specified that makes this unnecessary
        if len(result) == 3:
            msg = 'All kinds of words are equally common.'
        else:
            kinds = ''
            for kind in result:
                if kinds:
                    kinds += ' or '
                kinds += KIND_DESC[kind]
            # Add compact statistical info
            stats = '/'.join(f'{kind.name[0]}:{round(count, 1)}' for kind, count in sorted(
                counter.items(), key=lambda pair: pair[1]))
            msg = f'Should add {kinds} ({stats}).'
        LOG.info(msg)

        return frozenset(result)

    @staticmethod
    def fill_polyseme_dict(entries: Sequence[LineDict]) -> Dict[str, Set[str]]:
        """Initialize and fill the dictionary used in order to check for possible polysemes.

        Polysemes are words that have several related meanings.

        Keys have the form "langcode:word" (e.g. "en:water"), values are sets of words in our
        language (e.g. "pani"). All keys are converted to lower-case (values are left as is).
        """
        result: Dict[str, Set[str]] = defaultdict(set)
        for entry in entries:
            for key, translations in entry.items():
                word = entry.get('word', '')
                if len(key) > 3 or key in util.COMMON_AUXLANGS:
                    # We're only interested in field names that have at most 3 letters
                    # (language codes); auxlangs are ignored
                    continue
                for trans in util.split_on_commas(translations):
                    if key == 'en':
                        # English words may be followed or preceded by an explanation in
                        # parentheses which we discard
                        trans = util.eliminate_parens(trans)
                    full_trans = f'{key}:{trans}'
                    result[full_trans.lower()].add(word)
        return result

    @staticmethod
    def fill_langcode_sets(entries: Sequence[LineDict]) -> Dict[str, Set[str]]:
        """Initialize a set of language codes giving the translations of each word.

        Maps each Lugamun word to a set of language code into which the word has been translated
        (e.g. ar, cmn, en, fr, es etc.)
        """
        result: Dict[str, Set[str]] = defaultdict(set)
        for entry in entries:
            for key in entry.keys():
                word = entry.get('word', '')
                if not (len(key) > 3 or key in util.COMMON_AUXLANGS):
                    result[word].add(key)
        return result

    @staticmethod
    def read_conversion_dict(filename: str) -> Dict[str, Conversion]:
        """Read a dictionary of phonetic rules from a CSV file.

        See 'extract_phonetic_conversion_rule' for a description of the expected format.
        """
        return util.read_dict_from_csv_file(filename, converter=extract_phonetic_conversion_rule)

    @staticmethod
    def format_entry_key(engl: str, sense: str) -> str:
        """Construct a formatted key string from the English and sense fields of an entry."""
        return f'{engl} ({sense})'

    def mk_entry_key(self, entry: LineDict) -> str:
        """Construct a key string from an entry.

        The returned string has the form "ENGLISH (SENSE)", where "ENGLISH" is the English word
        ("en" field, without IPA information) and "SENSE" is the "sense" field.
        """
        engl = util.discard_text_in_brackets(entry.get('en', ''))
        sense = entry.get('sense', '')
        return self.format_entry_key(engl, sense)

    @staticmethod
    def combine_entries(entry_1: LineDict, entry_2: LineDict) -> None:
        """Combine 'entry_2' into 'entry_1'.

        The "en" and "sense" fields are skipped, all other fields from 'entry_2' are
        added to 'entry_1'. If the latter already had such a field, it is overwritten.
        """
        for key, value in entry_2.items():
            if key in ('en', 'sense'):
                continue
            entry_1.add(key, value, -1, True)

    def build_existing_entry_dict(self) -> Dict[str, LineDict]:
        """Parse the existing entries into a dictionary mapping from keys to entries.

        Keys are formed as per the 'mk_entry_key' method.

        If an entry has been merged from several separate original entries (e.g. `sense:
        personal pronoun | direct object of a verb`; `en: I, me`), two separate dictionary
        entries will be created, one for each original entry. Both will point to the same
        merged entry.
        """
        result = {}

        for entry in self.existing_entries:
            engl = util.discard_text_in_brackets(entry.get('en', ''))
            sense = entry.get('sense', '')
            parse_word_from_sense = False

            if ' | ' in sense:
                sense_list = sense.split(' | ')
                engl_list = util.split_on_commas(engl)
                engl_word = None

                # When there is just one English word, it's used for all senses; otherwise
                # there should be one word per sense – if that's not the case, the intended
                # word must be added in parentheses at the end of each sense, e.g.
                # "to make a declaration (declare) | act or process of declaring (declaration)
                # | written or oral indication of a fact, opinion, or belief (declaration)"
                if len(engl_list) == 1:
                    engl_word = engl_list[0]
                elif len(sense_list) != len(engl_list):
                    parse_word_from_sense = True

                for idx, subsense in enumerate(sense_list):
                    if parse_word_from_sense:
                        # Intended word must be given at the end of each sense
                        orig_subsense = subsense
                        subsense, engl = util.split_text_and_explanation(subsense)  # type: ignore
                        if engl is None:
                            LOG.warn(f'Problem splitting merged entry: sense "{orig_subsense}" '
                                     "doesn't give the intended word in parentheses, and the "
                                     'numbers of senses and of English words differ')
                    else:
                        engl = engl_word if engl_word else engl_list[idx]
                    if subsense is not None:
                        # English word may end in an explanation in parentheses which we discard
                        engl = util.split_text_and_explanation(engl)[0]
                        result[self.format_entry_key(engl, subsense)] = entry
            else:
                # English word may end in an explanation in parentheses which we discard
                engl = util.split_text_and_explanation(engl)[0]
                result[self.format_entry_key(engl, sense)] = entry
        return result

    @staticmethod
    def skip_entry(entry: LineDict) -> bool:
        """
        Return True iff this entry should be skipped.

        This is the case for all entries whose class is "article", since Lugamun doesn't have
        any articles.
        """
        return entry.get('class') == 'article'

    def sort_entries_by_transcount(self) -> Dict[int, List[LineDict]]:
        """Sort entries in termdict by number of translations.

        All entries that already exist in our own dictionary are skipped. Those flagged by the
        'skip_entry' method are skipped as well. No skipping is performed, however, if the
        --copy or --polycheck option has been specified.

        Also reads extradict.txt and uses this to complete the entries before their
        translations are counted. If an extradict entry is not used, a warning is printed.
        """
        count_map: Dict[int, List[LineDict]] = defaultdict(list)
        entries = util.read_dicts_from_file(util.TERM_DICT)
        existing_entry_dict = self.build_existing_entry_dict()
        extra_entries = util.read_dicts_from_file(util.EXTRA_DICT)
        extra_entry_dict = {}

        for extra_entry in extra_entries:
            extra_entry_dict[self.mk_entry_key(extra_entry)] = extra_entry

        extra_keys_seen = set()

        for entry in entries:
            # Remove ' — see also' from senses, if present
            sense = entry.get('sense', '')
            see_also_pos = sense.find('— see also')

            if see_also_pos != -1:
                sense = sense[:see_also_pos].strip()
                entry.add('sense', sense, allow_replace=True)

            entry_key = self.mk_entry_key(entry)
            filter_out_existing = not (self.args.polycheck or self.args.copy)

            # Skip if the entry already exists in our dictionary or if it should be skipped
            # (but remember the key to avoid spurious warnings about unused extradict entries)
            if filter_out_existing and (entry_key in existing_entry_dict or self.skip_entry(entry)):
                extra_keys_seen.add(entry_key)
                continue

            # Merge with extra entry, if any
            extra_entry = extra_entry_dict.get(entry_key, None)  # type: ignore
            if extra_entry:
                extra_keys_seen.add(entry_key)
                self.combine_entries(entry, extra_entry)
                # If the extra entry is tagged as 'skip', the whole entry will be skipped
                if entry.get('tags', '') == 'skip':
                    continue

            transcount = int(entry['transcount'])
            count_map[transcount].append(entry)

        # Add entries with "tags: add" and warn if any unused extra entries remain
        for entry in extra_entry_dict.values():
            entry_key = self.mk_entry_key(entry)
            if entry_key not in extra_keys_seen:
                tags = entry.get('tags', '')
                if tags == 'add':
                    del entry['tags']
                    transcount = int(entry.get('transcount', '1'))
                    count_map[transcount].append(entry)
                else:
                    LOG.warn(f'Unused entry "{entry_key}" in {util.EXTRA_DICT}, line '
                             f'{entry.first_lineno()}.')

        return count_map

    def preprocess_candidate_word(self, word: str, conv_dict_name: str, cls: str) -> str:
        """Do some language-specific preprocessing on a candidate word."""
        # pylint: disable=too-many-branches, too-many-statements
        original = word

        if conv_dict_name in ('es', 'fr', 'id', 'ja', 'ru', 'sw'):
            # Convert to lower case
            word = word.lower()

            if conv_dict_name in ('fr', 'ja'):
                if conv_dict_name == 'fr':
                    # We need to preserve the diacritic in these two cases where combinations
                    # are typically pronounced differently, hence we upper-case them
                    # (the conversion dictionary will handle the rest)
                    word = word.replace('éa', 'ÉA')
                    word = word.replace('ée', 'ÉE')
                    word = word.replace('oê', 'OÊ')

                word = word.translate(self.tone_trans_table)

                if conv_dict_name == 'fr':
                    # Handle soft c and soft g and some related cases
                    word = re.sub(r's?c(?=[eÉiy])', 's', word)
                    word = re.sub(r'g(?=[eÉiy])', 'j', word)
                    word = re.sub(r'gu(?=[eÉiy])', 'g', word)
                    word = re.sub(r'[cx]c(?=[eÉiy])', 'x', word)
                    word = re.sub(r'ge(?=[aoOu])', 'j', word)

                    # 'ouill' becomes ultimately 'uy' (we prepare this here)
                    word = word.replace('ouill', 'ouY')

                    # Pronunciation of i/y and ou before vowels (except after another vowel)
                    word = re.sub(rf'(?<![{SIMPLE_VOWELS}AEÊ])[iy](?=[{SIMPLE_VOWELS}ÉO])', 'Y',
                                  word)
                    word = re.sub(rf'(?<![{SIMPLE_VOWELS}AEÊ])ou(?=[{SIMPLE_VOWELS}ÉO])', 'v',
                                  word)
                    # But also after 'qu'
                    word = re.sub(rf'(?<=qu)i(?=[{SIMPLE_VOWELS}ÉO])', 'Y', word)

                    # Delete final letters that are usually silent
                    if (len(word) > 3
                            and (word.endswith('er') or word.endswith('il'))
                            and re.search(rf'[{SIMPLE_VOWELS}AEOÉÊYw]', word[:-2])):
                        word = word[:-1]
                    elif word.endswith('ng'):
                        word = word[:-1]
                    elif word.endswith('ngt'):
                        word = word[:-2]
                    elif (len(word) > 2
                            and word.endswith('s')
                            and word[-2] in 'bdefgpt'
                            and not original.endswith('ès')):
                        word = word[:-2]
                    elif len(word) > 1 and word[-1] in 'bdegpstxz' and not (
                            re.search('(ng|[ps]t|^.ix)$', word) or original.endswith('é')):
                        word = word[:-1]

            elif conv_dict_name == 'id' and cls == 'verb':
                # Strip meng- prefix (and its variants) from Indonesian verbs, see
                # https://en.wiktionary.org/wiki/meng-#Indonesian
                orig_verb = word
                if word.startswith('menge'):
                    # menge -> ke, e.g. mengepung -> kepung
                    word = 'ke' + word[5:]
                else:
                    word = re.sub(r'^meng(?=[ghkaeiou])', '', word)
                if orig_verb == word:
                    word = re.sub(r'^mem(?=[bfp])', '', word)
                if orig_verb == word:
                    word = re.sub(r'^men(?=[cdjstz])', '', word)
                if orig_verb == word:
                    word = re.sub(r'^me(?=[lmnrwy])', '', word)
                if orig_verb == word:
                    word = re.sub(r'^meny', 's', word)
                ##if orig_verb != word:
                ##    print(f 'Note: Indonesian verb "{orig_verb}" changed to "{word}".')

            elif conv_dict_name == 'ru':
                # 'lj' is simplified to 'l', since the /j/ is very reduced
                word = word.replace('lj', 'l')
                # Russian word-final 'd' is always pronounced /t/ – we convert it accordingly,
                # since that sound is allowed to end a syllable in our phonology
                if word.endswith('d'):
                    word = word[:-1] + 't'
                # Stress first vowel, if none is stressed (stressed vowels are often
                # pronounced differently, hence this matters)
                if not ACUTE_ACCENT_RE.search(word):
                    word = VOWEL_WO_ACCENT_RE.sub(
                        lambda matchobj: ACUTE_ACCENTS[VOWELS_WO_ACCENT.find(matchobj.group(0))],
                        word, count=1)

        elif conv_dict_name == 'ar':
            # Strip initial 'al-' from Arabic words
            if word.startswith('al-'):
                word = word[3:]

        elif conv_dict_name == 'pinyin':
            # Convert to lower case and strip accents marking tones
            word = word.lower()
            word = word.translate(self.tone_trans_table)
            # Insert a '+' sign (will be deleted during conversion) between vowels and 'n' or 'r',
            # to distinguish initials such as 'fùnǚ' (fù+nǚ) from finals such as 'ānchún'
            word = NR_BETWEEN_VOWELS.sub(r'\1+\2', word)

        # Remove any zero-width non-joiners
        word = word.replace('\u200C', '')
        return word

    @staticmethod
    def shorten(word, syllables: int):
        """
        Shorted a word to the first N syllables.

        The word must use the vowel spellings used in candidates ('aI' etc. for diphthongs).

        Syllable boundaries might not strictly correspond to those used for word division –
        instead, the first consonant after the last included vowel will always be included as
        well if it's allowed to end a word, e.g. shorten('trogloditide', 3) will return
        'troglodit'.
        """
        parts_to_keep = []
        vowels_seen = 0
        parts = CAND_VOWEL_RE_CAPT.split(word)

        for part in parts:
            if not part:
                continue
            if part[0] in ALL_CONSONANTS:
                # It's a consonant (cluster)
                if vowels_seen == syllables:
                    # Just keep one final consonant, if possible; otherwise we're done
                    if part[0] in FINAL_CONSONANTS:
                        parts_to_keep.append(part[0])
                        break
                else:
                    parts_to_keep.append(part)
            else:
                # It's a vowel
                vowels_seen += 1
                if vowels_seen > syllables:
                    break
                parts_to_keep.append(part)
        return ''.join(parts_to_keep)

    def postprocess_candidate(self, candidate: str, original: str, langcode: str, cls: str) -> str:
        """Perform language-specific postprocessing on a candidate entry, where needed.

        Returns the candidate, either as is or with the necessary changes performed.
        """
        # pylint: disable=too-many-branches, too-many-statements
        words = candidate.split()
        new_words = []

        for word in words:
            if langcode == 'en':
                # Word-final R-colored schwa as in 'water' or 'user' becomes 'a' rather than 'er',
                # thus better reflecting that its unstressed (and following the example of
                # Swahili 'picha' from 'picture'
                original = original.strip('/')
                if word.endswith('er') and (FINAL_R_COLORED_SCHWA.search(original)
                                            and not original.endswith('ɛəɹ')):
                    word = word[:-2] + 'a'
                elif word.endswith('e') and original.endswith('ə'):
                    # We also convert a final schwa to 'a' if it's written like that in the
                    # original word, as it nearly always is (e.g. 'comma')
                    word = word[:-1] + 'a'

            elif langcode == 'es':
                # Word-initial 'x' is 's' instead of 'ks'
                if word.startswith('ks'):
                    word = word[1:]

                # 'i' before a vowel becomes 'y', 'u' becomes 'v'
                word = IU_BEFORE_VOWEL.sub(
                    lambda matchobj: ('y' if matchobj.group(1) == 'i' else 'v') + matchobj.group(2),
                    word)

                # 'S' was used as a marker of silent letters (h) -- now, after semivowels have
                # been converted, we can delete it
                word = word.replace('S', '')
                # Strip acute accents -- we do this only now since accented vowels never become
                # semivowels
                word = word.translate(self.acute_accents_trans_table)

            elif langcode == 'fr':
                if word.endswith('tyon') and original.endswith('tYon'):
                    # Final -tion is pronounced more like -syon rather than -tyon
                    word = word[:-4] + 'syon'

            elif langcode == 'id':
                # Final 'k' usually represents the glottal stop, so we delete it
                if word.endswith('k'):
                    word = word[:-1]

            elif langcode == 'ru':
                if word.endswith('i'):
                    # Final 'e' is pronounced 'e'
                    if original.endswith('e'):
                        word = word[:-1] + 'e'
                    # Final 'ja' is /jə/ – here we convert the final vowel to 'a' since
                    # that seems to fit better (cf. the spelling of the romanization)
                    elif original.endswith('ja') and word.endswith('i'):
                        word = word[:-1] + 'a'

            elif langcode == 'taxo':
                # 'c' becomes 's' before front vowels, otherwise 'k'
                if word.endswith('c'):
                    word = word[:-1] + 'k'
                word = C_BEFORE_NON_EI.sub('k', word)
                word = word.replace('c', 's')
                # 'ng' becomes 'n' in positions where /g/ is not allowed
                word = ILLEGAL_NG.sub('n', word)
                # Final -s is deleted after 'u' or a consonant, provided the word has at least
                # two syllables
                if (word.endswith('s') and util.count_vowels(word) > 1
                        and word[-2].lower() not in 'aeio'):
                    word = word[:-1]
                # Shorten long words to three syllables
                word = self.shorten(word, 3)

            if langcode in ('es', 'fr'):
                # Double 'oo' is reduced to one, e.g. in 'alcohol/alcool'
                word = word.replace('oo', 'o')

            if cls == 'verb':
                # Strip typical infinitive markers from verbs
                if langcode in ('es', 'fr'):
                    if langcode == 'fr' and word.endswith('var') and (
                            original.endswith('avoir')
                            or original.endswith('ouvoir')
                            or original.endswith('loir')):
                        # In a handful of French verbs (avoir, mouvoir, pouvoir; falloir,
                        # prévaloir, vouloir), replace the final -oir with -e, since this
                        # sequence doesn't appear in the conjugated forms, while -e appears
                        # in the related Spanish/Portuguese verbs
                        word = word[:-3] + 'e'
                    elif langcode == 'fr' and E_AFTER_FINAL_CONSONANT.search(word):
                        # The final 'er' is stripped from French verbs if it's preceded by
                        # a single consonant allowed to end a syllable, since the 'e' is
                        # silent in most conjugated forms – e.g. 'arrêter' becomes 'aret'.
                        # Actually the -r was already stripped in preprocessing, so we just
                        # need to strip one letter.
                        word = word[:-1]
                    elif langcode == 'fr' and word.endswith('tr'):
                        # In French verbs ending in 'tr(e)' (e.g. 'croître'), both these final
                        # consonants are silent in the typical present-tense forms
                        word = word[:-2]
                    elif word.endswith('r'):
                        word = word[:-1]
                    elif langcode == 'es' and word.endswith('rse'):
                        word = word[:-3] + 'se'
                elif langcode == 'hi':
                    if word.endswith('na'):
                        word = word[:-2]
                elif langcode == 'ru':
                    if word.endswith('t'):
                        word = word[:-1]
                elif langcode == 'ar':
                    # Strip the final -a from the dictionary form of Arabic verbs with three
                    # (or more) vowels if the result is allowed by Lugamun's phonology (i.e.
                    # the -a is kept after a consonant that's not allowed to end a syllable)
                    if (util.count_vowels(word) >= 3
                            and word[-1] == 'a'
                            and (word[-2] in FINAL_CONSONANTS
                                 or word[-2].lower() in SIMPLE_VOWELS)
                            # Simplification of 'iy' and 'uv' happens only later
                            or word[-3:].lower() in ('iya', 'uva')):
                        word = word[:-1]
                        ##print(f 'Arabic verb changed from "{word}a" to "{word}".')
                elif langcode == 'ja':
                    # Likewise strip the final -u from the dictionary form of Japanese verbs with
                    # three (or more) vowels if the result is allowed by Lugamun's phonology
                    if (util.count_vowels(word) >= 3
                            and word[-1] == 'u'
                            and (word[-2] in FINAL_CONSONANTS
                                 or word[-2].lower() in SIMPLE_VOWELS)
                            # Simplification of 'iy' and 'uv' happens only later
                            or word[-3:].lower() in ('iyu', 'uvu')):
                        word = word[:-1]
                        ##print(f 'Japanese verb changed from "{word}u" to "{word}".')
                elif langcode == 'sw':
                    # Swahili: strip initial ku- unless it's a short verb with just two syllables,
                    # since these tend to preserve the ku- in many cases
                    if word.startswith('ku') and util.count_vowels(word) >= 3:
                        word = word[2:]

            new_words.append(word)

        result = ' '.join(new_words)
        return result

    def mk_candidate(self, word: str, langcode: str, conv_dict_name: str,
                     cls: str) -> Optional[Candidate]:
        """Convert a word or its phonetic representation into a candidate word.

        'conv_dict_name' is the name of the conversion dictionary to use, e.g. 'ipa' or 'pinyin'.

        A word may be followed by an explanation or comment enclosed in parentheses, which
        will be stripped.

        If the word is empty or completely enclosed in parentheses, None will be returned.
        """
        # pylint: disable=too-many-locals
        cache_key = f'{langcode}:{word}'
        if cache_key in self.candi_cache:
            return self.candi_cache[cache_key]

        if word.endswith(')'):
            start_idx = word.rfind('(')
            if start_idx >= 0:
                # Strip comment in parentheses
                word = word[:start_idx]
                word = word.strip()
        if not word:
            result = None
            self.candi_cache[cache_key] = result
            return result

        original = word

        if langcode not in SOURCE_LANG_CODES and conv_dict_name not in self.convdicts:
            # For languages requested via --consider that don't have a conversion dictionary,
            # we just return the word converted to lower-case
            result = Candidate(original.lower(), 0, langcode, original)
            self.candi_cache[cache_key] = result
            return result

        word = self.preprocess_candidate_word(word, conv_dict_name, cls)
        convdict = self.convdicts[conv_dict_name]
        maxkeylen = self.maxkeylengths[conv_dict_name]
        out_word = ''
        penalty = 0
        rest_word = word

        while rest_word:
            found_match = False
            # We always try to find the longest match in the convdict
            for idx in range(min(maxkeylen, len(rest_word)), 0, -1):
                conv = convdict.get(rest_word[0:idx])

                if conv is not None:
                    out_word += conv.output
                    penalty += conv.penalty
                    rest_word = rest_word[idx:]
                    found_match = True
                    break

            if not found_match:
                # Just copy the first character and warn
                out_word += rest_word[0]
                LOG.warn(f'Unexpected character "{rest_word[0]}" encountered while converting '
                         f'{langcode} candidate "{word}".')
                rest_word = rest_word[1:]

        out_word = self.postprocess_candidate(out_word, word, langcode, cls)

        #if langcode == 'fr':
        #    LOG.info(f 'Input: {word}, output: {out_word}')
        result = Candidate(out_word, penalty, langcode, original)
        self.candi_cache[cache_key] = result
        return result

    def ignore_missing(self) -> bool:
        """Whether warnings about missing candidate words should be ignored.

        This is the case of one of the options --ignoremissing, --addmeaning, or --compound has
        been specified (in the case of the latter two, no warning is needed since the
        candidates won't be used anyway).
        """
        return self.args.addmeaning or self.args.compound or self.args.ignoremissing

    def build_candidates_for_lang(self, langcode: str, entry: LineDict) -> Sequence[Candidate]:
        """Build and candidate words for one specific language."""
        # pylint: disable=too-many-branches, too-many-locals
        word = entry.get(langcode, '')
        cands = []
        conv_dict_name = langcode
        fieldname = langcode

        # Custom overrides in a few cases
        if langcode == 'cmn':
            conv_dict_name = 'pinyin'
        elif langcode == 'en':
            conv_dict_name = 'ipa'

        raw_cand = entry.get(fieldname, '')
        cls = entry.get('class', '')

        if langcode == 'id' and not word:
            # Try using Malay as fallback
            word = entry.get('ms', '')
            raw_cand = word

        if raw_cand:
            raw_cand = raw_cand.lower()
            raw_cand_words = util.split_on_semicolons(raw_cand)
            # We keep a set of words to avoid adding duplicates
            cand_word_set: Set[str] = set()

            for raw_cand_word in raw_cand_words:
                # If there's text in square brackets in the candidate (e.g. "water [ˈwɔtəɹ]"),
                # we use just that part -- for English, we skip any candidates that don't have
                # such text (usually that's multi-word expressions which lack IPA information)
                actual_cand_words = util.extract_text_in_brackets(raw_cand_word, langcode != 'en')

                # Sometimes the text in brackets contains comma-separated alternatives (e.g.
                # "骨頭 /骨头 [gǔtou, gútou]", so we have to split again)
                for actual_cand_word in util.split_on_commas(actual_cand_words):
                    candidate = self.mk_candidate(actual_cand_word, langcode, conv_dict_name, cls)
                    if not candidate:
                        continue

                    candidate.insert_filler_vowels()
                    val_err = candidate.validate()

                    if val_err:
                        LOG.warn(f'Invalid candidate for {langcode}:"{word}": {val_err}.')

                    if self.args.schwastrip:
                        self._schwastrip(candidate)

                    if candidate.word not in cand_word_set:
                        cand_word_set.add(candidate.word)
                        cands.append(candidate)

            ##LOG.info(f'{langcode}:{word} ({", ".join(map(lambda x: x.original, cands))}) -> '
            ##         f'{", ".join(map(str, cands))}')
        elif word:
            if not (self.ignore_missing() or (word.startswith('(') and word.endswith(')'))):
                # If the whole original word is wrapped in parentheses, it's an explanation such as
                # "(not used in Mandarin)", hence the absence of a candidate is not a problem
                LOG.warn(f'No raw candidate found for {langcode} word "{word}".')
        else:
            if not self.ignore_missing():
                LOG.warn(f'No {langcode} word found for "{entry.get("en")}" '
                         f'({entry.get("sense")}).')

        return cands

    @staticmethod
    def _schwastrip(candidate: Candidate) -> None:
        """Strip a schwa (filler vowel) from the start and/or end of this candidate."""
        if candidate.word.startswith('ə'):
            candidate.word = candidate.word[1:]
        if candidate.word.endswith('ə'):
            candidate.word = candidate.word[:-1]

    @staticmethod
    def print_frequency_distribution(intro_msg: str, counter: Counter[str]) -> None:
        """Print 'intro_msg' followed by frequency statistics from a counter."""
        LOG.info(intro_msg)
        total = sum(counter.values())
        formatted_entries = []

        for item, count in sorted(counter.items(), key=lambda pair: (-pair[1], pair[0])):
            percentage = count / total * 100.0
            formatted_entries.append(f'{item}: {count} ({percentage:.1f}%)')

        LOG.info(util.format_compact_string_list(formatted_entries))

    def calc_letter_distribution(self) -> Counter[str]:
        """Calculate letter frequency distribution for the words already in the dictionary."""
        lettercounter: Counter[str] = Counter()
        for entry in self.existing_entries:
            word_field = entry.get('word', '')
            words = util.split_on_commas(word_field.lower())

            for word in words:
                if ' ' in word:
                    continue  # Skip multi-word expressions
                rest = word

                while rest:
                    letter = rest[0:2]
                    if letter in util.DIPHTHONGS_SET:
                        lettercounter[letter] += 1
                        rest = rest[2:]
                    else:
                        letter = rest[0]
                        if letter not in "'-":  # skip punctuation
                            lettercounter[letter] += 1
                        rest = rest[1:]
        return lettercounter

    def print_letter_distribution(self) -> None:
        """Print letter frequency distribution for the words already in the dictionary."""
        lettercounter = self.calc_letter_distribution()
        vowelcounter: Counter[str] = Counter()
        conscounter: Counter[str] = Counter()

        # Print separate statistics for vowels and consonants
        for letter, count in lettercounter.items():
            if letter[0] in SIMPLE_VOWELS:
                vowelcounter[letter] = count
            else:
                conscounter[letter] = count

        self.print_frequency_distribution(
            'Vowel frequency distribution (ignoring multi-word expressions):', vowelcounter)
        self.print_frequency_distribution(
            'Consonant frequency distribution (ignoring multi-word expressions):', conscounter)

    @staticmethod
    def update_infl_counts(infl_counts: Dict[str, float], infl_field: str) -> None:
        """Increment the influence statistics based on the "infl" field of one entry."""
        taglist = util.split_on_commas(infl_field)
        tagset: Set[str] = set()

        for tag in taglist:
            if tag not in SOURCE_LANG_CODES:
                tagset.add(OTHER)
            elif tag in tagset:
                LOG.warn(f'Duplicated language tag in "infl" field: {tag}.')
            else:
                tagset.add(tag)

        local_infl = 1.0 / len(tagset)
        for tag in tagset:
            infl_counts[tag] += local_infl

    def calc_influences(self) -> Tuple[Dict[str, float], int, int]:
        """Calculate the influence distribution among source languages in the existing dictionary.

        Returns a tuple of three values:

        1. A mapping from language total to the influence of that language. The total of all
           influences will add up to 1.0. (The only exception is an empty dictionary, in which
           case each influence will be 0.0.)
        2. The number of words derived from source languages.
        3. The total number of words in the dictionary.
        """
        source_langs = SOURCE_LANG_CODES + [OTHER]
        infl_counts = {lang : 0.0 for lang in source_langs}
        derived_words = 0
        total_words = len(self.existing_entries)

        for entry in self.existing_entries:
            infl_field = entry.get('infl')

            if infl_field:
                derived_words += 1
                self.update_infl_counts(infl_counts, infl_field)

        # Skip OTHER entry, if there are no other languages
        if infl_counts[OTHER] == 0.0:
            source_langs = SOURCE_LANG_CODES

        if derived_words:
            # Normalize influences so they add up to 1.0
            infl_dict = {lang : infl_counts[lang] / derived_words for lang in source_langs}
        else:
            # No derived words yet, so no language has any influence
            infl_dict = infl_counts

        return infl_dict, derived_words, total_words

    @staticmethod
    def calc_infl_penalties(infl_dict: Dict[str, float]) -> Dict[str, float]:
        """Calculate the influence penalties (PI) for each source language.

        Usually, PI will be 0.0 for the lowest-influence language, 1.0 for the highest-influence
        language, and equally distributed values for the languages in between (0.071, 0.143 ..
        0.929) if there are 15 source languages. If there are several languages with the
        same influence, they all receive the same (average) penalty instead – e.g. the average
        between 0.071 and 0.143 would be 0.107.

        The tag used for OTHER languages (not among the standard source languages) is skipped
        (with a dummy penalty set to 0.0).
        """
        result = {}
        lang_count = len(infl_dict) - 1
        if OTHER in infl_dict:
            lang_count -= 1
        step = 1.0 / lang_count
        # List of languages in ascending order of influence, with those with the same influence
        # grouped together
        grouped_langs: List[List[str]] = []
        last_infl = -0.1
        last_penalty: Optional[float] = None

        for lang, infl in sorted(infl_dict.items(), key=lambda pair: (pair[1], pair[0])):
            if lang == OTHER:
                result[lang] = 0.0
                continue
            if infl == last_infl:
                grouped_langs[-1].append(lang)
            else:
                grouped_langs.append([lang])
            last_infl = infl

        for langs in grouped_langs:
            first_penalty = 0 if last_penalty is None else last_penalty + step
            last_penalty = first_penalty + (len(langs) - 1) * step
            avg_penalty = (first_penalty + last_penalty) / 2
            for lang in langs:
                result[lang] = avg_penalty

        return result

    @staticmethod
    def print_influences(infl_dict: Dict[str, float], infl_penalties: Dict[str, float],
                         derived_words: int, total_words: int) -> None:
        """Print influence percentages and penalties for each language.

        Sorted first by descending percentage and then alphabetically.

        'derived_words' is the number of words derived from source languages.

        'total_words' is the total number of words in the dictionary.
        """
        LOG.info(f'{derived_words} of {total_words} entries directly derived from source '
                 'languages.')
        LOG.info('Influence distribution and penalties:')
        formatted_entries = []

        for tag, infl in sorted(infl_dict.items(), key=lambda pair: (-pair[1], pair[0])):
            percentage = infl * 100.0
            penalty = infl_penalties[tag]
            lang = tag + ':'
            if tag == OTHER:
                formatted_entries.append(f'{lang:5} {percentage:3.1f}%')
            else:
                formatted_entries.append(f'{lang:4} {percentage:4.1f}% (PI: {penalty:5.3f})')

        LOG.info(util.format_compact_string_list(formatted_entries))

    @staticmethod
    def add_infl_penalties(infl_penalties: Dict[str, float],
                           candidates: Dict[str, Sequence[Candidate]]) -> None:
        """Store the influence penalty (PI) in each candidate."""
        for lang, lang_cands in candidates.items():
            if not lang:
                continue  # Skip candidate specified by --word argument
            # For languages specified via --consider, PI is set to 0
            pinf = infl_penalties.get(lang, 0.0)
            for cand in lang_cands:
                cand.pinf = pinf

    @staticmethod
    @lru_cache(maxsize=None)
    def calc_distance(word: str, other_word: str) -> Tuple[float, bool]:
        """Return the normalized edit distance between two candidate words.

        The edit distance (ED) is normalized by dividing it by the average length of the two
        words.

        We due this to avoid giving an undue advantage to short words, which will typically
        have a lower edit distance.

        Another method of normalization would be to divide by the length of the longer word –
        in that way, the normalized distance would reach from 0 (best) to 1 (worst).
        However, this normalization approach would give an undue advantage to longer words,
        since all comparisons with them would be divided by a high number (their own length).
        To avoid this, we use the average instead.

        Also return True if the two words are similar enough to be considered related.
        This is the case, if the ED divided by (this time) the length of the longer word is 0.5
        or less OR if the longer word starts or ends with the shorter word, provided that the
        latter has at least 2 letters (discounting any outer filler 'ə').
        """
        # If both words start or end with 'ə', we do the comparison without that filler vowel.
        # Otherwise e.g. "kubə" and "sabə" would be considered related, through they share
        # only one actual (non-filler) letter.
        if word.startswith('ə') and other_word.startswith('ə'):
            return VocBuilder.calc_distance(word[1:], other_word[1:])
        if word.endswith('ə') and other_word.endswith('ə'):
            return VocBuilder.calc_distance(word[:-1], other_word[:-1])

        # Edit distance is symmetric, hence we always pass the arguments in alphabetic order
        # to avoid needless recalculations
        if word > other_word:
            # pylint: disable=arguments-out-of-order
            return VocBuilder.calc_distance(other_word, word)

        # Inner 'ə' is no longer converted to 'e' since that would lead to exaggerated
        # similarities between words that have 'e' and those that actually have no vowel
        # in that position
        edist = editdistance.eval(word, other_word)
        avg_len = (len(word) + len(other_word)) / 2
        if not avg_len:
            LOG.warn(f'Average length of "{word}" and "{other_word}" is {0}')
        norm_ed = edist / avg_len
        related = edist / max(len(word), len(other_word)) <= 0.5

        if not related:
            if len(word) < len(other_word):
                shorter = word
                longer = other_word
            else:
                shorter = other_word
                longer = word
            related = len(shorter) >= 2 and (longer.startswith(shorter)
                                             or longer.endswith(shorter))

        return norm_ed, related

    def calc_sim_penalties(self, lang: str, candidates: Dict[str, Sequence[Candidate]]) -> None:
        """Calculate raw similarity penalties (PS) for candidates of one language.

        The lower the PS, the more similar a candidate is to the candidate words in the other
        languages. The result will be stored in the candidate itself; any related candidates
        are also stored in the candidate.

        Pseudo-candidates (empty language code as used by the --word option) are NOT considered
        as related.
        """
        my_cands = candidates[lang]
        for cand in my_cands:
            pen = 0.0
            for other_lang in candidates.keys():
                if other_lang == lang or not other_lang or not candidates[other_lang]:
                    continue
                min_dist = 1000.0

                for other_cand in candidates[other_lang]:
                    dist, related = self.calc_distance(cand.word, other_cand.word)
                    if dist < min_dist:
                        min_dist = dist
                    if related:
                        cand.related_cands[other_lang].append(other_cand)

                # We add the lowest distance to the penalty
                pen += min_dist

            # Store penalty in the candidate itself
            cand.raw_psim = pen

    @staticmethod
    def store_normalized_sim_penalties(candidates: Dict[str, Sequence[Candidate]]
                                       ) -> Sequence[Candidate]:
        """Store the normalized PS in each candidate.

        The normalized PS is the raw PS normalized to that it will be 0.0 for the lowest (best)
        raw PS and 1.0 for the highest (worst) raw PS.

        The candidate are modified in-place; a unified list of all candidates is returned for
        convenience. Note that pseudo-candidates (empty language code as used by the --word
        option) are omitted from the returned list.
        """
        result: List[Candidate] = []
        for lang_cands in candidates.values():
            if lang_cands and lang_cands[0].lang:
                result += lang_cands

        if result:
            max_psim = max(result, key=lambda cand: cand.raw_psim).raw_psim
            min_psim = min(result, key=lambda cand: cand.raw_psim).raw_psim
            diff = max_psim - min_psim
        else:
            max_psim = min_psim = diff = 0

        LOG.info(f'Raw PS range: {min_psim:.2f}..{max_psim:.2f}.')
        for cand in result:
            if diff:
                cand.psim = (cand.raw_psim - min_psim) / diff
            else:
                # Special case: there is only one candidate (or all have exactly the same PS)
                cand.psim = 0
        return result

    def min_length(self, entry: LineDict) -> int:
        """Determine the minimum length (in sounds) candidates should fulfill.

        For words that belong to the 'CONTENT_CLASSES', the minimum length is 3, unless the
        --allowshort option has been specified.

        Otherwise there is no specific minimum length (1 will be returned).
        """
        if self.args.allowshort:
            LOG.info(self.format_msg('Allowing candidates that would otherwise be considered '
                                     f'too short, rationale: {self.args.allowshort}'))
            return 1
        # If the entry has multiple classes (e.g. "verb, noun"), we consider the first one
        first_class = util.split_on_commas(entry.get('class', ''))[0]
        return 3 if first_class in CONTENT_CLASSES else 1

    def export_entry(self, entry: LineDict, discard_brackets: bool = True) -> LineDict:
        """Prepare an entry for export into the dictionary.

        This method ensure that the fields of the returned entry will be serialized in the
        proper order:

        Order of serialization:
        1. the word itself
        2. fields that aren't translations in alphabetic order (class, sense etc.)
        3. translations in alphabetic order

        The 'transcount' key from the original entry is skipped altogether.

        Semicolons listing multiple translations are changed into commas for consistency.

        If discard_brackets is False, bracketed annotations (giving IPA or a romanization)
        in translation entries are preserved (e.g. 'hi: हड्डी [haḍḍī], अस्थि [asthi]'), otherwise
        they are discarded ('hi: हड्डी, अस्थि'). The former is useful for showing the entry before
        it is actually chosen, while the latter is used for the actual export.
        """
        # pylint: disable=too-many-branches
        if self.args.core:
            # Add "core" tag
            self.add_or_append_field('tags', 'core', entry)
        if self.args.tags:
            # Add specified tag(s)
            self.add_or_append_field('tags', self.args.tags, entry)
        if self.args.field:
            # Add custom fields
            for name, value in self.args.field:
                self.add_or_append_field(name, value, entry)

        other_keys = []
        trans_keys = []

        for key in entry:
            if key in ('transcount', 'word'):
                continue
            if len(key) <= 3:
                # It's a translation listing
                trans_keys.append(key)
                orig_value = entry[key]
                value = orig_value.replace(';', ',')
                if discard_brackets:
                    value = util.discard_text_in_brackets(value)
                if key in ('cmn', 'wuu', 'yue'):
                    # For the Chinese languages, we also comma-separate the traditional
                    # and simplified spellings
                    value = value.replace(' /', ', ')
                if value != orig_value:
                    entry.add(key, value, allow_replace=True)
            else:
                other_keys.append(key)

        all_keys = sorted(other_keys) + sorted(trans_keys)
        word = entry['word']
        cls = entry['class']

        if cls == 'name' and entry['en'][0].isupper():
            # Names (proper nouns) are capitalized if the English version is
            word = util.capitalize(word)
        elif cls == 'prefix' and not word.endswith('-'):
            # Add a hyphen after prefixes and before suffixes, if not yet present
            word = word + '-'
        elif cls == 'suffix' and not word.startswith('-'):
            word = '-' + word

        # Now create the result itself
        result = LineDict()
        result.add('word', word)
        for key in all_keys:
            result.add(key, entry[key])
        return result

    @staticmethod
    def add_or_append_field(key: str, value: str, entry: LineDict) -> None:
        """Add a key/value pair to `entry`.

        If the key already exists in the entry, the new value will be appended after the
        existing one, separated by a comma (and a space).
        """
        if key in entry:
            entry.append_to_val(key, ', ' + value)
        else:
            entry.add(key, value)

    def add_entry_to_dict(self, entry: Optional[LineDict]) -> None:
        """Add the selected candidate entry to the output dictionary and write it to disk.

        Set 'entry' to None to indicate that one or more entries were deleted (--delete option).
        """
        # pylint: disable=too-many-branches, too-many-locals
        if entry:
            entry = self.export_entry(entry)
            out_entries: List[LineDict] = [entry]
            out_entries += self.existing_entries
        else:
            out_entries = self.existing_entries  # type: ignore

        # We sort first by the word itself, then by the word class, finally by the English
        # translation. If two words differ only by case, the capitalized one will be put first
        # ('Jungvo' precedes 'jungvo').
        sorted_entries = sorted(out_entries,
                                key=lambda entry: (entry.get('word', '').lower(),
                                                   entry.get('word'),
                                                   entry.get('class', '').lower(),
                                                   entry.get('en', '').lower()))

        # Add the word to the dictionary
        util.rename_to_backup(DICT_FILE)
        util.dump_dicts(sorted_entries, DICT_FILE)

        # Also append a new row to the CSV file – unless an option was used which means that
        # we have to (potentially) modify or delete some existing entries instead of adding a
        # one
        util.copy_to_backup(DICT_CSV)

        if (self.args.addmeaning or self.args.addenglish or self.args.addlugamun) and entry:
            with open(DICT_CSV, newline='', encoding='utf8') as csvfile:
                reader = csv.reader(csvfile)
                lines = list(reader)

                if self.args.addlugamun:
                    # The Lugamun word has changed
                    old_word = self.old_word
                    new_word = entry.get('word', '')
                    en_word = entry.get('en', '')

                    for idx, line in enumerate(lines):
                        if line[1] == old_word:
                            lines[idx] = [en_word, new_word]
                            break
                    else:
                        LOG.warn(f'Entry "{old_word}" not found in {DICT_CSV}')
                else:
                    en_word = entry.get('en', '')
                    our_word = entry.get('word', '')

                    for idx, line in enumerate(lines):
                        if line[1] == our_word:
                            lines[idx] = [en_word, our_word]
                            break
                    else:
                        LOG.warn(f'Entry "{en_word}" – "{our_word}" not found in {DICT_CSV}')

            # Reopen file for writing
            with open(DICT_CSV, 'w', newline='', encoding='utf8') as csvfile:
                writer = csv.writer(csvfile)
                writer.writerows(lines)
        elif self.args.delete or self.args.delete_only:
            words_to_keep = set()
            for out_entry in out_entries:
                words_to_keep.add(out_entry.get('word', ''))
            with open(DICT_CSV, newline='', encoding='utf8') as csvfile:
                reader = csv.reader(csvfile)
                lines = list(reader)
                lines_to_keep = []

                for line in lines:
                    if line[1] in words_to_keep:
                        lines_to_keep.append(line)
            # Reopen file for writing
            with open(DICT_CSV, 'w', newline='', encoding='utf8') as csvfile:
                writer = csv.writer(csvfile)
                writer.writerows(lines_to_keep)
        elif entry:
            with open(DICT_CSV, 'a', newline='', encoding='utf8') as csvfile:
                writer = csv.writer(csvfile)
                writer.writerow([entry.get('en'), entry.get('word')])

    def combine_entry(self, cand: Candidate, entry: LineDict) -> LineDict:
        """Combine a candidate an entry into a unified entry.

        The entry is modified in-place and also returned for convenience.
        """
        # Build influence list; order:
        # - first the donor language and any other languages that yield the same candidate,
        #   in alphabetic order
        # - then any other related languages, also in alphabetic
        first_set = cand.find_langs_with_identical_candidate()

        if cand.lang:
            # Will be empty (and is skipped) if the --word option is used
            first_set.add(cand.lang)

        second_set = set(cand.related_cands.keys()) - first_set

        # Check if English/French/Spanish should be added to the list of influences based on
        # the spelling – e.g. 'relasi' is clearly related to 'relation', but because the
        # English pronunciation is so different, the latter is nevertheless not listed in the
        # related_cands; likewise for 'historia' and French 'histoire' or for '-aje' in
        # Lugamun and Spanish
        this_word = cand.export_word().lower()
        for lang_code in ('en', 'es', 'fr'):
            if lang_code in first_set or lang_code in second_set:
                continue
            enfr_words = util.split_on_commas(entry.get(lang_code, '').lower())
            for enfr_word in enfr_words:
                if self.calc_distance(this_word, enfr_word)[1]:
                    second_set.add(lang_code)
                    break

        infl_list = sorted(first_set) + sorted(second_set)
        entry.add('infl', ', '.join(infl_list))
        entry.add('word', cand.export_word())
        return entry

    @staticmethod
    def format_msg(msg: str) -> str:
        """Format a message by line wrapping it and adding a final dot, if missing."""
        if not msg:
            raise ValueError('format_msg: msg is empty!')
        # Append final punctuation, if missing
        if not msg[-1] in '.!?':
            msg += '.'
        return textwrap.fill(msg, width=78, subsequent_indent='  ')

    def validate_equals_gloss(self, word: str, gloss: str) -> Optional[str]:
        """Validate an '=' gloss such as '=Jungvo' for 'jungvo'.

        Returns a error message if validation fails, otherwise None.
        """
        rest = gloss[1:]
        if not rest.lower() in self.existing_words_dict:
            return f'Word "{rest}" doesn\'t exist in the dictionary'
        if rest.lower() != word.lower():
            return f'"{word}" and "{rest}" are different words'
        return None

    def check_gloss_parts(self, parts: Sequence[str]) -> Tuple[Optional[str], bool, bool]:
        """Check that all parts of a gloss exist.

        Returns a tuple of three values:

        1. An error message if validation has failed, otherwise None
        2. True iff the first part is a prefix
        3. True iff the last part is a suffix
        """
        last_idx = len(parts) - 1
        errmsg = None
        first_is_prefix = False
        last_is_suffix = False

        for idx, part in enumerate(parts):
            if not part:
                errmsg = 'part is empty'
                break
            if not part.lower() in self.existing_words_dict:
                # Check if it's an affix
                if idx == 0:
                    alt_part = part + '-'
                    first_is_prefix = True
                elif idx == last_idx:
                    alt_part = '-' + part
                    last_is_suffix = True
                else:
                    alt_part = ''
                if not (alt_part and alt_part.lower() in self.existing_words_dict):
                    errmsg = f'Part "{part}" doesn\'t exist in the dictionary'
                    break
        return errmsg, first_is_prefix, last_is_suffix

    @staticmethod
    def check_that_parts_fit(word: str,
                             parts: List[str],
                             first_is_prefix: bool,
                             last_is_suffix: bool) -> Optional[str]:
        """Check that the joined parts of a glass fit the actual word.

        Returns an error message if this is not the case, None otherwise.
        """
        if last_is_suffix or not (first_is_prefix and len(parts) == 2):
            # If the last part starts with a vowel and the preceding one ends with one,
            # the latter vowel is deleted – except when that part is a prefix (without the
            # last part being a suffix) or if it's the only vowel in the word
            final_part_initial_vowel = util.initial_vowel(parts[-1])
            if final_part_initial_vowel:
                prior_part_final_vowel = util.final_vowel(parts[-2])
                if prior_part_final_vowel and util.count_vowels(parts[-2].lower()) > 1:
                    parts[-2] = parts[-2][:-len(prior_part_final_vowel)]

        prev_part = ''
        last_idx = len(parts) - 1

        for idx, part in enumerate(parts):
            prior_part_final_vowel = util.final_vowel(prev_part)
            this_part_initial_vowel = util.initial_vowel(part)

            if prior_part_final_vowel and prior_part_final_vowel == this_part_initial_vowel:
                # If joining parts would lead to the same vowel being duplicated, only one
                # instance is kept, e.g. yu+uma -> yuma, distansia+i -> distansi+i -> distansi
                parts[idx] = part[len(this_part_initial_vowel):]

            if (prior_part_final_vowel
                    and this_part_initial_vowel
                    and not (last_is_suffix and idx == last_idx)
                    and prior_part_final_vowel + this_part_initial_vowel in util.DIPHTHONGS_SET):
                # When joining two vowels would result in an accidental diphthong, 'y' is
                # inserted between them to prevent this (e.g. ko+inda -> koyinda), except
                # before suffixes (e.g. fa+u = fau)
                parts[idx - 1] += 'y'

            prev_part = part

        # 'y' is dropped before the 'i' suffix
        if parts[-1] == 'i' and parts[-2].endswith('y'):
            parts[-2] = parts[-2][:-1]

        # Check that parts fit together
        joined_parts = ''.join(parts)
        # We delete spaces from the gloss, since e.g. "des sis+nari" becomes "dessisnari"
        # (keeping the space in such cases would be confusing)
        spaceless_parts = joined_parts.replace(' ', '').lower()
        if spaceless_parts != word.lower():
            return f'gloss doesn\'t correspond to word "{word}" (expected "{spaceless_parts}")'
        return None

    def validate_and_store_gloss(self, word: str, gloss: str, entry: LineDict) -> None:
        """Validate and store the gloss documenting how a compound is formed.

        The gloss should be made up two or more parts separated by '+'. All parts must already
        exist in the dictionary and together they must add up to 'word'. Dies with an error if
        this is not the case.

        As a special case, '=' followed by a single part can be used by add a word that is
        identical to another word except for a change in case (e.g. 'jungvo' is glossed as
        '=Jungvo').

        If some letters are written in parentheses, they are omitted from the compound.
        For example, "Merkur(i)+den" indicates that "Merkuri" + "den" are put together to form
        the shortened compound "merkurden".

        Alternatively, an informal explanation such as "contraction of ..." can be given
        (containing spaces and no plus sign). In such cases, no validation is performed.

        After validation, the gloss is added as 'gloss' field to 'entry'.
        """
        errmsg = None
        if util.gloss_is_informal(gloss):
            LOG.info(self.format_msg(f'Adding informal gloss "{gloss}" without validation.'))
        elif gloss.startswith('='):
            errmsg = self.validate_equals_gloss(word, gloss)
        else:
            if '(' in gloss:
                # Eliminate parentheses, but not their content, to look up the parts
                gloss_wo_parens = gloss.replace('(', '').replace(')', '')
                parts_to_lookup = gloss_wo_parens.split('+')
                # But to combine the parts into a whole, the text in the parentheses is removed
                # as well
                gloss_wo_text_in_parens = util.eliminate_parens(gloss)
                parts_to_combine = gloss_wo_text_in_parens.split('+')
            else:
                parts_to_lookup = gloss.split('+')
                parts_to_combine = parts_to_lookup

            if len(parts_to_lookup) == 1:
                errmsg = "gloss must have two or more parts ('+' separated)"
            else:
                errmsg, first_is_prefix, last_is_suffix = self.check_gloss_parts(parts_to_lookup)
            if not errmsg:
                errmsg = self.check_that_parts_fit(word, parts_to_combine,
                                                   first_is_prefix, last_is_suffix)
        if errmsg:
            sys.exit(f'error validating gloss "{gloss}": {errmsg}')
        entry.add('gloss', gloss)

    def validate_compound(self, word: str, entry: LineDict) -> str:
        """Ensure that a compound is valid.

        If gloss information is provided, this returns a short string describing the gloss
        (which can be used for logging). Otherwise it returns the empty string.

        In case of two-word compounds it also checks that it's not

        * a noun/verb combination (such as "jen hafa") – these are problematic because they
          read like a phrase ('a person fears')
        * a verb/verb combination – in such cases, the first should almost certainly be an
          auxiliary, e.g. "finu cwan" instead of "fin cwan".

        Dies with an error if validation fails.
        """
        if self.args.gloss:
            gloss = self.args.gloss.strip()
            self.validate_and_store_gloss(word, gloss, entry)
            return gloss + ' = '

        # Some phrases end in a punctuation symbol
        if word and word[-1] in ('?', '!'):
            word = word[:-1]

        parts = word.split()
        class_sets: List[Set[str]] = []

        for part in parts:
            if part.endswith(','):
                # Split trailing comma in order to allow comma-separated alternatives
                part = part[:-1]

            class_set = self.existing_words_dict.get(part.lower(), set())
            class_sets.append(class_set)

            if not (class_set or part == '...'):
                sys.exit(f'validation error: Part "{part}" doesn\'t exist in the dictionary')

        if len(parts) == 2:
            # Check for problematic word class combinations
            if 'verb' in class_sets[1]:
                if 'noun' in class_sets[0]:
                    sys.exit(f'validation error: "{word}" reads like a phrase (it\'s a '
                             "noun–verb combination)")
                if 'verb' in class_sets[0]:
                    sys.exit(f'validation error: "{word}" is a verb–verb combination (the '
                             "first part should be an auxiliary)")
        return ''

    def add_as_compound(self, entry: LineDict) -> None:
        """Add the entry as a compound word as requested by the --compound argument.

        Dies with an error if the parts of the compound don't yet exist in the dictionary.
        """
        word, rationale = self.args.compound
        word = word.strip()
        rationale = rationale.strip()
        details = self.validate_compound(word, entry)
        entry.add('word', word)
        norm_word = normalize_word(word)
        if (norm_word in self.existing_norm_words
                and not (self.args.allowduplicates or entry.get('gloss', '').startswith('='))):
            sys.exit(f'error: --compound argument "{word}" (normalized: {norm_word}) '
                     'exists already in the dictionary')
        self.add_entry_to_dict(entry)
        en_word = entry.get('en', '')
        details += en_word
        full_rationale = '' if rationale == '-' else f', rationale: {rationale}'
        LOG.info(self.format_msg(f'Added compound "{word}" ({details}) to the dictionary on '
                                 + str(datetime.now())[:19]
                                 + full_rationale))
        # Append all print and warn messages to the selection log
        LOG.append_all_messages(SELECTION_LOG)

    def add_as_meaning(self, entry: LineDict) -> None:
        """'Add this entry to the meaning of another word which already exists."""
        word = self.args.addmeaning.strip()
        existing_entry = None
        other_entries = []

        for ex_en in self.existing_entries:
            if ex_en.get('word', '') == word:
                existing_entry = ex_en
            else:
                other_entries.append(ex_en)

        if not existing_entry:
            sys.exit(f'error: Word "{word}" doesn\'t exist in the dictionary')

        self.existing_entries = other_entries
        combined_entry = self.do_merge_entries(existing_entry, entry)
        matchdict, entry_langs = self.do_polysemy_check(entry)
        langset = matchdict[word]
        word_langs = self.langcode_sets.get(word, set())
        shared_lang_count = len(entry_langs.intersection(word_langs))
        percentage = len(langset) / shared_lang_count * 100.0
        if self.args.amr:
            rationale = 'rationale: ' + self.args.amr
        else:
            rationale = (f'because {len(langset)} of {shared_lang_count} languages '
                         f'({percentage:.1f}%) use the same word for both concepts')
        LOG.info(self.format_msg(f'Adding this to the meaning of "{word}" as requested, '
                                 + rationale))
        self.add_entry_to_dict(combined_entry)
        LOG.info('Dictionary updated on ' + str(datetime.now())[:19] + '.')
        # Append all print and warn messages to the selection log
        LOG.append_all_messages(SELECTION_LOG)

    def find_and_remove_lugamun_entry(self, word: str) -> LineDict:
        """Find and retrieve the existing entry for the Lugamun word 'word'.

        The entry is removed from the dictionary and returned. This allows modifying and then
        re-adding it, if desired.

        Dies with an error if the entry does not exist in the dictionary.
        """
        entry = None
        other_entries = []

        for ex_en in self.existing_entries:
            if ex_en.get('word', '') == word:
                entry = ex_en
            else:
                other_entries.append(ex_en)

        if not entry:
            sys.exit(f'error: Word "{word}" doesn\'t exist in the dictionary')
        self.existing_entries = other_entries
        return entry

    def add_english(self) -> None:
        """Add one of more words to the English translation of an existing entry.

        This implements the --addenglish option.
        """
        # pylint: disable=too-many-locals
        lugamun, english = self.args.addenglish
        lugamun = lugamun.strip()
        english = english.strip()
        entry = self.find_and_remove_lugamun_entry(lugamun)

        # If the whole new text is wrapped in parentheses, it's just an explanation
        is_explanation = english.startswith('(') and english.endswith(')')
        sep = ' ' if is_explanation else ', '
        orig_engl = entry.get('en', '')
        new_engl = orig_engl + sep + english
        entry.add('en', new_engl, allow_replace=True)

        # If there are new translations, we set the sense of each of them to '-'
        if not is_explanation:
            # Check if we need to add each word to the sense to allow disambiguation
            orig_engl_split = util.split_on_commas(orig_engl)
            orig_sense = entry.get('sense', '')
            orig_sense_split = orig_sense.split(' | ')

            if len(orig_engl_split) != len(orig_sense_split):
                # Sense disambiguation is necessary since the numbers of existing words and
                # senses differ
                mod_subsenses = []
                for idx, subsense in enumerate(orig_sense_split):
                    # Old senses: add word in parentheses if it doesn't yet seem to be there
                    if util.split_text_and_explanation(subsense)[1] is None:
                        if idx >= len(orig_engl_split):
                            # Re-use the last original word for this sense
                            matching_en_word = orig_engl_split[-1]
                        else:
                            matching_en_word = orig_engl_split[idx]
                        mod_subsense = f'{subsense} ({matching_en_word})'
                    else:
                        mod_subsense = subsense
                    mod_subsenses.append(mod_subsense)
                initial_sense = ' | '.join(mod_subsenses)
                new_sense_entry = '- (-)'
            else:
                new_sense_entry = '-'
                initial_sense = orig_sense

            new_trans_count = len(util.split_on_commas(english))
            new_senses = (' | ' + new_sense_entry) * new_trans_count
            joint_sense = initial_sense + new_senses
            entry.add('sense', joint_sense, allow_replace=True)

        self.add_entry_to_dict(entry)
        LOG.info(self.format_msg(f'Added "{english}" to the English translation of "{lugamun}" '
                                 f'on {util.current_datetime()}'))
        LOG.append_all_messages(SELECTION_LOG)

    def add_lugamun(self) -> None:
        """Add a synonym to an existing Lugamun entry.

        This implements the --addlugamun option.
        """
        old_word, new_word, rationale = self.args.addlugamun
        old_word = old_word.strip()
        new_word = new_word.strip()
        entry = self.find_and_remove_lugamun_entry(old_word)

        # If it includes spaces, check that new_word is a valid compound
        if ' ' in new_word:
            self.validate_compound(new_word, entry)
        if self.args.gloss:
            self.validate_and_store_gloss(new_word, self.args.gloss.strip(), entry)

        joint_word = f'{new_word}, {old_word}' if self.args.first else f'{old_word}, {new_word}'
        entry.add('word', joint_word, allow_replace=True)
        self.old_word = old_word  # storing this since we'll need to adjust the csv file
        self.add_entry_to_dict(entry)
        position = 'before' if self.args.first else 'after'

        # Rationale may be omitted if a gloss is given
        rationale_msg = '' if self.args.gloss and rationale == '-' else f'; rationale: {rationale}'
        gloss_info = f' ({self.args.gloss})' if self.args.gloss else ''

        LOG.info(self.format_msg(f'Added "{new_word}"{gloss_info} as a synonym {position} '
                                 f'"{old_word}" on {util.current_datetime()}{rationale_msg}'))
        LOG.append_all_messages(SELECTION_LOG)

    def add_to_dict(self, cand_entry: LineDict, choice: int) -> None:
        """Add the selected candidate entry to the output dictionary.

        If choice is set to -1, this signals that the word specified through the --word option
        was used instead.

        Also adds a log of everything that happened to the selectionlog file.
        """
        self.add_entry_to_dict(cand_entry)
        if choice == -1:
            word_rationale = self.args.word[1].strip()
            LOG.info(self.format_msg(f'Specified word "{cand_entry["word"]}" added to the '
                                     f'dictionary on {util.current_datetime()}, rationale: '
                                     + word_rationale))
        else:
            LOG.info(f'Candidate #{choice} "{cand_entry["word"]}" added to the dictionary on '
                     + util.current_datetime() + '.')
            if self.args.sr:
                LOG.info(self.format_msg(f'Selection rationale: {self.args.sr}'))

        # Append all print and warn messages to the selection log
        LOG.append_all_messages(SELECTION_LOG)

    def print_cands(self, cand_list: Sequence[Candidate], min_length: int) -> Sequence[Candidate]:
        """Print candidates in order of penalty.

        A number is added before each candidate that is not SKIPPED, and all these candidates will
        be added to the result list.

        Unless the --allowomitted option is used, candidates that aren't from the top-5
        languages (or from the --consider option) are omitted altogether if they don't have a
        related candidate in other language.

        Candidates with a distortion penalty of 0.4 or higher are SKIPPED as well.

        The candidate with the best total penalty will be prefixed with a star.
        """
        # pylint: disable=too-many-branches, too-many-locals
        if not cand_list:
            return []  # Nothing to print
        best_penalty_cand = min(cand_list, key=lambda cand: cand.total_penalty)

        if self.args.allowomitted:
            LOG.info(self.format_msg('Allowing candidates that would otherwise be omitted, '
                                     f'rationale: {self.args.allowomitted}'))
            langs_to_skip = set()
        else:
            langs_to_skip = set(ADDITIONAL_SOURCE_LANG_CODES)
            if self.args.consider:
                extra_lang_codes = set(util.split_on_commas(self.args.consider))
                langs_to_skip = langs_to_skip - extra_lang_codes

        # Sort by number of related candidates
        cands_by_relcount: Dict[int, List[Candidate]] = defaultdict(list)
        for cand in cand_list:
            cands_by_relcount[len(cand.related_cands)].append(cand)

        result = []
        num = 1
        omitted_count = 0

        for relcount, cands in sorted(cands_by_relcount.items(), key=lambda pair: -pair[0]):
            plural = '' if relcount == 1 else 's'
            LOG.info(f'++ Candidates with {relcount} related candidate{plural}:')

            for cand in sorted(cands, key=lambda cand: cand.total_penalty):
                norm_word = normalize_word(cand.export_word())
                star = '*' if cand == best_penalty_cand else ' '

                if relcount == 0 and cand.lang in langs_to_skip:
                    omitted_count += 1
                    continue
                if norm_word in self.existing_norm_words and not self.args.allowduplicates:
                    prefix = f'[SKIPPED (word exists already)]{star}  '
                elif len(norm_word) < min_length:
                    prefix = f'[SKIPPED (too short)]{star} '
                elif cand.pcon >= 0.4:
                    prefix = f'[SKIPPED (too distorted)]{star} '
                else:
                    prefix = f'[{num}]{star} '
                    num += 1
                    result.append(cand)

                LOG.info(prefix + cand.show_info())

        if omitted_count:
            LOG.info(self.format_msg(f'{omitted_count} candidates have been omitted since they '
                                     "don't belong to the top-5 languages and don't have a "
                                     'related candidate.'))
        return result

    def present_cands_for_selection(self, cand_list: Sequence[Candidate],
                                    candidates: Dict[str, Sequence[Candidate]],
                                    entry: LineDict) -> None:
        """Print the list of candidates in order of preferences.

        Numbers are added before each eligible candidate, allowing the user to make their choice.

        Candidates are sorted:

        1. by the number of related candidates
        2. by total penalty (in ascending order)

        Candidates will be marked as SKIPPED and not numbered if

        * they are identical to a word that already exists in the dictionary
        * they are too short (less than 3 sounds for content words)

        Only candidates that aren't SKIPPED can be selected.

        If no choice was made by the user, this will also print the entry that would result if
        the first candidate is selected. Or if a choice was already made, that entry will be
        written to the output file and the choice recorded in the selection log, completing the
        selection process.
        """
        if self.args.allowduplicates:
            LOG.info('Allowing candidates that are duplicates of already existing words as '
                     'requested.')
        min_length = self.min_length(entry)
        sorted_eligible_list = self.print_cands(cand_list, min_length)

        if self.args.select is not None:
            choice = self.args.select
            if choice <= 0 or choice > len(sorted_eligible_list):
                sys.exit(f'error: Choose a candidate in the range from 1 to '
                         f'{len(sorted_eligible_list)}, not {choice}')
            if choice > 1 and not self.args.sr:
                sys.exit('error: You must specify a rationale for your choice (-sr option)')

            # Add selected candidate to the dictionary
            cand_entry = self.combine_entry(sorted_eligible_list[choice - 1], entry)
            self.add_to_dict(cand_entry, choice)
        elif self.args.word:
            # Add specified word to the dictionary
            self.store_specified_word(candidates, entry)
        else:
            # Print sample entry
            if sorted_eligible_list:
                LOG.info('++ The following entry will result if the first candidate is selected:')
                cand_entry = self.export_entry(self.combine_entry(sorted_eligible_list[0], entry),
                                               False)
                LOG.info(util.stringify_dict(cand_entry).strip())
            else:
                LOG.info('++ Word has no eligible candidates!')

    def add_specified_word(self, candidates: Dict[str, Sequence[Candidate]]):
        """Add the word specified by the --word option to the candidates dictionary.

        An empty language code is used as key.

        This also validates that the word is phonetically valid and doesn't yet exist in the
        dictionary.
        """
        word_to_use = self.args.word[0].strip()
        cand = Candidate(word_to_use, 0, '', word_to_use)
        val_err = cand.validate()
        if val_err:
            sys.exit(f'error: --word argument is phonetically invalid: {val_err}')
        norm_word = normalize_word(cand.export_word())
        if norm_word in self.existing_norm_words and not self.args.allowduplicates:
            sys.exit(f'error: --word argument "{cand.export_word()}" (normalized: {norm_word}) '
                     'exists already in the dictionary')
        candidates[''] = [cand]

    def build_candidates(self, entry: LineDict) -> Dict[str, Sequence[Candidate]]:
        """Build and return candidate words for each languages."""
        if self.args.ignoremissing:
            LOG.info('Suppressing warnings about missing candidate words as requested.')
        candidates: Dict[str, Sequence[Candidate]] = {}
        lang_codes = SOURCE_LANG_CODES
        if self.args.consider:
            extra_lang_codes = util.split_on_commas(self.args.consider)
            LOG.info(f'Also considering candidates from {", ".join(extra_lang_codes)} as '
                     'requested.')
            lang_codes = lang_codes + extra_lang_codes
        for langcode in lang_codes:
            candidates[langcode] = self.build_candidates_for_lang(langcode, entry)
        if self.args.word:
            self.add_specified_word(candidates)
        return candidates

    def do_polysemy_check(self, entry: LineDict) -> Tuple[Dict[str, Set[str]], Set[str]]:
        """Check whether one might add this meaning to an existing word.

        Returns:

        1. A dictionary of words in our languages to sets of languages which use the same word
           for both meanings. Matching is case-insensitive.
        2. The set of languages for which the meaning has translations.

        Auxlangs in the input dictionary (Esperanto etc.) are generally ignored when doing these
        comparisons.
        """
        # Mapping from our words to languages which share this meaning
        matchdict: Dict[str, Set[str]] = defaultdict(set)
        entry_langs = set()

        for key, translations in entry.items():
            if len(key) > 3 or key in util.COMMON_AUXLANGS:
                # We're only interested in translations, which have at most 3 letters
                # (language codes); auxlangs are ignored
                continue
            entry_langs.add(key)

            for trans in util.split_on_commas(translations):
                full_trans = f'{key}:{trans}'.lower()
                our_words = self.polyseme_dict.get(full_trans, set())
                for our_word in our_words:
                    matchdict[our_word].add(key)
                    ##LOG.info(f'{key} uses the same word')

        return matchdict, entry_langs

    def check_polysemy(self, entry: LineDict) -> None:
        """Check whether one might add this meaning to an existing word.

        If yes, print suitable suggestions.
        """
        matchdict, entry_langs = self.do_polysemy_check(entry)
        requested_check = bool(self.args.polycheck)

        if requested_check and not matchdict:
            # Invoked through --polycheck option, so we need to print a status update anyway
            LOG.info(self.format_msg("Polysemy check: Don't consider merging these two concepts "
                                     '– no languages (0.0%) use the same word for both concepts.'))

        for word, langset in sorted(matchdict.items(), key=lambda pair: (-len(pair[1]), pair[0])):
            word_langs = self.langcode_sets.get(word, set())
            shared_lang_count = len(entry_langs.intersection(word_langs))

            if not shared_lang_count:
                if requested_check:
                    LOG.info("Sorry, but the two words don't have any languages in common!")
                continue

            percentage = len(langset) / shared_lang_count * 100.0

            if percentage < 33.3 and not requested_check:
                ## LOG.info(f'--- {word}: {percentage:.1f}% ---')
                continue
            if percentage > 66.6:
                intro = 'Urgently consider'
            elif percentage >= 50:
                intro = 'Do consider'
            elif percentage > 33.3:
                intro = 'Consider'
            else:
                # Only relevant if this is a requested check (--polycheck), where results should
                # be printed in any case
                intro = "Don't consider"

            # The recommended action depends on whether or not we are in a requested check
            if requested_check:
                action = 'merging these two concepts'
            else:
                action = f'adding this meaning to "{word}"'

            langlist = ', '.join(sorted(langset))
            LOG.info(self.format_msg(f'Polysemy check: {intro} {action} – {len(langset)} of '
                                     f'{shared_lang_count} languages ({percentage:.1f}%) use '
                                     f'the same word for both concepts ({langlist}).'))

    def store_specified_word(self, candidates: Dict[str, Sequence[Candidate]],
                             entry: LineDict) -> None:
        """Add the word specified by the --word option to the dictionary.

        This word must now already exist in the dictionary using an empty string as language code.

        If the specified word is NOT related to the words candidates yielded by any of our
        source languages, it is rejected with an error message.
        """
        cand_entry = self.combine_entry(candidates[''][0], entry)
        # Ensure that 'infl' field isn't empty
        if not cand_entry.get('infl', ''):
            sys.exit(f'error: --word argument "{cand_entry.get("word", "")}" is not related to '
                     'any word from our source languages')
        self.add_to_dict(cand_entry, -1)

    def select_candidate(self, candidates: Dict[str, Sequence[Candidate]],
                         entry: LineDict) -> None:
        """Calculate penalties and let the user select one candidate.

        If the --class option is specified, the class of the entry will be set accordingly.

        If the --compound option is used, the specified compound will be added instead.
        """
        # Adjust class if the --class option is used
        if self.args.cls:
            entry.add('class', self.args.cls.strip(), allow_replace=True)

        if self.args.compound is not None:
            self.add_as_compound(entry)
            return
        if self.args.addmeaning is not None:
            self.add_as_meaning(entry)
            return

        # Check whether one might add this meaning to an existing word (unless the --copy
        # option has been used, which would make this pointless)
        if not self.args.copy:
            self.check_polysemy(entry)

        # Calculate and store influence penalties
        infl_dict, derived_words, total_words = self.calc_influences()
        infl_penalties = self.calc_infl_penalties(infl_dict)
        self.print_influences(infl_dict, infl_penalties, derived_words, total_words)
        self.add_infl_penalties(infl_penalties, candidates)

        self.print_letter_distribution()

        # Calculate similarity penalties
        for lang in candidates:
            self.calc_sim_penalties(lang, candidates)
        cand_list = self.store_normalized_sim_penalties(candidates)
        self.present_cands_for_selection(cand_list, candidates, entry)

    def filter_entries_by_kind(self, entries: List[LineDict]) -> List[LineDict]:
        """Remove entries of the wrong kind to ensure a balanced dictionary.

        Also remove spurious entries occasionally produced by the wiktextract parser.

        Sometimes there were entries with the same translations and sense, but different
        word classes, see https://github.com/tatuylonen/wiktextract/issues/57.
        This function removes any such spurious entries, returning only entries that
        should actually be added.

        That issue is fixed now, but this code stays around in case that some similar problem
        shows up.

        Note that the result list may be empty.
        """
        entries_by_key: Dict[str, List[LineDict]] = defaultdict(list)

        for entry in entries:
            if self.get_kind(entry) not in self.kinds_to_add:
                # Entry has the wrong kind (e.g. 'noun' if we want to add an adjective)
                continue
            entry_key = self.mk_entry_key(entry)
            entries_by_key[entry_key].append(entry)

        result = []

        for entry_key, entry_list in entries_by_key.items():
            if len(entry_list) > 1:
                LOG.info(f'Note: found {len(entry_list)} entries for "{entry_key}" -- will just '
                         'keep the first noun')
                for entry in entry_list:
                    if entry.get('class') == 'noun':
                        result.append(entry)
                        break
                else:
                    LOG.warn('No noun found!')
            else:
                result += entry_list

        return result

    def select_cand_dict_to_handle_first(self,
                                         cand_dict_list: List[Dict[str, Sequence[Candidate]]],
                                         entries: List[LineDict]) -> Tuple[
                                             Dict[str, Sequence[Candidate]], LineDict]:
        """Select the candidate dictionary that should be added first.

        Returns the selected candidate dictionary and the corresponding entry (the element
        at the same position of the 2nd argument).

        To make the choice, all candidates are sorted alphabetically (using the form:
        "word/langcode", where word is the external form of the word and langcode is the
        language code).
        """
        # Sanity check
        if len(cand_dict_list) != len(entries):
            raise ValueError('Both arguments to select_cand_dict_to_handle_first must have the '
                             'same number of elements')

        if len(cand_dict_list) == 1:
            ## Trivial choice
            return cand_dict_list[0], entries[0]

        list_of_cand_lists = []

        # Build lists of candidates and sort them alphabetically
        for cand_dict in cand_dict_list:
            cand_list = []
            for cands in cand_dict.values():
                for cand in cands:
                    cand_list.append(f'{cand.export_word()}/{cand.lang}')
            cand_list.sort()
            list_of_cand_lists.append(cand_list)

        # Choose the list that comes alphabetically first and its position
        pos, first_list = min(enumerate(list_of_cand_lists), key=itemgetter(1))
        selected_cand_dict = cand_dict_list[pos]
        selected_entry = entries[pos]
        en_word = selected_entry['en']
        LOG.info(self.format_msg(f'Will handle "{en_word}" first because it has the '
                                 f'alphabetically first candidate: {first_list[0]}.'))
        return selected_cand_dict, selected_entry

    def find_entry(self, entry_map: Dict[int, List[LineDict]], word: str, sense: str) -> LineDict:
        """Find the entry with the specified English word and word sense.

        Dies with an error if the entry cannot be found.
        """
        for entry_list in entry_map.values():
            for entry in entry_list:
                if (util.discard_text_in_brackets(entry.get('en', '')) == word
                        and entry.get('sense') == sense):
                    if self.args.typ:
                        # If the --type option was used, the class must match as well
                        if entry.get('class') == self.args.typ:
                            return entry
                    else:
                        return entry
        sys.exit(f'error: No matching entry found for "{word}" ({sense})')

    def process_requested_entry(self, entry_map: Dict[int, List[LineDict]]) -> None:
        """Process the entry requested by the --add command."""
        word, sense, rationale = self.args.add
        # This allows adding a space in front of the word starts with a hyphen (otherwise
        # argparse won't accept it)
        word = word.strip()

        with_opt = ' with copy option turned on' if self.args.copy else ''
        if self.args.schwastrip:
            if with_opt:
                with_opt = with_opt.replace('option', 'and schwastrip options')
            else:
                with_opt = ' with schwastrip option turned on'

        LOG.info(self.format_msg(
            f'Processing entry "{word}" ({sense}) as requested{with_opt}, rationale: {rationale}'))
        entry = self.find_entry(entry_map, word, sense)

        if self.args.merge:
            cand_dict, entry = self.merge_entries(entry, entry_map)
        else:
            cand_dict = self.build_candidates(entry)
        self.select_candidate(cand_dict, entry)

    def mk_requested_polycheck(self, entry_map: Dict[int, List[LineDict]]) -> None:
        """Check whether it is reasonable to consider two concepts as meanings of the same word.

        This is triggered by the --polycheck option and will print a suitable recommendation
        based on how many of the widely spoken languages use the same word for them.
        """
        word_1, sense_1, word_2, sense_2 = self.args.polycheck
        word_1 = word_1.strip()
        word_2 = word_2.strip()

        if word_1 == word_2 and sense_1 == sense_2:
            sys.exit('error: There is no point in comparing an entry with itself!')

        entry_1 = self.find_entry(entry_map, word_1, sense_1)
        entry_2 = self.find_entry(entry_map, word_2, sense_2)

        # Rebuild polyseme_dict and langcode_sets, using just the first entry as contents
        entry_1.add('word', POLY_DUMMY)
        self.polyseme_dict = self.fill_polyseme_dict([entry_1])
        self.langcode_sets = self.fill_langcode_sets([entry_1])

        # Do check on second entry
        self.check_polysemy(entry_2)

    @staticmethod
    def do_merge_entries(entry: LineDict, other_entry: LineDict) -> LineDict:
        """Merge the given entry (entry) with the one specified by the --merge option (other_entry).

        Translations will be merged by adding all those of the second entry except for
        those already listed in the first entry.

        Senses are merged by separated them with a ' | ' (a pipe character surrounded by
        spaces).

        If both entries have the same word class, it won't be duplicated; otherwise, the two
        classes will be separated by a comma.

        The "transcount" field is merged by keeping the larger value.
        """
        # pylint: disable=too-many-branches, too-many-locals, too-many-statements
        combined_entry = LineDict()

        for key, value in entry.items():
            value_2 = other_entry.get(key, '')
            if not value_2:
                joint_value = value
            elif key == 'class':
                if value == value_2:
                    joint_value = value
                elif ',' in value:
                    # The first is a list such as 'verb, noun': only append the second value
                    # if it's not yet part of the list
                    existing_classes = set(util.split_on_commas(value))
                    if value_2 in existing_classes:
                        joint_value = value
                    else:
                        joint_value = value + ', ' + value_2
                else:
                    joint_value = value + ', ' + value_2
            elif key == 'sense':
                joint_value = value + ' | ' + value_2
            elif key == 'transcount':
                joint_value = str(max(int(value), int(value_2)))
            else:
                words = set(util.split_on_commas(value))
                joint_word_list = [value]
                for word in util.split_on_commas(value_2):
                    if word not in words:
                        joint_word_list.append(word)
                joint_value = ', '.join(joint_word_list)
            combined_entry.add(key, joint_value)

        # Check if sense disambiguation is possible without problems
        sense = combined_entry['sense']
        senses = sense.split(' | ')
        en_word = combined_entry['en']
        en_words = util.split_on_commas(en_word)

        if len(en_words) > 1 and len(senses) != len(en_words):
            # This can happen if the new word is already part of the existing translations, e.g.
            # en: declare, declaration  (new "declaration" ignored since it's already listed)
            # sense: to make a declaration | act or process of declaring | written or oral
            #        indication of a fact, opinion, or belief
            # We disambiguate this by adding the word in parentheses at the end of each sense,
            # resulting in
            # sense: to make a declaration (declare) | act or process of declaring (declaration)
            #        | written or oral indication of a fact, opinion, or belief (declaration)
            orig_en_word = entry['en']
            orig_en_words = util.split_on_commas(orig_en_word)

            new_subsenses = []
            for idx, subsense in enumerate(senses[:-1]):
                # Old senses: add word in parentheses if it doesn't yet seem to be there
                if util.split_text_and_explanation(subsense)[1] is None:
                    if idx >= len(orig_en_words):
                        # Re-use the last original word for this sense
                        matching_en_word = orig_en_words[-1]
                    else:
                        matching_en_word = orig_en_words[idx]
                    # If the matching word contains an explanation in parentheses, we strip it
                    matching_en_word = util.eliminate_parens(matching_en_word)
                    new_subsense = f'{subsense} ({matching_en_word})'
                else:
                    new_subsense = subsense
                new_subsenses.append(new_subsense)

            # Finally add the sense from the new entry
            new_subsense = f"{other_entry.get('sense', '')} ({other_entry.get('en', '')})"
            new_subsenses.append(new_subsense)
            joint_sense = ' | '.join(new_subsenses)
            combined_entry.add('sense', joint_sense, allow_replace=True)

        # Copy any additional entries from the second entry
        for key, value in other_entry.items():
            if key not in combined_entry:
                combined_entry.add(key, value)
        return combined_entry

    def merge_entries(self, entry: LineDict, all_entries: Dict[int, List[LineDict]]) -> Tuple[
            Dict[str, Sequence[Candidate]], LineDict]:
        """Merge the given entry with the one specified by the --merge option.

        See 'do_merge_entries' for details.
        """
        word, sense, rationale = self.args.merge
        if self.args.addmeaning:
            sys.exit("error: please don't use --merge and --addmeaning together; instead add "
                     'each meaning separately')
        # This allows adding a space in front of the word starts with a hyphen (otherwise
        # argparse won't accept it)
        word = word.strip()
        first_word = entry.get('en', '')
        first_sense = entry.get('sense', '')
        LOG.info(self.format_msg(f'Merging "{first_word}" ({first_sense}) and "{word}" ({sense}) '
                                 f'as requested, rationale: {rationale}'))
        other_entry = self.find_entry(all_entries, word, sense)
        combined_entry = self.do_merge_entries(entry, other_entry)
        cand_dict = self.build_candidates(combined_entry)
        return cand_dict, combined_entry

    def process_entry_batch(self, entries: List[LineDict], transcount: int,
                            all_entries: Dict[int, List[LineDict]]) -> int:
        """Process a batch of entries, building candidate words and selecting the best ones.

        Returns the number of entries handled.
        """
        entries = self.filter_entries_by_kind(entries)
        if not entries:
            return 0

        entry_str = 'entry' if len(entries) == 1 else 'entries'
        LOG.info(f'Processing {len(entries)} {entry_str} with {transcount} translations.')
        cand_dict_list: List[Dict[str, Sequence[Candidate]]] = []
        for entry in entries:
            cand_dict_list.append(self.build_candidates(entry))
        cand_dict, entry = self.select_cand_dict_to_handle_first(cand_dict_list, entries)
        if self.args.merge:
            cand_dict, entry = self.merge_entries(entry, all_entries)
        self.select_candidate(cand_dict, entry)
        return len(entries)

    def delete_word(self) -> None:
        """Delete the specified word and optionally its derivatives from the dictionary.

        Derivatives are deleted if the --delete is used, but not if the --delete-only option
        is used.

        The word to delete and the reason for the deletion are read from the specified option.
        """
        # pylint: disable=too-many-branches, too-many-locals
        if self.args.delete_only:
            word, reason = self.args.delete_only
            delete_derivatives = False
            runs_required = 1
        else:
            word, reason = self.args.delete
            delete_derivatives = True
            runs_required = 2

        word = word.strip()
        word_lower = word.lower()
        words_to_delete: Set[str] = set([word])
        words_to_delete_lower: Set[str] = set([word_lower])

        if delete_derivatives:
            if ',' in word_lower:
                # We also add each of the comma-separated synonyms separately, to find their
                # derivatives
                for synonym in util.split_on_commas(word_lower):
                    words_to_delete_lower.add(synonym)

            # Also add a hyphen-stripped versions in case of affixes, to find uses in glosses
            for this_word in words_to_delete_lower.copy():
                if this_word.startswith('-') or this_word.endswith('-'):
                    words_to_delete_lower.add(this_word.strip('-'))

        word_seen = False

        # If --delete is used, we run this code twice to also catch indirect derivatives,
        # e.g. "jen brasili" is derived from "brasili" which is derived from "Brasil"
        for _idx in range(runs_required):
            entries_to_keep: List[LineDict] = []
            for entry in self.existing_entries:
                this_word = entry.get('word', '')
                this_word_lower = this_word.lower()
                this_gloss = entry.get('gloss', '')
                parts = this_word_lower.split() if ' ' in this_word_lower else []

                if word_lower == this_word_lower or (delete_derivatives and any(
                        deletable_word in parts for deletable_word in words_to_delete_lower)):
                    if word == this_word:
                        word_seen = True
                    else:
                        words_to_delete.add(this_word)
                        words_to_delete_lower.add(this_word.lower())
                elif delete_derivatives and this_gloss:
                    parts = this_gloss.lower().split('+')
                    if any(deletable_word in parts for deletable_word in words_to_delete_lower):
                        words_to_delete.add(this_word)
                        words_to_delete_lower.add(this_word.lower())

                if this_word not in words_to_delete:
                    entries_to_keep.append(entry)
            self.existing_entries = entries_to_keep

        if not word_seen:
            sys.exit(f'error: "{word}" not found in the dictionary')

        self.add_entry_to_dict(None)
        words_joined = '"' + '", "'.join(words_to_delete) + '"'
        LOG.info(self.format_msg(
            f'Deleted {words_joined} on {util.current_datetime()}; reason: {reason}'))
        # Append all print and warn messages to the selection log
        LOG.append_all_messages(SELECTION_LOG)

    def run(self) -> None:
        """Main function: build our vocabulary or perform the specified command(s)."""
        if self.args.delete or self.args.delete_only:
            self.delete_word()
            return
        if self.args.addenglish:
            self.add_english()
            return
        if self.args.addlugamun:
            self.add_lugamun()
            return

        count_map = self.sort_entries_by_transcount()
        addcount = 0

        if self.args.add:
            self.process_requested_entry(count_map)
        elif self.args.polycheck:
            self.mk_requested_polycheck(count_map)
        else:
            # If no specific entry was requested, we add one of the entries with the highest
            # number of translations
            for count, entries in sorted(count_map.items(), key=lambda pair: -pair[0]):
                addcount += self.process_entry_batch(entries, count, count_map)
                if addcount >= 1:
                    break  # We process just one word at a time


##### ArgumentParser and main entry point #####

def build_arg_parser() -> argparse.ArgumentParser:
    """Build a parser for the arguments this script can handle."""
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--select', type=int, help='the candidate to add to the dictionary')
    parser.add_argument('-sr', help='the selection rationale -- must be specified whenever any '
                                    'other but the 1st candidate is selected')
    parser.add_argument('-a', '--add', metavar=('WORD', 'SENSE', 'RATIONALE'), nargs=3,
                        help='the entry to add next, requires three arguments: the English word, '
                             'the specific word sense, and a rationale for why this word is to '
                             'be added now')
    parser.add_argument('-ae', '--addenglish', metavar=('LUGAMUN', 'ENGLISH'), nargs=2,
                        help='Add one or more words to the English translation of an existing '
                             "entry; multiple translations are comma-separated; it's also "
                             'possible to include an explanation in parentheses or to put the '
                             'whole addition in parentheses; sample including both: "nobody, '
                             'anyone, anybody (in negated sentences)"')
    parser.add_argument('-al', '--addlugamun', metavar=('OLD_WORD', 'NEW_WORD', 'RATIONALE'),
                        nargs=3,
                        help='Add NEW_WORD as a synonym to the Lugamun entry OLD_WORD; requires '
                             "a rationale as third argument (can be set to '-' if a gloss is "
                             'specified instead)')
    parser.add_argument('-am', '--addmeaning', metavar='WORD',
                        help='Add this entry to the meaning of another word which already exists '
                             "in the dictionary -- unless the merger was suggested by the "
                             'polysemy check, the reason should be added using the -amr argument')
    parser.add_argument('-amr', help='the rationale for calling --addmeaning -- if not specified, '
                                     'it will be assumed that the decision was due to the '
                                     'polysemy check')
    parser.add_argument('-ad', '--allowduplicates', action='store_true',
                        help='Allow candidates that are duplicates of already existing words')
    parser.add_argument('-ao', '--allowomitted', metavar='RATIONALE',
                        help='Allow candidates that would otherwise be omitted; requires a '
                             'rationale for this decision')
    parser.add_argument('-as', '--allowshort', metavar='RATIONALE',
                        help='Allow candidates that would otherwise be considered too short; '
                             'requires a rationale for this decision')
    parser.add_argument('-cl', '--class', dest='cls', metavar='CLASS',
                        help='Set the class of the new entry to the specified value.')
    parser.add_argument('-c', '--compound', metavar=('WORD', 'RATIONALE'), nargs=2,
                        help='Add the next entry as compound instead of choosing one of the '
                             'candidates; requires two arguments: the compound to add and a '
                             'rationale for why this word is to be formed in this manner (if the '
                             "latter is obvious, e.g. in the case of phrases, it can be set to '-')"
                        )
    parser.add_argument('--consider', metavar='LANGS',
                        help='Also consider candidates from the listed language code or '
                             '(comma-separated) codes when building candidates. This allows '
                             'adding words from languages that are not usually sources. Note: '
                             'If you add an xx-ipa field to extradict.txt, it will be used to '
                             'make the candidate, otherwise the raw (original) form of the '
                             'word will be used.')
    parser.add_argument('--copy', action='store_true',
                        help='This can be used together with --add to reprocess an entry that '
                             'already exists in the dictionary (e.g. it was used to add the '
                             'entry "human being" (person) to "san" in addition to "insan")')
    parser.add_argument('--core', action='store_true',
                        help='Tag this entry as "core" vocabulary (this is a shortcut for '
                             '--field tags core)')
    parser.add_argument('--delete', metavar=('WORD', 'REASON'), nargs=2,
                        help='Delete the specified word (as well as all of its derivatives!) '
                             'from the dictionary; requires a reason for the deletion.')
    parser.add_argument('--delete-only', metavar=('WORD', 'REASON'), nargs=2,
                        help='Delete the specified word, but not its derivatives; requires a '
                             'reason for the deletion. When using this, you have to ensure that '
                             'you won\'t leave the dictionary in an inconsistent state!')
    parser.add_argument('-f', '--field', metavar=('NAME', 'VALUE'), action='append', nargs=2,
                        help='Add an additional field to the entry; this option can be called '
                             'several times (with different names); it is useful to add fields '
                             'that are present in some, but not all entries (e.g. "sample", '
                             '"tags", "value")')
    parser.add_argument('-1', '--first', action='store_true',
                        help='If this is combined with the --addlugamun option, NEW_WORD will be '
                             'added in front of OLD_WORD rather than after it')
    parser.add_argument('-g', '--gloss',
                        help="shows the parts ('+' separated) of a compound that's a single word, "
                             'e.g. "dudes" should be glossed as "du+des"; alternatively, an '
                             'informal explanation such as "contraction of ..." can be given '
                             '(containing spaces)')
    parser.add_argument('-im', '--ignoremissing', action='store_true',
                        help='If this is specified, no warnings about words missing in some of '
                             'the source languages are printed – use this if (e.g.) a certain '
                             'particle genuinely has no equivalent in some languages')
    parser.add_argument('-m', '--merge', metavar=('WORD', 'SENSE', 'RATIONALE'), nargs=3,
                        help="the entry that'll be added next will be merged with the specified "
                             'entry, that is, all translations will be combined (eliminating '
                             'duplicates) and candidates will be derived from all of them; '
                             'arguments as for --add')
    parser.add_argument('-pc', '--polycheck', metavar=('WORD_1', 'SENSE_1', 'WORD_2', 'SENSE_2'),
                        nargs=4,
                        help='check whether it is reasonable to consider two concepts as meanings '
                             'of the same word (explicit polysemy check)')
    parser.add_argument('--schwastrip', action='store_true',
                        help='Normally this option should not be used, but it can be used in '
                             'exceptional cases to strip the filler vowels (schwas) inserted '
                             'for phonetic reasons at the start or end of some candidates. '
                             'NOTE that this can lead to invalid candidates!!')
    parser.add_argument('-t', '--type', dest='typ', metavar='CLASS',
                        help='Sometimes the same word/sense combination occurs twice in the '
                             'dictionary, with different word classes. In this case, this '
                             'argument can be used to specify which of them should be selected. '
                             'Don\'t confuse this with the --class option, which can be used to '
                             'change the class of a word added to the dictionary!')
    parser.add_argument('--tags',
                        help='Add the specified tag or tags (multiple tags should be '
                             'comma-separated and listed in alphabetic order)')
    parser.add_argument('-w', '--word', metavar=('WORD', 'RATIONALE'), nargs=2,
                        help="Use the specified word to represent the entry that'll be added "
                             "next – this allows adding a form that doesn't exactly correspond "
                             'to any of the proposed candidates. Note that the internally used '
                             'form must be used: diphthongs are written as aI/aU/oI, no '
                             'apostrophe is used. A rationale explaining the chosen form must '
                             'be supplied as 2nd argument.')
    return parser


if __name__ == '__main__':
    # pylint: disable=invalid-name
    builder = VocBuilder(build_arg_parser().parse_args())
    builder.run()
