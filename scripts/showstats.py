#!/usr/bin/env python3
"""Prints various informative statistics for the Lugamun dictionary.

Must be called from the directory containing the dictionary.

Can be invoked with one argument that specified the language code of the translations to add,
e.g. 'en' for 'English' or 'ja' for 'Japanese'. English is the default.

Requires the matplotlib library.
"""

from datetime import date
import itertools
import re
from typing import Any, Counter, Dict, List, Optional, Sequence, Set, Tuple
from warnings import warn

import cycler
import matplotlib.pyplot as plt

from buildvoc import VocBuilder, SOURCE_LANG_CODES
import buildvoc
from util import LineDict, ValueProvider
import util


##### Helper functions #####

def create_voc_builder() -> VocBuilder:
    """Create a VocBuilder in order to get at the statistics it calculates."""
    dummy_args = ['-ae', 'wata', 'dummy']  # needed to suppress "Should add a ..." message
    return VocBuilder(buildvoc.build_arg_parser().parse_args(dummy_args))


def extract_lang_mapping(row: Sequence[str], filename: str) -> Tuple[str, str]:
    """
    Extract language codes (2nd field) and names (1st field) from a CSV file.

    Additional fields are ignored.

    Returns a mapping from codes to names.

    This works similar to 'util.extract_key_val_ignoring_extras', except that keys and values
    are swapped and that hyphen-separated codes are split as described.
    """
    fieldcount = len(row)
    if fieldcount < 2:
        warn(f'Error parsing {filename}: Row "{",".join(row)}" has {fieldcount} '
             'fields, expected at least 2')
    val = row[0] if fieldcount else ''
    key = row[1] if fieldcount >= 2 else ''
    return (key, val)


##### Main class #####

class StatsPrinter:
    """Prints various informative statistics for the Lugamun dictionary."""

    def __init__(self, lang: str) -> None:
        """Create a new instance, using 'lang' as language."""
        self.entries: Sequence[LineDict] = util.read_dicts_from_file(util.DICT_FILE)
        self.value_provider = ValueProvider(lang)
        self.is_first = True  # will be set False after the first header was printed
        self.code2lang = self.create_lang_codes_mapping()
        self.voc_builder = create_voc_builder()
        self.derived_words: Optional[int] = None

    @staticmethod
    def create_lang_codes_mapping() -> Dict[str, str]:
        """
        Read a mapping from source language codes to language names from sourcelangs.csv.

        Names are read from the first field, codes from the second one.

        If the code field contains slashes, a separate entry will be created for each
        slash-separated code. All will map to the same language name, even if the name field
        contains slashes as well. For example, if the first two fields are "Hindi/Urdu,hi/ur",
        both "hi" and "ur" will map to "Hindi/Urdu" (that's by design, not by oversight).

        An entry mapping 'others' to 'Others' is added as well.
        """
        raw_dict: Dict[str, str] = util.read_dict_from_csv_file(
            'sourcelangs.csv', converter=extract_lang_mapping)
        result_dict: Dict[str, str] = {}

        # Iterate over entries to handle slashes
        for key, val in raw_dict.items():
            key_parts = key.split('/')
            for key_part in key_parts:
                result_dict[key_part] = val

        result_dict['others'] = 'Others'  # add an entry for 'others'
        return result_dict

    def print_header(self, text: str, level: int = 5) -> None:
        """Print 'text' as a DokuWiki header.

        'level' is the header level as used by DokuWiki – from 6 (top level) to 1 (smallest
        level). The default is 5.
        """
        if self.is_first:
            self.is_first = False
        else:
            print()  # Print newline before header
        markers = '=' * level
        print(f'{markers} {text} {markers}')

    @staticmethod
    def print_item(text: str) -> None:
        """Print 'text' as a DokuWiki list item."""
        print(f'  * {text}')

    def print_main_header(self) -> None:
        """Print main header and intro line."""
        self.print_header('Dictionary statistics', 6)
        intro = self.value_provider.lookup('Intro')
        intro = intro.replace('(TODAY)', str(date.today()))
        print()
        print(f'**{intro}**')

    def do_print_influences(self, infl_dict: Dict[str, float]) -> None:
        """Print influence percentages for each language.

        Sorted first by descending percentage and then alphabetically.
        """
        for tag, infl in sorted(infl_dict.items(), key=lambda pair: (-pair[1], pair[0])):
            percentage = infl * 100.0
            lang = self.code2lang.get(tag, tag + ' (?)')
            lang += ':'
            self.print_item(f'{lang} {percentage:3.1f}%')

    def generate_influence_piechart(self, infl_dict: Dict[str, float]) -> None:
        """Generate and add a pie chart showing the influence distribution."""
        lang_labels = []
        percentages = []

        for tag, infl in sorted(infl_dict.items(), key=lambda pair: (-pair[1], pair[0])):
            percentage = infl * 100.0
            lang = self.code2lang.get(tag, tag + ' (?)')
            # Keep just the first word of multi-word names such as "Mandarin Chinese:"
            lang_labels.append(lang.split(' ')[0])
            percentages.append(percentage)

        # We add one color to the color cycle since we need 11 instead of the default 10
        plt.rcParams['axes.prop_cycle'] = cycler.concat(plt.rcParams['axes.prop_cycle'],
                                                        cycler.cycler(color=['gold']))
        # Plot and save the pie chart
        plt.pie(percentages, labels=lang_labels, autopct='%1.1f%%', counterclock=False,
                explode=[0.0] * 10 + [0.1], radius=1.2, startangle=90)
        plt.savefig('influence_chart.png', dpi=300)

    def insert_image(self, filename: str, title: str):
        """Insert an image into the output.

        'filename' is the name of the image text, 'title' is the title text (that will
        by shown on mouseover by typical browsers).
        """
        print(f'{{{{{filename}|{title}}}}}')

    def print_influence_stats(self) -> None:
        """Print influence statistics for languages."""
        self.print_header('Influence distribution')
        self.insert_image('influence_chart.png', 'Influence distribution')
        infl_dict, derived_words, total_words = self.voc_builder.calc_influences()
        self.do_print_influences(infl_dict)
        self.generate_influence_piechart(infl_dict)
        print(f'{derived_words} of {total_words} entries directly derived from source languages.')
        # We store this so we can check it later
        self.derived_words = derived_words

    def sanity_check_derived_words(self, derived_words) -> None:
        """Sanity-check that we counted the same number of derived words as the VocBuilder."""
        if derived_words != self.derived_words:
            raise ValueError(f'We counted {derived_words} derived words, but VocBuilder counted '
                             f'{self.derived_words}')

    def print_related_stats(self) -> None:
        """Print which fraction of the vocabulary is related to words from the source languages.

        Also prints which combinations of two or three source languages occur most often.
        """
        self.print_header('Related vocabulary percentages')
        self.insert_image('related_voc_shares.png', 'Related vocabulary percentages')
        rel_counts = {lang : 0 for lang in SOURCE_LANG_CODES}
        derived_words = 0
        lang_triple_counter: Counter[Tuple[str, str, str]] = Counter()
        lang_pair_counter: Counter[Tuple[str, str]] = Counter()

        for entry in self.voc_builder.existing_entries:
            infl_field = entry.get('infl')
            if infl_field:
                derived_words += 1
                tagset = set(util.split_on_commas(infl_field))
                for tag in tagset:
                    if tag in rel_counts:
                        rel_counts[tag] += 1

                # Increment pairs and triples of source languages
                sorted_tags = sorted(tagset)
                lang_triples = itertools.combinations(sorted_tags, 3)
                for lang_triple in lang_triples:
                    lang_triple_counter[lang_triple] += 1
                lang_pairs = itertools.combinations(sorted_tags, 2)
                for lang_pair in lang_pairs:
                    lang_pair_counter[lang_pair] += 1

        # Convert into percentages and print stats together with explanation
        rel_fractions = {lang : count / derived_words for lang, count in rel_counts.items()}
        self.do_print_influences(rel_fractions)
        print(self.value_provider.lookup('RelVocInfo'))
        self.sanity_check_derived_words(derived_words)
        self.generate_relvoc_barchart(rel_fractions)

        self.print_header('Most common source language combinations')
        self.print_source_language_combination_stats(lang_triple_counter, derived_words, 'three')
        print()
        self.print_source_language_combination_stats(lang_pair_counter, derived_words, 'two')
        print()
        print(self.value_provider.lookup('LangCombiInfo'))

    def generate_relvoc_barchart(self, rel_fractions: Dict[str, float]) -> None:
        """Generate and add a bar chart showing the related vocabulary percentages."""
        percentages: List[float] = []  # Heights of bars
        tick_labels: List[str] = []  # Labels for bars

        for tag, infl in sorted(rel_fractions.items(), key=lambda pair: (-pair[1], pair[0])):
            lang = self.code2lang.get(tag, tag + ' (?)')
            # 'Hindi/Urdu' is short enough, but we shorten "Mandarin Chinese" and
            # "Indonesian/Malay" by keeping just the first word
            if len(lang) > 12:
                lang = re.split('[/ ]', lang)[0]
            # Prepend line break before each even label to prevent the labels from overlapping
            # each other
            if len(tick_labels) % 2:
                lang = '\n' + lang
            tick_labels.append(lang)
            percentages.append(infl * 100)

        indexes = list(range(1, len(rel_fractions) + 1))  # X-coordinates of left sides of bars
        # Use colors from standard color cycle
        prop_cycle = plt.rcParams['axes.prop_cycle']
        colors = prop_cycle.by_key()['color']
        # Show percentage values above bars
        _fig, axes = plt.subplots()
        bars = axes.bar(indexes, percentages)
        axes.bar_label(bars, fmt='%1.1f%%', size=8)
        # Plot bar chart
        plt.bar(indexes, percentages, tick_label=tick_labels, color=colors)
        plt.ylabel('Percentage')  # Name the y-axis
        plt.title('Related vocabulary')  # Plot title
        plt.savefig('related_voc_shares.png', dpi=300)  # Store the chart

    def print_source_language_combination_stats(self, combination_counter: Counter[Any],
                                                derived_words: int, number_str: str) -> None:
        """Print which combinations of source languages occur most often.

        'derived_words' is the total number of entries derived from source languages.

        'number_str' is the string representation of the number of languages in each
        combination, e.g. 'two' or 'three'.

        Entries are sorted from most to least frequent. The 15 first entries are all printed,
        afterwards entries are only printed if they mention a new language (such as Mandarin,
        which is relatively rare in combinations).

        At the first mention, each language is printed in bold.
        """
        line_count = 0
        langs_mentioned = set()
        source_lang_count = len(SOURCE_LANG_CODES)
        skipped = False
        print(f'Combinations involving {number_str} languages:')

        for combi, count in sorted(combination_counter.items(),
                                   key=lambda pair: (-pair[1], pair[0])):
            percentage = count / derived_words * 100.0
            # To prevent confusion, we keep just the first element of slash-separated entries
            # such as 'Hindi/Urdu' and 'Indonesian/Malay', and of multi-word entries such as
            # 'Mandarin Chinese'
            language_list = [re.split('[/ ]', self.code2lang.get(tag, tag + ' (?)'))[0]
                             for tag in combi]
            # Print each language in bold at first mention
            formatted_langs = [lang if lang in langs_mentioned else f'**{lang}**'
                               for lang in language_list]
            langs_mentioned.update(language_list)
            languages = '/'.join(formatted_langs)
            line_count += 1

            if line_count > 15:
                # Only print combination if it mentions a new source language
                if '**' in languages:
                    if skipped:
                        self.print_item('...')
                        skipped = False
                else:
                    skipped = True
                    continue

            self.print_item(f'{languages}: {percentage:3.1f}%')
            # Stop if each regular source language has been mentioned at least once
            if len(langs_mentioned) >= source_lang_count:
                break

    def print_class_statistics(self) -> None:
        """Print frequency statistics for word classes."""
        counter: Counter[str] = Counter()
        # A set of lemmas (headwords), ignoring case. Some lemmas are listed in several entries
        # (e.g. "nihon" is listed once as adjective, capitalized "Nihon" as country, and in the
        # entry "nihon, luga nihon" as one way of referring to the language), but we count them
        # only once.
        lemma_set: Set[str] = set()

        for entry in self.entries:
            cls_value = entry.get('class', '')
            if not cls_value:
                warn(f'{util.DICT_FILE} entry starting on line {entry.first_lineno()} lacks class '
                     'information')
                continue
            cls_list = util.split_on_commas(cls_value)

            # A few entries include synonyms such as "false, nesahi"
            words = util.split_on_commas(entry.get('word', ''))

            for word in words:
                lemma_set.add(word.lower())
            for cls in cls_list:
                counter[self.value_provider.lookup(cls)] += 1
            if len(cls_list) > 1:
                counter['_multi'] += 1

        self.print_header('Word class distribution')

        for cls, count in sorted(counter.items(), key=lambda pair: (-pair[1], pair[0])):
            if not cls.startswith('_'):
                self.print_item(f'{count} {cls}')

        syn_count = len(lemma_set) - len(self.entries)
        print(f'{len(lemma_set)} lemmas in total, {syn_count} of which are synonyms of another '
              f'lemma.\n{counter["_multi"]} entries belong to more than one class.')

    def print_percentage_distribution(self, counter: Counter[str], frac_digits: int = 2) -> None:
        """Print percentage statistics calculated from a counter."""
        total = sum(counter.values())
        for item, count in sorted(counter.items(), key=lambda pair: (-pair[1], pair[0])):
            percentage = count / total * 100.0
            self.print_item(f'{item}: {percentage:.{frac_digits}f}%')

    def print_sound_distribution(self) -> None:
        """Print sound (letter) frequency distributions."""
        self.print_header('Sound frequency distribution')
        lettercounter = self.voc_builder.calc_letter_distribution()
        self.print_percentage_distribution(lettercounter)
        print('Multi-word expressions are ignored.')

    @staticmethod
    def get_first_lemma(entry: LineDict) -> str:
        """Get the first Lugamun lemma from an entry.

        An entry may have several comma-separated lemmas (e.g. "false, nesahi") – in this
        case, the first of them will be returned ("false").
        """
        return util.split_on_commas(entry.get('word', ''))[0]

    def print_trivia(self) -> None:
        """Print some trivial such as the longest root."""
        self.print_header('Trivia')
        derived_words = 0
        longest_root_len = 0
        root_count = 0
        summed_root_len = 0

        # Iterate entries to find the length of the longest root
        for entry in self.voc_builder.existing_entries:
            infl_field = entry.get('infl')
            if not infl_field:
                continue
            derived_words += 1
            # All words that have an 'infl' field are considered roots
            word = self.get_first_lemma(entry)
            longest_root_len = max(longest_root_len, len(word))
            root_count += 1
            summed_root_len += len(word)

        self.sanity_check_derived_words(derived_words)

        longest_roots: List[str] = []
        max_infl_count = len(SOURCE_LANG_CODES)
        roots_with_most_influences: List[str] = []

        # Iterate again to find the actual longest roots and those with a related candidates
        # in all source languages
        for entry in self.voc_builder.existing_entries:
            infl_field = entry.get('infl')
            if not infl_field:
                continue
            word = self.get_first_lemma(entry)
            if len(word) == longest_root_len:
                longest_roots.append(word)
            if len(util.split_on_commas(infl_field)) >= max_infl_count:
                roots_with_most_influences.append(word)

        print(f'Average length of root words: {summed_root_len/root_count:.2f} letters.')
        print()
        print(f'Longest roots ({longest_root_len} letters): **{", ".join(longest_roots)}**.')
        print()
        print(f'Roots with maximum number of relations in the source languages '
              f'({max_infl_count} or more): **{", ".join(roots_with_most_influences)}**.')

        # Explain that it's occasionally possible that a word has MORE relations than
        # we have source languages
        print()
        print('(' + self.value_provider.lookup('MaxRelationsNote') + ')')

    def run(self) -> None:
        """Main function: prints various informative statistics to stdout."""
        self.print_main_header()
        self.print_influence_stats()
        self.print_related_stats()
        self.print_class_statistics()
        self.print_sound_distribution()
        self.print_trivia()


##### Main entry point #####

if __name__ == '__main__':
    # pylint: disable=invalid-name
    LANG = util.retrieve_single_arg(default='en')
    sprinter = StatsPrinter(LANG)
    sprinter.run()
