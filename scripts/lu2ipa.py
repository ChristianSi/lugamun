#!/usr/bin/env python3
"""Convert Lugamun text into IPA, showing its pronunciation.

To invoke, either specify one or more files whose contents will be converted.
Or if no filenames are specified, input will be read from stdin.

In either case, the output will be printed to stdout.
"""

from functools import lru_cache
import re

import util


##### Constants and precompiled regexes #####

IPA_STRESS = "ˈ"

# This is almost like VOWEL_RE, except that the vowel is captured
CAPTURED_VOWEL_RE = re.compile(f'({util.DIPHTHONGS_RE}|[{util.SIMPLE_VOWELS}])')

# Mapping from consonants and consonant pairs to their IPA equivalents
CONS_MAPPING = {
    'c': 't̠ʃ',
    'j': 'd̠ʒ',
    'r': 'ɾ',
    'v': 'w',
    'x': 'ʃ',
    'y': 'j',
    # For pairs, we need stress-marker-separated versions as well
    'ng': 'ŋg',
    'nk': 'ŋk',
    f'n{IPA_STRESS}g': f'ŋ{IPA_STRESS}g',
    f'n{IPA_STRESS}k': f'ŋ{IPA_STRESS}k',
}


##### Helper functions #####

def contains_illegal_letter(word: str) -> bool:
    """Return True if a word contains a letter that is not a regular part of the Lugamun alphabet.

    This is the case for 'q, w, z' as well as all non-ASCII letters.
    """
    return any(char.lower() in 'qwz'for char in word) or any(ord(char) >= 128 for char in word)


def insert_stress_marker(seq: str) -> str:
    """Insert an IPA stress marker in the proper place in a consonant sequence between two vowels.

    The sequence must contain only lower-case consonants; it may be empty.
    """
    length = len(seq)
    # If the sequence is empty or has just one consonant, insert the marker before it
    if length <= 1:
        return IPA_STRESS + seq

    # If there are two consonants between the vowels and the first of them is allowed to end a
    # syllable, insert the marker between them
    if length == 2 and seq[0] in util.FINAL_CONSONANTS:
        return seq[0] + IPA_STRESS + seq[1]

    # Otherwise insert the marker before the last two consonants
    return seq[:-2] + IPA_STRESS + seq[-2:]


def ipa_convert_vowel(vowel: str) -> str:
    """Convert a vowel into IPA notation."""
    if len(vowel) == 2:
        # The second letter of a diphthong gets a breve under it
        first = vowel[0]
        second = vowel[1]
        if second == 'i':
            sec_conv = 'i̯'
        elif second == 'u':
            sec_conv = 'u̯'
        else:
            # This should never happen
            raise ValueError(f'Unexpected diphthong: {vowel}')
        return first + sec_conv
    # Return simple vowel as is
    return vowel


def ipa_convert_consonants(seq: str) -> str:
    """Convert a sequence of consonants into IPA notation."""
    for key, val in CONS_MAPPING.items():
        seq = seq.replace(key, val)
    return seq


@lru_cache(maxsize=1048576)
def word2ipa(word: str) -> str:
    """Convert a single Lugamun word into IPA.

    If the word contains characters that aren't a part of the regular Lugamun alphabet,
    it is prefixed with a question mark (?) and otherwise left unchanged.

    The results for each word are memoized (using an 'lru_cache' wrapper) to prevent
    unnecessary double conversions.
    """
    if contains_illegal_letter(word):
        return '?' + word
    # Split into vowel – non-vowel sequences
    sequences = re.split(CAPTURED_VOWEL_RE, word.lower())
    ##last_idx = len(sequences) - 1
    vowel_count = 0
    cons_pos = []  # Store which 'sequences' are consonants

    for idx, seq in enumerate(sequences):
        if not seq:
            pass
            ## For clarity, we insert a period (.) between adjacent vowels if the first of them
            ## isn't a diphthong
            #if idx and idx < last_idx and len(sequences[idx - 1]) == 1:
            #    sequences[idx - 1] += '.'
        elif seq[0] in util.SIMPLE_VOWELS:
            # It's a vowel (possibly a diphthong)
            vowel_count += 1
        else:
            # It's a non-empty consonant sequence
            cons_pos.append(idx)

    if vowel_count >= 2:
        # Word has 2+ syllables, so we need to insert a stress marker.
        # The last vowel before the last consonant is stressed, hence we need to insert the
        # marker in the (possibly empty) consonant sequence two positions before the last
        # non-empty consonant sequence
        last_cons_pos = cons_pos[-1] if cons_pos else -1
        if last_cons_pos >= 2:
            sequences[last_cons_pos - 2] = insert_stress_marker(sequences[last_cons_pos - 2])
        else:
            # If there is no such vowel, the first syllable is stressed
            sequences[0] = IPA_STRESS + sequences[0]

    result_seq = []
    # Go again over the list to perform the necessary IPA conversions (for correct results,
    # we can to this only after the stress marker has been inserted)
    for seq in sequences:
        if not seq:
            # Empty sequence, can be skipped
            continue
        if seq[0] in util.SIMPLE_VOWELS:
            result_seq.append(ipa_convert_vowel(seq))
        else:
            result_seq.append(ipa_convert_consonants(seq))

    result = ''.join(result_seq)
    ### Discard unnecessary periods before stress markers
    ##result = result.replace('.' + IPA_STRESS, IPA_STRESS)
    return result


##### Main function and entry point #####

def lugamun2ipa(text: str) -> str:
    """Return the IPA equivalent of a Lugamun text."""
    return util.process_tokens(text, word2ipa)


if __name__ == '__main__':
    util.process_text_from_files_or_stdin(lugamun2ipa)
