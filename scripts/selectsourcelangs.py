#!/usr/bin/env python3
"""Select the source languages to be used for Lugamun."""

import csv
from typing import Set

import util

INFILE = 'top-30-langs.csv'
OUTFILE = 'sourcelangs.csv'


def selectlangs() -> None:
    """Select the source languages to be used for Lugamun.

    Input languages are read from INFILE; the subset of selected languages is written to OUTFILE.

    Fields 2 (Name) to 5 (Writing system) are copied to the output file if the language belongs
    to one of the selected languages.
    """
    util.rename_file_if_exists(OUTFILE, f'{OUTFILE}.bak')
    branches: Set[str] = set()

    with open(INFILE, newline='', encoding='utf8') as infile:
        csvreader = csv.reader(infile)
        with open(OUTFILE, 'w', newline='', encoding='utf8') as outfile:
            csvwriter = csv.writer(outfile)
            branch_count = 0

            for row in csvreader:
                fields_to_copy = row[1:6]
                langcode = fields_to_copy[1]
                branch = fields_to_copy[2]

                # Add first 8 branch representative as well as French and the first Bantu language
                # (header line is copies as well, hence '<=' instead of '<')
                if branch not in branches and (branch_count <= 8 or branch == 'Bantu'):
                    csvwriter.writerow(fields_to_copy)
                    branches.add(branch)
                    branch_count += 1
                elif langcode == 'fr':
                    csvwriter.writerow(fields_to_copy)

    print(f'Output written to {OUTFILE}')


if __name__ == '__main__':
    selectlangs()
