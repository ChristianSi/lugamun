cldf is the cldf directory within the
[cldf-wals-dataset](https://github.com/cldf-datasets/wals). If not yet
present, check out that repository and copy or symlink the cldf directory
here.
