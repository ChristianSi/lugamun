# WALS features

## Features for the "Phonology" section

"Phonology" is section 1 in WALS.

### Consonant Inventories (WALS feature 1A)

Most frequent value (8 languages):

* **Average** (#3 – Mandarin Chinese/cmn, English/en, Spanish/es, French/fr, Indonesian/id, Thai/th, Vietnamese/vi, Yue Chinese/yue)

Rarer values are "Moderately large" (#4, 3 languages), "Large" (#5, 1 language), and "Moderately small" (#2, 1 language).

### Vowel Quality Inventories (WALS feature 2A)

Most frequent value (8 languages):

* **Average (5-6)** (#2 – Egyptian Arabic/arz, cmn, es, Hindi/hi, id, Japanese/ja, Russian/ru, Swahili/sw)

Another frequent value:

* **Large (7-14)** (#3) – 5 languages (en, fr, th, vi, yue – 62% relative frequency)

### Consonant-Vowel Ratio (WALS feature 3A)

Most frequent value (4 languages):

* **Average** (#3 – cmn, es, id, ja)

Other frequent values:

* **Low** (#1) – 3 languages (en, fr, vi – 75% relative frequency)
* **Moderately high** (#4) – 3 languages (arz, hi, sw – 75% relative frequency)
* **Moderately low** (#2) – 2 languages (th, yue – 50% relative frequency)

A rarer value is "High" (#5, 1 language).

### Voicing in Plosives and Fricatives (WALS feature 4A)

Most frequent value (8 languages):

* **In both plosives and fricatives** (#4 – arz, en, fr, hi, id, ja, ru, sw)

Rarer values are "In fricatives alone" (#3, 3 languages), "No voicing contrast" (#1, 1 language), and "In plosives alone" (#2, 1 language).

### Voicing and Gaps in Plosive Systems (WALS feature 5A)

Most frequent value (7 languages):

* **None missing in /p t k b d g/** (#2 – en, fr, hi, id, ja, ru, sw)

Another frequent value:

* **Other** (#1) – 4 languages (cmn, es, vi, yue – 57% relative frequency)

Rarer values are "Missing /p/" (#3, 1 language) and "Missing /g/" (#4, 1 language).

### Uvular Consonants (WALS feature 6A)

Most frequent value (10 languages):

* **None** (#1 – cmn, en, es, hi, id, ru, sw, th, vi, yue)

Rarer values are "Uvular continuants only" (#3, 2 languages) and "Uvular stops and continuants" (#4, 1 language).

### Glottalized Consonants (WALS feature 7A)

Most frequent value (12 languages):

* **No glottalized consonants** (#1 – arz, cmn, en, es, fr, hi, id, ja, ru, sw, th, yue)

A rarer value is "Implosives only" (#3, 1 language).

### Lateral Consonants (WALS feature 8A)

Most frequent value (12 languages):

* **/l/, no obstruent laterals** (#2 – arz, cmn, en, es, fr, hi, id, ru, sw, th, vi, yue)

A rarer value is "No laterals" (#1, 1 language).

### The Velar Nasal (WALS feature 9A)

Most frequent value (6 languages):

* **No velar nasal** (#3 – arz, es, fr, hi, ja, ru)

Another frequent value:

* **Initial velar nasal** (#1) – 5 languages (id, sw, th, vi, yue – 83% relative frequency)

A rarer value is "No initial velar nasal" (#2, 2 languages).

### Vowel Nasalization (WALS feature 10A)

Most frequent value (10 languages):

* **Contrast absent** (#2 – arz, cmn, en, es, id, ja, ru, sw, th, vi)

A rarer value is "Contrast present" (#1, 2 languages).

### Front Rounded Vowels (WALS feature 11A)

Most frequent value (10 languages):

* **None** (#1 – arz, en, es, hi, id, ja, ru, sw, th, vi)

Rarer values are "High and mid" (#2, 2 languages) and "High only" (#3, 1 language).

### Syllable Structure (WALS feature 12A)

Most frequent values (6 languages):

* **Complex** (#3 – arz, en, fr, hi, id, ru)
* **Moderately complex** (#2 – cmn, es, ja, th, vi, yue)

A rarer value is "Simple" (#1, 1 language).

### Tone (WALS feature 13A)

Most frequent value (8 languages):

* **No tones** (#1 – arz, en, es, fr, hi, id, ru, sw)

Another frequent value:

* **Complex tone system** (#3) – 4 languages (cmn, th, vi, yue – 50% relative frequency)

A rarer value is "Simple tone system" (#2, 1 language).

### Fixed Stress Locations (WALS feature 14A)

Most frequent value (7 languages):

* **No fixed stress** (#1 – arz, cmn, en, es, fr, hi, ru)

A rarer value is "Penultimate" (#6, 2 languages).

### Weight-Sensitive Stress (WALS feature 15A)

Most frequent value (3 languages):

* **Right-oriented: One of the last three** (#4 – arz, en, hi)

Other frequent values:

* **Right-edge: Ultimate or penultimate** (#3) – 2 languages (es, fr – 67% relative frequency)
* **Fixed stress (no weight-sensitivity)** (#8) – 2 languages (id, sw – 67% relative frequency)

Rarer values are "Not predictable" (#7, 1 language) and "Unbounded: Stress can be anywhere" (#5, 1 language).

### Weight Factors in Weight-Sensitive Stress Systems (WALS feature 16A)

Most frequent values (2 languages):

* **Long vowel or coda consonant** (#4 – en, hi)
* **Lexical stress** (#6 – cmn, ru)
* **Combined** (#7 – arz, es)
* **No weight** (#1 – id, sw)

Another frequent value:

* **Prominence** (#5) – 1 languages (fr – 50% relative frequency)

### Rhythm Types (WALS feature 17A)

Most frequent value (4 languages):

* **Trochaic** (#1 – arz, en, es, id)

Rarer values are "Undetermined" (#4, 1 language) and "No rhythmic stress" (#5, 1 language).

### Absence of Common Consonants (WALS feature 18A)

Most frequent value (13 languages):

* **All present** (#1 – arz, cmn, en, es, fr, hi, id, ja, ru, sw, th, vi, yue)

### Presence of Uncommon Consonants (WALS feature 19A)

Most frequent value (9 languages):

* **None** (#1 – cmn, fr, hi, id, ja, ru, th, vi, yue)

Rarer values are "'Th' sounds" (#5, 3 languages) and "Pharyngeals" (#4, 1 language).

## Features for the "Morphology" section

"Morphology" is section 2 in WALS.

### Fusion of Selected Inflectional Formatives (WALS feature 20A)

Most frequent value (7 languages):

* **Exclusively concatenative** (#1 – en, es, fr, hi, ja, ru, sw)

Rarer values are "Isolating/concatenative" (#7, 2 languages), "Exclusively isolating" (#2, 2 languages), and "Ablaut/concatenative" (#6, 1 language).

### Exponence of Selected Inflectional Formatives (WALS feature 21A)

Most frequent value (7 languages):

* **No case** (#5 – arz, en, fr, id, sw, th, vi)

Another frequent value:

* **Monoexponential case** (#1) – 4 languages (cmn, es, hi, ja – 57% relative frequency)

A rarer value is "Case + number" (#2, 1 language).

### Exponence of Tense-Aspect-Mood Inflection (WALS feature 21B)

Most frequent value (8 languages):

* **monoexponential TAM** (#1 – cmn, en, id, ja, ru, sw, th, vi)

Rarer values are "TAM+agreement" (#2, 3 languages) and "TAM+agreement+diathesis" (#3, 1 language).

### Inflectional Synthesis of the Verb (WALS feature 22A)

Most frequent value (6 languages):

* **4-5 categories per word** (#3 – es, fr, id, ja, ru, sw)

Another frequent value:

* **2-3 categories per word** (#2) – 3 languages (en, hi, th – 50% relative frequency)

Rarer values are "0-1 category per word" (#1, 2 languages) and "6-7 categories per word" (#4, 1 language).

### Locus of Marking in the Clause (WALS feature 23A)

Most frequent value (5 languages):

* **No marking** (#4 – arz, fr, id, th, vi)

Another frequent value:

* **Dependent marking** (#2) – 4 languages (cmn, en, ja, ru – 80% relative frequency)

Rarer values are "Double marking" (#3, 2 languages) and "Head marking" (#1, 1 language).

### Locus of Marking in Possessive Noun Phrases (WALS feature 24A)

Most frequent value (9 languages):

* **Dependent marking** (#2 – cmn, en, es, fr, hi, ja, ru, sw, th)

A rarer value is "No marking" (#4, 3 languages).

### Locus of Marking: Whole-language Typology (WALS feature 25A)

Most frequent value (6 languages):

* **Inconsistent or other** (#5 – arz, es, fr, hi, sw, th)

Another frequent value:

* **Dependent-marking** (#2) – 4 languages (cmn, en, ja, ru – 67% relative frequency)

A rarer value is "Zero-marking" (#4, 2 languages).

### Zero Marking of A and P Arguments (WALS feature 25B)

Most frequent value (9 languages):

* **Non-zero marking** (#2 – arz, cmn, en, es, fr, hi, ja, ru, sw)

A rarer value is "Zero-marking" (#1, 3 languages).

### Prefixing vs. Suffixing in Inflectional Morphology (WALS feature 26A)

Most frequent value (9 languages):

* **Strongly suffixing** (#2 – Standard Arabic/ar, cmn, en, es, fr, hi, id, ja, ru)

Rarer values are "Little affixation" (#1, 3 languages) and "Weakly prefixing" (#5, 1 language).

### Reduplication (WALS feature 27A)

Most frequent value (6 languages):

* **Productive full and partial reduplication** (#1 – arz, cmn, hi, sw, th, vi)

Another frequent value:

* **No productive reduplication** (#3) – 4 languages (en, es, fr, ru – 67% relative frequency)

A rarer value is "Full reduplication only" (#2, 2 languages).

### Case Syncretism (WALS feature 28A)

Most frequent value (7 languages):

* **No case marking** (#1 – arz, cmn, id, ja, sw, th, vi)

Another frequent value:

* **Core and non-core** (#3) – 4 languages (es, fr, hi, ru – 57% relative frequency)

A rarer value is "Core cases only" (#2, 1 language).

### Syncretism in Verbal Person/Number Marking (WALS feature 29A)

Most frequent value (6 languages):

* **Syncretic** (#2 – arz, en, es, fr, hi, sw)

Another frequent value:

* **No subject person/number marking** (#1) – 5 languages (cmn, id, ja, th, vi – 83% relative frequency)

A rarer value is "Not syncretic" (#3, 1 language).

## Features for the "Nominal Categories" section

"Nominal Categories" is section 3 in WALS.

### Number of Genders (WALS feature 30A)

Most frequent value (5 languages):

* **None** (#1 – cmn, id, th, vi, yue)

Another frequent value:

* **Two** (#2) – 4 languages (arz, es, fr, hi – 80% relative frequency)

Rarer values are "Three" (#3, 2 languages) and "Five or more" (#5, 1 language).

### Sex-based and Non-sex-based Gender Systems (WALS feature 31A)

Most frequent value (6 languages):

* **Sex-based** (#2 – arz, en, es, fr, hi, ru)

Another frequent value:

* **No gender** (#1) – 5 languages (cmn, id, th, vi, yue – 83% relative frequency)

A rarer value is "Non-sex-based" (#3, 1 language).

### Systems of Gender Assignment (WALS feature 32A)

Most frequent value (6 languages):

* **Semantic and formal** (#3 – arz, es, fr, hi, ru, sw)

Another frequent value:

* **No gender** (#1) – 5 languages (cmn, id, th, vi, yue – 83% relative frequency)

A rarer value is "Semantic" (#2, 1 language).

### Coding of Nominal Plurality (WALS feature 33A)

Most frequent value (7 languages):

* **Plural suffix** (#2 – cmn, en, es, fr, hi, ja, ru)

Rarer values are "No plural" (#9, 1 language), "Plural word" (#7, 1 language), "Mixed morphological plural" (#6, 1 language), "Plural complete reduplication" (#5, 1 language), and "Plural prefix" (#1, 1 language).

### Occurrence of Nominal Plurality (WALS feature 34A)

Most frequent value (7 languages):

* **All nouns, always obligatory** (#6 – arz, en, es, fr, hi, ru, sw)

Rarer values are "Only human nouns, optional" (#2, 2 languages) and "All nouns, always optional" (#4, 2 languages).

### Plurality in Independent Personal Pronouns (WALS feature 35A)

Most frequent value (6 languages):

* **Person-number stem** (#4 – arz, en, hi, id, sw, th)

Another frequent value:

* **Person stem + nominal plural affix** (#8) – 3 languages (cmn, ja, yue – 50% relative frequency)

Rarer values are "Person-number stem + nominal plural affix" (#6, 2 languages), "Person stem + pronominal plural affix" (#7, 1 language), and "Person-number stem + pronominal plural affix" (#5, 1 language).

### The Associative Plural (WALS feature 36A)

Most frequent value (8 languages):

* **No associative plural** (#4 – arz, en, es, fr, hi, ru, th, vi)

Rarer values are "Unique periphrastic associative plural" (#3, 3 languages), "Unique affixal associative plural" (#2, 1 language), and "Associative same as additive plural" (#1, 1 language).

### Definite Articles (WALS feature 37A)

Most frequent value (4 languages):

* **Definite word distinct from demonstrative** (#1 – en, es, fr, vi)

Other frequent values:

* **No definite, but indefinite article** (#4) – 3 languages (ja, th, yue – 75% relative frequency)
* **No definite or indefinite article** (#5) – 2 languages (hi, ru – 50% relative frequency)
* **Demonstrative word used as definite article** (#2) – 2 languages (id, sw – 50% relative frequency)

A rarer value is "Definite affix" (#3, 1 language).

### Indefinite Articles (WALS feature 38A)

Most frequent values (3 languages):

* **Indefinite word distinct from 'one'** (#1 – en, ja, th)
* **Indefinite word same as 'one'** (#2 – es, fr, yue)

Other frequent values:

* **No definite or indefinite article** (#5) – 2 languages (hi, ru – 67% relative frequency)
* **No indefinite, but definite article** (#4) – 2 languages (arz, id – 67% relative frequency)

### Inclusive/Exclusive Distinction in Independent Pronouns (WALS feature 39A)

Most frequent value (8 languages):

* **No inclusive/exclusive** (#3 – arz, en, es, fr, hi, ja, ru, sw)

Rarer values are "Inclusive/exclusive" (#5, 2 languages) and "'We' the same as 'I'" (#2, 2 languages).

### Inclusive/Exclusive Distinction in Verbal Inflection (WALS feature 40A)

Most frequent value (6 languages):

* **No person marking** (#1 – cmn, hi, id, ja, th, vi)

Another frequent value:

* **No inclusive/exclusive** (#3) – 5 languages (arz, es, fr, ru, sw – 83% relative frequency)

A rarer value is "'We' the same as 'I'" (#2, 1 language).

### Distance Contrasts in Demonstratives (WALS feature 41A)

Most frequent value (9 languages):

* **Two-way contrast** (#2 – arz, cmn, en, id, ru, sw, Urdu/ur, vi, yue)

Rarer values are "Three-way contrast" (#3, 2 languages) and "No distance contrast" (#1, 1 language).

### Pronominal and Adnominal Demonstratives (WALS feature 42A)

Most frequent value (9 languages):

* **Identical** (#1 – arz, cmn, en, es, id, ru, sw, ur, yue)

Rarer values are "Different stem" (#2, 1 language) and "Different inflection" (#3, 1 language).

### Third Person Pronouns and Demonstratives (WALS feature 43A)

Most frequent value (5 languages):

* **Unrelated** (#1 – es, id, ja, th, yue)

Rarer values are "Related for all demonstratives" (#2, 2 languages), "Related by gender markers" (#5, 2 languages), "Related for non-human reference" (#6, 1 language), and "Related to remote demonstratives" (#3, 1 language).

### Gender Distinctions in Independent Personal Pronouns (WALS feature 44A)

Most frequent values (4 languages):

* **3rd person singular only** (#3 – cmn, en, fr, ru)
* **No gender distinctions** (#6 – hi, id, th, vi)

Other frequent values:

* **In 3rd person + 1st and/or 2nd person** (#1) – 2 languages (arz, es – 50% relative frequency)
* **3rd person only, but also non-singular** (#2) – 2 languages (ja, sw – 50% relative frequency)

### Politeness Distinctions in Pronouns (WALS feature 45A)

Most frequent values (4 languages):

* **Binary politeness distinction** (#2 – cmn, es, fr, ru)
* **Pronouns avoided for politeness** (#4 – id, ja, th, vi)

Another frequent value:

* **No politeness distinction** (#1) – 3 languages (arz, en, sw – 75% relative frequency)

A rarer value is "Multiple politeness distinctions" (#3, 1 language).

### Indefinite Pronouns (WALS feature 46A)

Most frequent value (5 languages):

* **Generic-noun-based** (#2 – arz, en, fr, id, sw)

Other frequent values:

* **Interrogative-based** (#1) – 4 languages (ja, ru, th, vi – 80% relative frequency)
* **Special** (#3) – 3 languages (es, hi, yue – 60% relative frequency)

A rarer value is "Mixed" (#4, 1 language).

### Intensifiers and Reflexive Pronouns (WALS feature 47A)

Most frequent value (8 languages):

* **Identical** (#1 – cmn, en, hi, id, ja, th, vi, yue)

Another frequent value:

* **Differentiated** (#2) – 4 languages (es, fr, ru, sw – 50% relative frequency)

### Person Marking on Adpositions (WALS feature 48A)

Most frequent value (11 languages):

* **No person marking** (#2 – cmn, en, es, fr, hi, id, ja, ru, sw, th, vi)

A rarer value is "Pronouns only" (#3, 1 language).

### Number of Cases (WALS feature 49A)

Most frequent value (8 languages):

* **No morphological case-marking** (#1 – arz, cmn, es, fr, id, sw, th, vi)

Rarer values are "2 cases" (#2, 2 languages), "6-7 cases" (#6, 1 language), and "8-9 cases" (#7, 1 language).

### Asymmetrical Case-Marking (WALS feature 50A)

Most frequent value (7 languages):

* **No case-marking** (#1 – arz, cmn, fr, id, sw, th, vi)

Rarer values are "Additive-quantitatively asymmetrical" (#3, 3 languages), "Syncretism in relevant NP-types" (#6, 1 language), and "Symmetrical" (#2, 1 language).

### Position of Case Affixes (WALS feature 51A)

Most frequent value (7 languages):

* **No case affixes or adpositional clitics** (#9 – cmn, en, es, id, th, vi, yue)

Rarer values are "Case suffixes" (#1, 3 languages), "Prepositional clitics" (#7, 1 language), "Postpositional clitics" (#6, 1 language), and "Mixed morphological case" (#5, 1 language).

### Comitatives and Instrumentals (WALS feature 52A)

Most frequent value (6 languages):

* **Differentiation** (#2 – arz, cmn, hi, ja, sw, th)

Rarer values are "Identity" (#1, 2 languages) and "Mixed" (#3, 2 languages).

### Ordinal Numerals (WALS feature 53A)

Most frequent value (5 languages):

* **First, second, three-th** (#7 – en, es, fr, ru, sw)

Another frequent value:

* **One-th, two-th, three-th** (#4) – 3 languages (cmn, ja, yue – 60% relative frequency)

Rarer values are "First, two-th, three-th" (#6, 2 languages), "First/one-th, two-th, three-th" (#5, 2 languages), and "Various" (#8, 1 language).

### Distributive Numerals (WALS feature 54A)

Most frequent value (9 languages):

* **No distributive numerals** (#1 – arz, cmn, en, es, fr, id, th, vi, yue)

Rarer values are "Marked by reduplication" (#2, 2 languages), "Marked by preceding word" (#5, 1 language), and "Marked by suffix" (#4, 1 language).

### Numeral Classifiers (WALS feature 55A)

Most frequent value (6 languages):

* **Absent** (#1 – arz, en, fr, hi, ru, sw)

Another frequent value:

* **Obligatory** (#3) – 5 languages (cmn, ja, th, vi, yue – 83% relative frequency)

A rarer value is "Optional" (#2, 1 language).

### Conjunctions and Universal Quantifiers (WALS feature 56A)

Most frequent value (7 languages):

* **Formally similar, with interrogative** (#3 – cmn, hi, id, ja, th, vi, yue)

Rarer values are "Formally similar, without interrogative" (#2, 1 language) and "Formally different" (#1, 1 language).

### Position of Pronominal Possessive Affixes (WALS feature 57A)

Most frequent value (10 languages):

* **No possessive affixes** (#4 – cmn, en, es, fr, id, ja, ru, th, vi, yue)

A rarer value is "Possessive suffixes" (#2, 2 languages).

## Features for the "Nominal Syntax" section

"Nominal Syntax" is section 4 in WALS.

### Obligatory Possessive Inflection (WALS feature 58A)

Most frequent value (12 languages):

* **Absent** (#2 – arz, cmn, en, es, fr, hi, id, ja, ru, sw, th, vi)

### Number of Possessive Nouns (WALS feature 58B)

Most frequent value (12 languages):

* **None reported** (#1 – arz, cmn, en, es, fr, hi, id, ja, ru, sw, th, vi)

### Possessive Classification (WALS feature 59A)

Most frequent value (12 languages):

* **No possessive classification** (#1 – arz, cmn, en, es, fr, hi, id, ja, ru, sw, th, vi)

### Genitives, Adjectives and Relative Clauses (WALS feature 60A)

Most frequent value (4 languages):

* **Highly differentiated** (#6 – en, fr, hi, ru)

Another frequent value:

* **Weakly differentiated** (#1) – 3 languages (cmn, id, yue – 75% relative frequency)

Rarer values are "Adjectives and relative clauses collapsed" (#4, 1 language), "Moderately differentiated in other ways" (#5, 1 language), and "Genitives and adjectives collapsed" (#2, 1 language).

### Adjectives without Nouns (WALS feature 61A)

Most frequent value (5 languages):

* **Without marking** (#2 – es, fr, ru, sw, th)

Another frequent value:

* **Marked by following word** (#6) – 4 languages (cmn, en, hi, yue – 80% relative frequency)

Rarer values are "Marked by preceding word" (#5, 2 languages) and "Marked by mixed or other strategies" (#7, 1 language).

### Action Nominal Constructions (WALS feature 62A)

Most frequent value (4 languages):

* **Ergative-Possessive** (#3 – es, fr, id, ru)

Other frequent values:

* **Mixed** (#6) – 3 languages (arz, en, th – 75% relative frequency)
* **Possessive-Accusative** (#2) – 3 languages (hi, sw, vi – 75% relative frequency)
* **No action nominals** (#8) – 2 languages (cmn, yue – 50% relative frequency)

A rarer value is "Double-Possessive" (#4, 1 language).

### Noun Phrase Conjunction (WALS feature 63A)

Most frequent value (8 languages):

* **'And' different from 'with'** (#1 – arz, en, es, fr, hi, ru, th, vi)

Another frequent value:

* **'And' identical to 'with'** (#2) – 4 languages (cmn, id, ja, sw – 50% relative frequency)

### Nominal and Verbal Conjunction (WALS feature 64A)

Most frequent value (9 languages):

* **Identity** (#1 – arz, en, es, fr, hi, id, ru, th, vi)

A rarer value is "Differentiation" (#2, 3 languages).

## Features for the "Verbal Categories" section

"Verbal Categories" is section 5 in WALS.

### Perfective/Imperfective Aspect (WALS feature 65A)

Most frequent value (7 languages):

* **Grammatical marking** (#1 – arz, cmn, es, fr, hi, ru, yue)

Another frequent value:

* **No grammatical marking** (#2) – 6 languages (en, id, ja, sw, th, vi – 86% relative frequency)

### The Past Tense (WALS feature 66A)

Most frequent value (8 languages):

* **Present, no remoteness distinctions** (#1 – arz, en, es, fr, hi, ja, ru, sw)

Another frequent value:

* **No past tense** (#4) – 5 languages (cmn, id, th, vi, yue – 62% relative frequency)

### The Future Tense (WALS feature 67A)

Most frequent value (8 languages):

* **No inflectional future** (#2 – cmn, en, id, ja, ru, th, vi, yue)

Another frequent value:

* **Inflectional future exists** (#1) – 5 languages (arz, es, fr, hi, sw – 62% relative frequency)

### The Perfect (WALS feature 68A)

Most frequent values (4 languages):

* **No perfect** (#4 – arz, cmn, ja, ru)
* **Other perfect** (#3 – hi, sw, vi, yue)

Other frequent values:

* **From possessive** (#1) – 3 languages (en, es, fr – 75% relative frequency)
* **From 'finish', 'already'** (#2) – 2 languages (id, th – 50% relative frequency)

### Position of Tense-Aspect Affixes (WALS feature 69A)

Most frequent value (8 languages):

* **Tense-aspect suffixes** (#2 – cmn, en, es, fr, hi, id, ja, yue)

Rarer values are "No tense-aspect inflection" (#5, 2 languages), "Mixed type" (#4, 2 languages), and "Tense-aspect prefixes" (#1, 1 language).

### The Morphological Imperative (WALS feature 70A)

Most frequent value (6 languages):

* **No second-person imperatives** (#5 – cmn, en, id, th, vi, yue)

Another frequent value:

* **Second singular and second plural** (#1) – 5 languages (arz, es, hi, ru, sw – 83% relative frequency)

Rarer values are "Second singular" (#2, 1 language) and "Second person number-neutral" (#4, 1 language).

### The Prohibitive (WALS feature 71A)

Most frequent value (7 languages):

* **Normal imperative + special negative** (#2 – cmn, hi, id, ja, th, vi, yue)

Rarer values are "Normal imperative + normal negative" (#1, 3 languages), "Special imperative + normal negative" (#3, 2 languages), and "Special imperative + special negative" (#4, 1 language).

### Imperative-Hortative Systems (WALS feature 72A)

Most frequent value (11 languages):

* **Neither type of system** (#4 – arz, cmn, en, es, fr, hi, id, ja, ru, th, vi)

A rarer value is "Maximal system" (#1, 1 language).

### The Optative (WALS feature 73A)

Most frequent value (12 languages):

* **Inflectional optative absent** (#2 – arz, cmn, en, es, fr, hi, id, ja, ru, sw, th, vi)

### Situational Possibility (WALS feature 74A)

Most frequent value (10 languages):

* **Verbal constructions** (#2 – arz, cmn, en, es, fr, hi, ru, th, vi, yue)

A rarer value is "Affixes on verbs" (#1, 3 languages).

### Epistemic Possibility (WALS feature 75A)

Most frequent value (9 languages):

* **Verbal constructions** (#1 – arz, cmn, en, es, fr, hi, ru, th, vi)

Rarer values are "Other" (#3, 3 languages) and "Affixes on verbs" (#2, 1 language).

### Overlap between Situational and Epistemic Modal Marking (WALS feature 76A)

Most frequent value (7 languages):

* **Overlap for both possibility and necessity** (#1 – arz, cmn, en, es, fr, ru, th)

Another frequent value:

* **Overlap for either possibility or necessity** (#2) – 5 languages (hi, ja, sw, vi, yue – 71% relative frequency)

A rarer value is "No overlap" (#3, 1 language).

### Semantic Distinctions of Evidentiality (WALS feature 77A)

Most frequent value (10 languages):

* **No grammatical evidentials** (#1 – arz, cmn, en, es, hi, id, ru, sw, th, vi)

A rarer value is "Indirect only" (#2, 3 languages).

### Coding of Evidentiality (WALS feature 78A)

Most frequent value (10 languages):

* **No grammatical evidentials** (#1 – arz, cmn, en, es, hi, id, ru, sw, th, vi)

Rarer values are "Separate particle" (#4, 1 language), "Modal morpheme" (#5, 1 language), and "Verbal affix or clitic" (#2, 1 language).

### Suppletion According to Tense and Aspect (WALS feature 79A)

Most frequent value (8 languages):

* **None** (#4 – arz, cmn, id, ja, sw, th, vi, yue)

Another frequent value:

* **Tense and aspect** (#3) – 4 languages (es, fr, hi, ru – 50% relative frequency)

A rarer value is "Tense" (#1, 1 language).

### Suppletion in Imperatives and Hortatives (WALS feature 79B)

Most frequent value (9 languages):

* **None (= no suppletive imperatives reported in the reference material)** (#5 – cmn, en, fr, hi, id, ja, th, vi, yue)

A rarer value is "Imperative" (#2, 4 languages).

### Verbal Number and Suppletion (WALS feature 80A)

Most frequent value (13 languages):

* **None** (#1 – arz, cmn, en, es, fr, hi, id, ja, ru, sw, th, vi, yue)

## Features for the "Word Order" section

"Word Order" is section 6 in WALS.

### Order of Subject, Object and Verb (WALS feature 81A)

Most frequent value (10 languages):

* **SVO** (#2 – cmn, en, es, fr, id, ru, sw, th, vi, yue)

Rarer values are "SOV" (#1, 2 languages) and "VSO" (#3, 1 language).

### Order of Subject and Verb (WALS feature 82A)

Most frequent value (11 languages):

* **SV** (#1 – cmn, en, fr, hi, id, ja, ru, sw, th, vi, yue)

Rarer values are "VS" (#2, 1 language) and "No dominant order" (#3, 1 language).

### Order of Object and Verb (WALS feature 83A)

Most frequent value (11 languages):

* **VO** (#2 – ar, cmn, en, es, fr, id, ru, sw, th, vi, yue)

A rarer value is "OV" (#1, 2 languages).

### Order of Object, Oblique, and Verb (WALS feature 84A)

Most frequent value (7 languages):

* **VOX** (#1 – ar, en, es, fr, id, th, vi)

Rarer values are "XVO" (#2, 2 languages) and "XOV" (#3, 1 language).

### Order of Adposition and Noun Phrase (WALS feature 85A)

Most frequent value (9 languages):

* **Prepositions** (#2 – ar, en, es, fr, id, ru, sw, th, vi)

Rarer values are "No dominant order" (#4, 2 languages) and "Postpositions" (#1, 2 languages).

### Order of Genitive and Noun (WALS feature 86A)

Most frequent value (8 languages):

* **Noun-Genitive** (#2 – ar, es, fr, id, ru, sw, th, vi)

Another frequent value:

* **Genitive-Noun** (#1) – 4 languages (cmn, hi, ja, yue – 50% relative frequency)

A rarer value is "No dominant order" (#3, 1 language).

### Order of Adjective and Noun (WALS feature 87A)

Most frequent value (7 languages):

* **Noun-Adjective** (#2 – ar, es, fr, id, sw, th, vi)

Another frequent value:

* **Adjective-Noun** (#1) – 6 languages (cmn, en, hi, ja, ru, yue – 86% relative frequency)

### Order of Demonstrative and Noun (WALS feature 88A)

Most frequent value (9 languages):

* **Demonstrative-Noun** (#1 – ar, cmn, en, es, fr, hi, ja, ru, yue)

A rarer value is "Noun-Demonstrative" (#2, 4 languages).

### Order of Numeral and Noun (WALS feature 89A)

Most frequent value (11 languages):

* **Numeral-Noun** (#1 – ar, cmn, en, es, fr, hi, id, ja, ru, vi, yue)

A rarer value is "Noun-Numeral" (#2, 2 languages).

### Order of Relative Clause and Noun (WALS feature 90A)

Most frequent value (9 languages):

* **Noun-Relative clause** (#1 – arz, en, es, fr, id, ru, sw, th, vi)

Rarer values are "Relative clause-Noun" (#2, 3 languages) and "Correlative" (#4, 1 language).

### Postnominal relative clauses (WALS feature 90C)

Most frequent value (9 languages):

* **Noun-Relative clause (NRel) dominant** (#1 – arz, en, es, fr, id, ru, sw, th, vi)

### Order of Degree Word and Adjective (WALS feature 91A)

Most frequent value (9 languages):

* **Degree word-Adjective** (#1 – cmn, en, es, fr, hi, id, ja, ru, yue)

A rarer value is "Adjective-Degree word" (#2, 2 languages).

### Position of Polar Question Particles (WALS feature 92A)

Most frequent values (4 languages):

* **Final** (#2 – cmn, ja, th, vi)
* **Initial** (#1 – ar, fr, hi, sw)

Another frequent value:

* **No question particle** (#6) – 2 languages (en, es – 50% relative frequency)

Rarer values are "Second position" (#3, 1 language) and "In either of two positions" (#5, 1 language).

### Position of Interrogative Phrases in Content Questions (WALS feature 93A)

Most frequent value (8 languages):

* **Not initial interrogative phrase** (#2 – arz, cmn, hi, ja, sw, th, vi, yue)

Another frequent value:

* **Initial interrogative phrase** (#1) – 4 languages (en, es, fr, ru – 50% relative frequency)

A rarer value is "Mixed" (#3, 1 language).

### Order of Adverbial Subordinator and Clause (WALS feature 94A)

Most frequent value (10 languages):

* **Initial subordinator word** (#1 – ar, en, es, fr, hi, id, ru, sw, th, vi)

Rarer values are "Mixed" (#5, 1 language) and "Final subordinator word" (#2, 1 language).

### Relationship between the Order of Object and Verb and the Order of Adposition and Noun Phrase (WALS feature 95A)

Most frequent value (9 languages):

* **VO and Prepositions** (#4 – ar, en, es, fr, id, ru, sw, th, vi)

Rarer values are "Other" (#5, 2 languages) and "OV and Postpositions" (#1, 2 languages).

### Relationship between the Order of Object and Verb and the Order of Relative Clause and Noun (WALS feature 96A)

Most frequent value (9 languages):

* **VO and NRel** (#4 – arz, en, es, fr, id, ru, sw, th, vi)

Rarer values are "VO and RelN" (#3, 2 languages), "Other" (#5, 1 language), and "OV and RelN" (#1, 1 language).

### Relationship between the Order of Object and Verb and the Order of Adjective and Noun (WALS feature 97A)

Most frequent value (7 languages):

* **VO and NAdj** (#4 – ar, es, fr, id, sw, th, vi)

Another frequent value:

* **VO and AdjN** (#3) – 4 languages (cmn, en, ru, yue – 57% relative frequency)

A rarer value is "OV and AdjN" (#1, 2 languages).

### Order of Negative Morpheme and Verb (WALS feature 143A)

Most frequent value (10 languages):

* **NegV** (#1 – ar, cmn, en, es, hi, id, ru, th, vi, yue)

Rarer values are "OptDoubleNeg" (#15, 1 language), "[V-Neg]" (#4, 1 language), and "[Neg-V]" (#3, 1 language).

### Preverbal Negative Morphemes (WALS feature 143E)

Most frequent value (11 languages):

* **NegV** (#1 – ar, cmn, en, es, fr, hi, id, ru, th, vi, yue)

Rarer values are "None" (#4, 1 language) and "[Neg-V]" (#2, 1 language).

### Postverbal Negative Morphemes (WALS feature 143F)

Most frequent value (11 languages):

* **None** (#4 – ar, cmn, en, es, hi, id, ru, sw, th, vi, yue)

Rarer values are "VNeg" (#1, 1 language) and "[V-Neg]" (#2, 1 language).

### Minor morphological means of signaling negation (WALS feature 143G)

Most frequent value (13 languages):

* **None** (#4 – ar, cmn, en, es, fr, hi, id, ja, ru, sw, th, vi, yue)

### Position of Negative Word With Respect to Subject, Object, and Verb (WALS feature 144A)

Most frequent value (8 languages):

* **SNegVO** (#2 – cmn, en, es, id, ru, th, vi, yue)

Rarer values are "MorphNeg" (#20, 2 languages), "SONegV" (#7, 1 language), "NegVSO" (#9, 1 language), and "OptDoubleNeg" (#19, 1 language).

### Position of negative words relative to beginning and end of clause and with respect to adjacency to verb (WALS feature 144B)

Most frequent value (10 languages):

* **Immed preverbal** (#3 – ar, cmn, en, es, hi, id, ru, th, vi, yue)

A rarer value is "Immed postverbal" (#4, 1 language).

### The Position of Negative Morphemes in SVO Languages (WALS feature 144D)

Most frequent value (8 languages):

* **SNegVO** (#2 – cmn, en, es, id, ru, th, vi, yue)

Rarer values are "OptNeg" (#15, 2 languages) and "S[Neg-V]O" (#5, 1 language).

### NegSVO Order (WALS feature 144H)

Most frequent value (11 languages):

* **No NegSVO** (#4 – arz, cmn, en, es, fr, id, ru, sw, th, vi, yue)

### SNegVO Order (WALS feature 144I)

Most frequent value (8 languages):

* **Word&NoDoubleNeg** (#1 – cmn, en, es, id, ru, th, vi, yue)

Rarer values are "Type 1 / Type 2" (#7, 1 language), "Word&OnlyWithAnotherNeg" (#5, 1 language), and "Prefix&NoDoubleNeg" (#2, 1 language).

### SVNegO Order (WALS feature 144J)

Most frequent value (9 languages):

* **No SVNegO** (#7 – cmn, en, es, id, ru, sw, th, vi, yue)

Rarer values are "Suffix&OnlyWithAnotherNeg" (#6, 1 language) and "Word&OptDoubleNeg" (#3, 1 language).

### SVONeg Order (WALS feature 144K)

Most frequent value (11 languages):

* **No SVONeg** (#4 – arz, cmn, en, es, fr, id, ru, sw, th, vi, yue)

### Features below the language quorum

14 features were skipped because they didn't reach the quorum of at least 5 source languages:

* 90B (Prenominal relative clauses; 3 languages)
* 90D (Internally-headed relative clauses; 1 language)
* 90E (Correlative relative clauses; 1 language)
* 143C (Optional Double Negation; 2 languages)
* 144G (Optional Double Negation in SVO languages; 2 languages)
* 144L (The Position of Negative Morphemes in SOV Languages; 2 languages)
* 144P (NegSOV Order; 2 languages)
* 144Q (SNegOV Order; 2 languages)
* 144R (SONegV Order; 2 languages)
* 144S (SOVNeg Order; 2 languages)
* 144T (The Position of Negative Morphemes in Verb-Initial Languages; 1 language)
* 144V (Verb-Initial with Preverbal Negative; 1 language)
* 144W (Verb-Initial with Negative that is Immediately Postverbal or between Subject and Object; 1 language)
* 144X (Verb-Initial with Clause-Final Negative; 1 language)

## Features for the "Simple Clauses" section

"Simple Clauses" is section 7 in WALS.

### Alignment of Case Marking of Full Noun Phrases (WALS feature 98A)

Most frequent value (8 languages):

* **Neutral** (#1 – arz, cmn, en, fr, id, sw, th, vi)

Rarer values are "Nominative - accusative (standard)" (#2, 3 languages) and "Tripartite" (#5, 1 language).

### Alignment of Case Marking of Pronouns (WALS feature 99A)

Most frequent value (6 languages):

* **Neutral** (#1 – arz, cmn, id, sw, th, vi)

Another frequent value:

* **Nominative - accusative (standard)** (#2) – 5 languages (en, es, fr, ja, ru – 83% relative frequency)

A rarer value is "Tripartite" (#5, 1 language).

### Alignment of Verbal Person Marking (WALS feature 100A)

Most frequent value (8 languages):

* **Accusative** (#2 – arz, en, es, fr, hi, id, ru, sw)

Another frequent value:

* **Neutral** (#1) – 4 languages (cmn, ja, th, vi – 50% relative frequency)

### Expression of Pronominal Subjects (WALS feature 101A)

Most frequent values (4 languages):

* **Obligatory pronouns in subject position** (#1 – en, fr, id, ru)
* **Optional pronouns in subject position** (#5 – cmn, ja, th, vi)

Another frequent value:

* **Subject affixes on verb** (#2) – 3 languages (arz, es, sw – 75% relative frequency)

### Verbal Person Marking (WALS feature 102A)

Most frequent values (4 languages):

* **Only the A argument** (#2 – en, fr, hi, ru)
* **No person marking** (#1 – cmn, ja, th, vi)

Another frequent value:

* **Both the A and P arguments** (#5) – 3 languages (arz, es, sw – 75% relative frequency)

A rarer value is "Only the P argument" (#3, 1 language).

### Third Person Zero of Verbal Person Marking (WALS feature 103A)

Most frequent value (7 languages):

* **No zero realization** (#2 – en, es, fr, hi, id, ru, sw)

Another frequent value:

* **No person marking** (#1) – 4 languages (cmn, ja, th, vi – 57% relative frequency)

A rarer value is "Zero in all 3sg forms" (#4, 1 language).

### Order of Person Markers on the Verb (WALS feature 104A)

Most frequent value (9 languages):

* **A and P do not or do not both occur on the verb** (#1 – cmn, en, fr, hi, id, ja, ru, th, vi)

Rarer values are "A precedes P" (#2, 2 languages) and "P precedes A" (#3, 1 language).

### Ditransitive Constructions: The Verb 'Give' (WALS feature 105A)

Most frequent value (6 languages):

* **Indirect-object construction** (#1 – es, fr, hi, ja, ru, th)

Another frequent value:

* **Mixed** (#4) – 5 languages (arz, cmn, en, id, yue – 83% relative frequency)

A rarer value is "Double-object construction" (#2, 2 languages).

### Reciprocal Constructions (WALS feature 106A)

Most frequent value (10 languages):

* **Distinct from reflexive** (#2 – arz, cmn, en, hi, id, ja, sw, th, vi, yue)

A rarer value is "Mixed" (#3, 3 languages).

### Passive Constructions (WALS feature 107A)

Most frequent value (12 languages):

* **Present** (#1 – arz, cmn, en, es, fr, hi, id, ja, ru, sw, th, vi)

### Antipassive Constructions (WALS feature 108A)

Most frequent value (12 languages):

* **No antipassive** (#3 – arz, cmn, en, es, fr, hi, id, ja, ru, sw, th, vi)

### Productivity of the Antipassive Construction (WALS feature 108B)

Most frequent value (12 languages):

* **no antipassive** (#4 – arz, cmn, en, es, fr, hi, id, ja, ru, sw, th, vi)

### Applicative Constructions (WALS feature 109A)

Most frequent value (10 languages):

* **No applicative construction** (#8 – arz, cmn, en, es, fr, hi, ja, ru, th, vi)

Rarer values are "Benefactive object; only transitive" (#2, 1 language) and "Benefactive and other; both bases" (#3, 1 language).

### Other Roles of Applied Objects (WALS feature 109B)

Most frequent value (10 languages):

* **No applicative construction** (#5 – arz, cmn, en, es, fr, hi, ja, ru, th, vi)

Rarer values are "No other roles (= Only benefactive)" (#4, 1 language) and "Locative" (#2, 1 language).

### Periphrastic Causative Constructions (WALS feature 110A)

Most frequent value (4 languages):

* **Purposive but no sequential** (#2 – arz, hi, ru, sw)

Other frequent values:

* **Both** (#3) – 3 languages (es, th, vi – 75% relative frequency)
* **Sequential but no purposive** (#1) – 2 languages (en, id – 50% relative frequency)

### Nonperiphrastic Causative Constructions (WALS feature 111A)

Most frequent value (7 languages):

* **Morphological but no compound** (#2 – arz, en, hi, id, ja, ru, sw)

Rarer values are "Compound but no morphological" (#3, 2 languages), "Both" (#4, 2 languages), and "Neither" (#1, 1 language).

### Negative Morphemes (WALS feature 112A)

Most frequent value (8 languages):

* **Negative particle** (#2 – arz, cmn, en, es, fr, hi, ru, yue)

Rarer values are "Negative word, unclear if verb or particle" (#4, 2 languages), "Negative affix" (#1, 2 languages), and "Negative auxiliary verb" (#3, 1 language).

### Symmetric and Asymmetric Standard Negation (WALS feature 113A)

Most frequent value (7 languages):

* **Symmetric** (#1 – arz, es, fr, id, ru, th, vi)

Another frequent value:

* **Both** (#3) – 5 languages (cmn, en, hi, sw, yue – 71% relative frequency)

A rarer value is "Asymmetric" (#2, 1 language).

### Subtypes of Asymmetric Standard Negation (WALS feature 114A)

Most frequent value (7 languages):

* **Non-assignable** (#7 – arz, es, fr, id, ru, th, vi)

Another frequent value:

* **A/Cat** (#3) – 4 languages (en, hi, sw, yue – 57% relative frequency)

Rarer values are "A/Fin" (#1, 1 language) and "A/Fin and A/Cat" (#5, 1 language).

### Negative Indefinite Pronouns and Predicate Negation (WALS feature 115A)

Most frequent value (10 languages):

* **Predicate negation also present** (#1 – arz, cmn, hi, id, ja, ru, sw, th, vi, yue)

A rarer value is "Mixed behaviour" (#3, 3 languages).

### Polar Questions (WALS feature 116A)

Most frequent value (11 languages):

* **Question particle** (#1 – ar, cmn, fr, hi, id, ja, ru, sw, th, vi, yue)

A rarer value is "Interrogative word order" (#4, 2 languages).

### Predicative Possession (WALS feature 117A)

Most frequent values (4 languages):

* **Topic** (#3 – cmn, id, th, vi)
* **Locational** (#1 – arz, hi, ja, ru)

Another frequent value:

* **'Have'** (#5) – 2 languages (en, es – 50% relative frequency)

A rarer value is "Conjunctional" (#4, 1 language).

### Predicative Adjectives (WALS feature 118A)

Most frequent value (6 languages):

* **Nonverbal encoding** (#2 – arz, en, es, fr, hi, ru)

Another frequent value:

* **Verbal encoding** (#1) – 4 languages (cmn, id, th, vi – 67% relative frequency)

A rarer value is "Mixed" (#3, 2 languages).

### Nominal and Locational Predication (WALS feature 119A)

Most frequent values (6 languages):

* **Identical** (#2 – arz, en, fr, hi, ru, sw)
* **Different** (#1 – cmn, es, id, ja, th, vi)

### Zero Copula for Predicate Nominals (WALS feature 120A)

Most frequent value (7 languages):

* **Impossible** (#1 – cmn, en, es, fr, hi, ja, sw)

Another frequent value:

* **Possible** (#2) – 5 languages (arz, id, ru, th, vi – 71% relative frequency)

### Comparative Constructions (WALS feature 121A)

Most frequent values (4 languages):

* **Particle** (#4 – en, es, fr, ru)
* **Exceed** (#2 – cmn, sw, th, vi)

Another frequent value:

* **Locational** (#1) – 3 languages (ar, hi, ja – 75% relative frequency)

## Features for the "Complex Sentences" section

"Complex Sentences" is section 8 in WALS.

### Relativization on Subjects (WALS feature 122A)

Most frequent value (8 languages):

* **Gap** (#4 – arz, cmn, es, id, ja, sw, th, vi)

Rarer values are "Relative pronoun" (#1, 3 languages) and "Non-reduction" (#2, 1 language).

### Relativization on Obliques (WALS feature 123A)

Most frequent values (4 languages):

* **Relative pronoun** (#1 – en, es, fr, ru)
* **Gap** (#4 – cmn, id, ja, th)

Rarer values are "Non-reduction" (#2, 1 language), "Pronoun-retention" (#3, 1 language), and "Not possible" (#5, 1 language).

### 'Want' Complement Subjects (WALS feature 124A)

Most frequent value (10 languages):

* **Subject is left implicit** (#1 – cmn, en, es, fr, hi, id, ru, th, vi, yue)

Rarer values are "Subject is expressed overtly" (#2, 1 language) and "Desiderative verbal affix" (#4, 1 language).

### Purpose Clauses (WALS feature 125A)

Most frequent values (3 languages):

* **Balanced/deranked** (#2 – en, ja, ru)
* **Deranked** (#3 – es, fr, Nigerian Pidgin/pcm)
* **Balanced** (#1 – cmn, id, vi)

### 'When' Clauses (WALS feature 126A)

Most frequent value (6 languages):

* **Balanced/deranked** (#2 – en, es, fr, hi, ja, ru)

Another frequent value:

* **Balanced** (#1) – 4 languages (cmn, id, pcm, vi – 67% relative frequency)

### Reason Clauses (WALS feature 127A)

Most frequent values (5 languages):

* **Balanced/deranked** (#2 – en, es, fr, hi, ru)
* **Balanced** (#1 – cmn, id, ja, pcm, vi)

### Utterance Complement Clauses (WALS feature 128A)

Most frequent value (9 languages):

* **Balanced** (#1 – cmn, en, hi, id, ja, pcm, ru, sw, vi)

A rarer value is "Balanced/deranked" (#2, 2 languages).

## Features for the "Lexicon" section

"Lexicon" is section 9 in WALS.

### Hand and Arm (WALS feature 129A)

Most frequent value (7 languages):

* **Different** (#2 – cmn, en, es, fr, id, th, yue)

A rarer value is "Identical" (#1, 3 languages).

### Finger and Hand (WALS feature 130A)

Most frequent value (10 languages):

* **Different** (#2 – cmn, en, es, fr, id, ja, ru, sw, th, yue)

### Numeral Bases (WALS feature 131A)

Most frequent value (12 languages):

* **Decimal** (#1 – arz, cmn, en, es, fr, hi, id, ja, ru, sw, th, vi)

### Number of Non-Derived Basic Colour Categories (WALS feature 132A)

Most frequent value (6 languages):

* **6** (#7 – cmn, en, es, fr, ja, ru)

### Number of Basic Colour Categories (WALS feature 133A)

Most frequent value (5 languages):

* **11** (#7 – en, es, fr, ja, ru)

A rarer value is "8-8.5" (#5, 1 language).

### Green and Blue (WALS feature 134A)

Most frequent value (6 languages):

* **Green vs. blue** (#1 – cmn, en, es, fr, ja, ru)

### Red and Yellow (WALS feature 135A)

Most frequent value (6 languages):

* **Red vs. yellow** (#1 – cmn, en, es, fr, ja, ru)

### M-T Pronouns (WALS feature 136A)

Most frequent value (7 languages):

* **No M-T pronouns** (#1 – arz, cmn, en, id, ja, sw, vi)

Another frequent value:

* **M-T pronouns, paradigmatic** (#2) – 4 languages (es, fr, hi, ru – 57% relative frequency)

### M in First Person Singular (WALS feature 136B)

Most frequent value (6 languages):

* **m in first person singular** (#2 – en, es, fr, hi, ru, sw)

Another frequent value:

* **No m in first person singular** (#1) – 5 languages (arz, cmn, id, ja, vi – 83% relative frequency)

### N-M Pronouns (WALS feature 137A)

Most frequent value (11 languages):

* **No N-M pronouns** (#1 – arz, cmn, en, es, fr, hi, id, ja, ru, sw, vi)

### M in Second Person Singular (WALS feature 137B)

Most frequent value (8 languages):

* **No m in second person singular** (#1 – arz, cmn, en, es, fr, hi, ru, sw)

A rarer value is "m in second person singular" (#2, 3 languages).

### Tea (WALS feature 138A)

Most frequent value (9 languages):

* **Words derived from Sinitic cha** (#1 – arz, cmn, hi, ja, ru, sw, th, vi, yue)

A rarer value is "Words derived from Min Nan Chinese te" (#2, 4 languages).

## Features for the "Other" section

"Other" is section 11 in WALS.

### Para-Linguistic Usages of Clicks (WALS feature 142A)

Most frequent value (7 languages):

* **Affective meanings** (#2 – en, es, ja, ru, sw, th, yue)

A rarer value is "Logical meanings" (#1, 1 language).
